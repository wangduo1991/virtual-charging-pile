/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-03-19     wd       the first version
 */

#include <comIf.h>

#include "DataIf\msgType.h"

void preTx()
{
    msgCCS.ChargingPermissible = CCSData->ChargeEnble;
    msgCCS.CumulativeChargingTime = CCSData->ChargingTime;
    msgCCS.CurrentOutputValue = CCSData->ChrgCur;
    msgCCS.VoltageOutputValue = CCSData->ChrgVolt;

    msgCEM.Timeout_BCL = CEMData->BCLTimeout00;
    msgCEM.Timeout_BCP = CEMData->BCPTimeoutAA;
    msgCEM.Timeout_BCS = CEMData->BCSTimeout;
    msgCEM.Timeout_BRM = CEMData->BRMTimeout;
    msgCEM.Timeout_BRO = CEMData->BROTimeout;
    msgCEM.Timeout_BSD = CEMData->BSDTimeout;
    msgCEM.Timeout_BST = CEMData->BSTTimeout;

    msgCHM.MajorChargerVersion = (((int16_t)CHMData->CommProtocolVersion)>>16)&0xffff;
    msgCHM.MinorChargerVersion = (((int16_t)CHMData->CommProtocolVersion)>>8)&0xff;

    msgCML.MaxOutputCurrent = CMLData->OFC_MaxChrgCur;
    msgCML.MaxOutputVoltage = CMLData->OFC_MaxChrgVolt;
    msgCML.MinOutputCurrent = CMLData->OFC_MinChrgCur;
    msgCML.MinOutputVoltage = CMLData->OFC_MinChrgVolt;

    msgCRM.ChargerLocation = CRMData->Regional_Code;
    msgCRM.ChargerNumber = CRMData->ChargerNo;
    msgCRM.RecognitionResult = CRMData->CRM_SPN2560;

    msgCRO.ChargerReadyForCharging = CROData->CRO_SPN2830;

    msgCSD.ChargerNo = CSDData->ChargerNo;
    msgCSD.CumulativeChargingTime = CSDData->ChargingTime;
    msgCSD.OutputEnergy = CSDData->ChrgEnergy;

    msgCST.AbnormalVoltageError = CSTData->ChargerVoltageAbnormal;
    msgCST.BmsActivelySuspends = CSTData->BST_Stop;
    msgCST.CannotSupplyReqElQuantity = CSTData->ElectrictyTransAbort;
    msgCST.ChargerIntPartOverTemp = CSTData->ChargerOvertempInside;
    msgCST.ChargerOverTemp = CSTData->ChargerOverTemp;
    msgCST.ChargingConnectorFault = CSTData->ChargerConnetFault;
    msgCST.ConditionReached = CSTData->ChargeCondiAchieve;
    msgCST.CurrentMismatchError = CSTData->ChargerCurrentNotMatch;
    msgCST.FaultSuspension = CSTData->ChargerFaultSt;
    msgCST.OtherFault = CSTData->ChargerOtherFault;
    msgCST.SuddenStop = CSTData->ChargerScram;
    msgCST.SuspendsArtificially = CSTData->ManualStop;

    msgCTS.Years = CTSData->Time_Year;
    msgCTS.Months = CTSData->Time_Month;
    msgCTS.Days = CTSData->Time_Date;
    msgCTS.Hours = CTSData->Time_Hour;
    msgCTS.Minutes = CTSData->Time_Minute;
    msgCTS.Seconds = CTSData->Time_Second;

}

void preRx()
{
    DchBusA->BCP_Bat_CurVolt = msgBCP.CurrentBatteryVoltage;
    DchBusA->BHM_MaxAllow_Chg_TotVol_BHM_FromRealBMS = msgBHM.MaxChargingVoltage;
    DchBusA->Battery_Charge_Ready_BRO_FromRealBMS = msgBRO.BmsReadyForCharging;
    DchBusA->Charge_mode_BCL_FromRealBMS = msgBCL.ChargingMode;
    DchBusA->Charging_Allow_BSM_FromRealBMS = msgBSM.ChargingPermissible;
    DchBusA->Current_Demand_BCL_FromRealBMS = msgBCL.CurrentDemand;
    DchBusA->Voltage_Demand_BCL_FromRealBMS = msgBCL.VoltageDemand;

    DchBusB->Battery_SOC_BSM_FromRealBMS = msgBSM.StateOfCharge;
    DchBusB->Cell_Voltage_BSM_FromRealBMS = msgBSM.BatteryCellVoltageState;
    DchBusB->Charge_Over_Current_BSM_FromRealBMS = msgBSM.BatteryChargingOvercurrent;
    DchBusB->Connector_State_BSM_FromRealBMS = msgBSM.BatteryOutputConnectorState;
    DchBusB->Isolation_BSM_FromRealBMS = msgBSM.BatteryInsulationState;
    DchBusB->Over_Temperature_BSM_FromRealBMS = msgBSM.BatteryExcessTemperature;

    DchBusC->BST_EngChg_Flt_HVContactorFlt_BST_FromRealBMS = msgBST.HighVoltageRelayFault;
    DchBusC->BST_EngChg_Flt_Point2VltMonitor_BST_FromRealBMS = msgBST.VoltageDetectionFault;
    DchBusC->End_Charging_BMUOverTemp_BST_FromRealBMS = msgBST.BatterySetOverTemp;
    DchBusC->End_Charging_ConnectorFault_BST_FromRealBMS = msgBST.ChargingConnectorFault;
    DchBusC->End_Charging_ConnectorOverTemp_BST_FromRealBMS = msgBST.BmsConnectorOverTemp;
    DchBusC->End_Charging_Isolation_BST_FromRealBMS = msgBST.InsulationFault;
    DchBusC->End_Charging_OtherFault_BST_FromRealBMS = msgBST.OtherFaults;
    DchBusC->End_Charging_OverCurrent_BST_FromRealBMS = msgBST.ExcessiveCurrentError;
    DchBusC->End_Charging_OverTempFault_BST_FromRealBMS = msgBST.OutputConnectorOverTemp;
    DchBusC->End_Charging_VoltageAbnormal_BST_FromRealBMS = msgBST.AbnormalVoltageError;
}

void updateRxMsg()
{
      J1939_MESSAGE rxmsg;
      if(J1939_Read_Message(&rxmsg, Select_CAN_NODE_1) == RC_SUCCESS)
      {
          /***********处理接受PUD1格式数据*************/

      }

}
