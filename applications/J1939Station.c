/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-03-07     wd       the first version
 */
#include<J1939/J1939_config.h>
#include "CANIf.h"
static rt_timer_t timer1;
static struct rt_semaphore mainSem;
static void timeout1(void *parameter)
{
    rt_sem_release(&mainSem);
}

void timerInit(int msT)
{
    timer1 = rt_timer_create("timer1", timeout1,
                                 RT_NULL, msT,
                                 RT_TIMER_FLAG_PERIODIC);
}

void sendMsg( void )
{
    J1939_MESSAGE _msg;
    /*****发送数据(参考J1939的ID组成填充下面)***/
    _msg.Mxe.DataPage = 0;
    _msg.Mxe.Priority = J1939_CONTROL_PRIORITY;
    _msg.Mxe.DestinationAddress = 0x0f;
    _msg.Mxe.DataLength = 8;
    _msg.Mxe.PDUFormat = 0xfe;
    _msg.Mxe.Data[0] = 1;
    _msg.Mxe.Data[1] = 2;
    _msg.Mxe.Data[2] = 3;
    _msg.Mxe.Data[3] = 4;
    _msg.Mxe.Data[4] = 5;
    _msg.Mxe.Data[5] = 6;
    _msg.Mxe.Data[6] = 7;
    _msg.Mxe.Data[7] = 8;
    while (J1939_Send_Message( &_msg, Select_CAN_NODE_1) != RC_SUCCESS);
}

void readMsg( void )
{
    J1939_MESSAGE _msg[2];
    if(J1939_Read_Message( _msg, Select_CAN_NODE_1) == RC_SUCCESS)
    {
        /***********处理接受PUD1格式数据*************/
//        if(_msg.Mxe.PGN == 0x000100)
//        {
//            ;//你的代码
//        }
    }
    if(J1939_Read_Message( (_msg+1), Select_CAN_NODE_2) == RC_SUCCESS)
    {
        /***********处理接受PDU2格式数据*************/
//        if(_msg.Mxe.PGN == 0x00F101)
//        {
//            ;//你的代码
//        }
    }
}

void mainTask()
{
    while(1)
    {
        rt_sem_take(&mainSem, RT_WAITING_FOREVER);
        J1939_Poll();
    }
}

int TestJ1939()
{
    j1939_int8_t _data[9] = {1,2,3,4,5,6,7,8,9};
    //初始化can驱动，定时器初始化
    rt_thread_t comTask;
    CANInit();
    timerInit(1);
    //初始化J1939协议栈
    J1939_Initialization();
    //在这里启动定时器
    if (timer1 != RT_NULL)
    {
        rt_timer_start(timer1);
    }
    else {
        return 0;
    }
    rt_sem_init(&mainSem, "main Station", 0,RT_IPC_FLAG_FIFO);
//    rt_sem_init(&read_sem, "read task", 0,RT_IPC_FLAG_FIFO);
    comTask = rt_thread_create("comTask", mainTask, RT_NULL, 1024, 10, 5);
    if (comTask != RT_NULL)
           rt_thread_startup(comTask);
    while(1)
    {

        /*发送一帧数据*/
        //sendMsg();
        J1939_TP_TX_Message(0x1100,0x56,(j1939_int8_t*)_data,sizeof(_data),Select_CAN_NODE_1);
        /*接受一帧数据*/
        readMsg();
        rt_thread_mdelay(100);
    }
}

MSH_CMD_EXPORT(TestJ1939,"TestJ1939");
