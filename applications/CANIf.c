/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-02-20     wd       the first version
 */

#include "CANIf.h"
uint8_t rFlag=0;
//static rt_device_t can_dev;

rt_err_t can1_rx_call(rt_device_t dev,rt_size_t size)
{
    rFlag=1;
    return RT_EOK;
}

void CANInit()
{
    can_dev = rt_device_find(CAN_DEVICE_NAME);
    if(!can_dev)
    {
        rt_kprintf("No device find!");
        return;
    }
//    rt_sem_init(&rx_sem,"rx_sem",0,RT_IPC_FLAG_FIFO);
    rt_device_open(can_dev, RT_DEVICE_FLAG_INT_RX|RT_DEVICE_FLAG_INT_TX);
    rt_device_control(can_dev, RT_CAN_CMD_SET_BAUD, (void *)CAN500kBaud);
    rt_device_control(can_dev, RT_CAN_CMD_SET_MODE, (void *)RT_CAN_MODE_NORMAL);
    rt_device_set_rx_indicate(can_dev, can1_rx_call);
}
