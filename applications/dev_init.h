/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-02-16     wd       the first version
 */
#ifndef APPLICATIONS_DEV_INIT_H_
#define APPLICATIONS_DEV_INIT_H_
void LEDInit();
void LED_ON();


#endif /* APPLICATIONS_DEV_INIT_H_ */
