﻿#include "J1939_config.h"


void J1939_SetAddressFilter(unsigned char Ps_Address)
{
	switch (Can_Node)
	{
		case  Select_CAN_NODE_1:
		{
			break;
		}
		case  Select_CAN_NODE_2:
		{
			break;
		}
		case  Select_CAN_NODE_3:
		{
			break;
		}
		case  Select_CAN_NODE_4:
		{
			break;
		}
		default  :
		{
			break;
		}
	}
}

/*
*输入：  *MsgPtr ，协议要发送的消息，
*输出： 
*说明：      将数据 从MsgPtr结构体赋值到CAN驱动自带的结构体中
		先将传入函数的MsgPtr中的数据写到CAN的结构体，再调用CAN驱动的发送函数
		默认支持4路CAN硬件的收发。如少于4路，只需配置相应的Can_Node开关代码区，
		其他（Select_CAN_NODE）保持不变。就直接返回（break）。
*/
void J1939_CAN_Transmit(J1939_MESSAGE *MsgPtr)
{
    struct rt_can_msg txmsg={0};
	
	switch (Can_Node)
	{
		case  Select_CAN_NODE_1:
		{
			/*加载第一路CAN硬件的29位ID*/
                txmsg.ide = RT_CAN_EXTID;
                txmsg.rtr = RT_CAN_DTR;
                txmsg.id =  (MsgPtr->Mxe.SourceAddress)+ ((MsgPtr->Mxe.PDUSpecific)<<8)
                        +  ((MsgPtr->Mxe.PDUFormat)<<16) +
                        ((((MsgPtr->Mxe.Priority)<<2) + ((MsgPtr->Mxe.Res)<<1)+(MsgPtr->Mxe.DataPage))<<24);
                //rt_kprintf("sa %0x",((((MsgPtr->Mxe.Priority)<<2) + ((MsgPtr->Mxe.Res)<<1)+(MsgPtr->Mxe.DataPage))<<24));
			/*CAN硬件加载数据长度*/
                txmsg.len = MsgPtr->Mxe.DataLength;
                rt_memcpy(txmsg.data,MsgPtr->Mxe.Data,MsgPtr->Mxe.DataLength);
			/*CAN硬件加载数据*/

			/*CAN硬件加载RTR*/

			//CAN硬件开始发送数据
                rt_device_write(can_dev,0,&txmsg,sizeof(txmsg));
			break;
		}
		case  Select_CAN_NODE_2:
		{
	
			/*加载第二路CAN硬件的29位ID*/

			/*CAN硬件加载数据长度*/
	
			/*CAN硬件加载数据*/

			/*CAN硬件加载RTR*/

			//CAN硬件开始发送数据
			break;
		}
		case  Select_CAN_NODE_3:
		{
			
			/*加载第三路CAN硬件的29位ID*/

			/*CAN硬件加载数据长度*/
	
			/*CAN硬件加载数据*/

			/*CAN硬件加载RTR*/

			//CAN硬件开始发送数据
			break;
		}
		case  Select_CAN_NODE_4:
		{
			/*加载第四路CAN硬件的29位ID*/

			/*CAN硬件加载数据长度*/
	
			/*CAN硬件加载数据*/

			/*CAN硬件加载RTR*/

			//CAN硬件开始发送数据
			break;
		}
		default  :
		{
			break;
		}
	}
}
/*
*输入：     *MsgPtr 数据要存入的内存的指针
*输出：      1 | 0
*说明：      读取CAN驱动的数据，如果没有数据，返回0
		将CAN中的数据取出，存入J1939_MESSAGE结构体中
		默认支持4路CAN硬件的收发。如少于4路，只需配置相应的Can_Node开关代码区，
		其他（Select_CAN_NODE）保持不变。就直接返回（return 0）
*/

int J1939_CAN_Receive(J1939_MESSAGE *MsgPtr)
{

	switch (Can_Node)
	{
		case  Select_CAN_NODE_1:
		{
			if(rFlag>0)//判断CAN硬件1是否有数据到来
			{
			    rt_device_read(can_dev,0,&gs_rxmsg,sizeof(gs_rxmsg));
			    rt_memcpy(MsgPtr->Mxe.Data,gs_rxmsg.data,gs_rxmsg.len);
			    //rt_sem_take(&read_sem, RT_WAITING_FOREVER);
			    MsgPtr->Mxe.DataLength = gs_rxmsg.id;
			    MsgPtr->Mxe.DataPage = (gs_rxmsg.id)>>24&1;
			    MsgPtr->Mxe.PDUFormat = (gs_rxmsg.id)>>16&0xff;
			    MsgPtr->Mxe.PDUSpecific =  (gs_rxmsg.id)>>8&0xff;
			    MsgPtr->Mxe.SourceAddress =  (gs_rxmsg.id)&0xff;
				//你的代码，从CAN硬件1 中将数据读取后，存入 MsgPtr
			    rFlag = 0;
			    rt_kprintf("data id is %d",gs_rxmsg.id);
				return 1;
			}
			return 0;
			break;
		}
		case  Select_CAN_NODE_2:
		{
			if("你的代码")//判断CAN硬件2是否有数据到来
			{
				//你的代码，从CAN硬件2 中将数据读取后，存入 MsgPtr
				return 1;
			}
			return 0;
			break;

		}
		case  Select_CAN_NODE_3:
		{
			if("你的代码")//判断CAN硬件3是否有数据到来
			{
				//你的代码，从CAN硬件3 中将数据读取后，存入 MsgPtr
				return 1;
			}
			return 0;
			break;

		}
		case  Select_CAN_NODE_4:
		{
			if("你的代码")//判断CAN硬件4是否有数据到来
			{
				//你的代码，从CAN硬件4 中将数据读取后，存入 MsgPtr
				return 1;
			}
			return 0;
			break;
		}
		default  :
		{
			return 0;//没有消息
			break;
		}
	}
	return 0;//没有消息
}

/*不使用中断模式，不对下面的函数进行移植*/
#if J1939_POLL_ECAN == J1939_FALSE
/*
*输入：
*输出：
*说明：使能接受中断
*/
	void J1939_RXinterruptEnable()
	{
	    rt_hw_interrupt_enable(RxLevel);
	}
/*
*输入：
*输出：
*说明：失能接受中断
*/
	void J1939_RXinterruptDisable()
	{
	    RxLevel = rt_hw_interrupt_disable();
	}
/*
*输入：
*输出：
*说明：使能发送中断
*/
	void J1939_TXinterruptEnable()
	{
	    rt_hw_interrupt_disable(TxLevel);
	}
/*
*输入：
*输出：
*说明：失能发送中断
*/
	void J1939_TXinterruptDisable()
	{
	    TxLevel = rt_hw_interrupt_disable();
	}
/*
*输入：
*输出：
*说明：触发发送中断标致位，当协议栈在中断模式下，要发送消息，将调用此函数
	CAN驱动函数，就将直接把消息发送出去，不需要协议在调用任何can驱动函数
*/
	void J1939_TXinterruptOk()
	{
		;
	}
/*
*输入：
*输出：
*说明：清除CAN驱动相关的中断产生标识位，包括（发送中断标志位，接受中断标
	志位，can总线错误标识位）
*/
	void CAN_identifier_clc()
	{
		;
	}
#endif

