/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-03-19     wd       the first version
 */
#ifndef APPLICATIONS_MAIN_H_
#define APPLICATIONS_MAIN_H_

#include <stdint.h>
#include <rtthread.h>
#include <rtdevice.h>
#include <n32g45x_rcc.h>

#include "Charger\DCHCode.h"

#include "J1939\J1939_config.h"
#include "CANIf.h"

extern BusA *DchBusA;
extern BusB *DchBusB;
extern BusC *DchBusC;
extern uint8_T OFCKey;
extern boolean_T DCPlugIn;
extern real32_T DCRelaySts;
extern real32_T ChargeCurr;
extern real32_T ChargeVolt;
extern real32_T OFC_MIN_VOLT;
extern real32_T OFC_MIN_CURR;
extern real32_T OFC_MAX_VOLT;
extern real32_T OFC_MAX_CURR;
extern real32_T K1K2Volt_ChargerSide;
extern real32_T K1K2Volt_VehSide;
extern boolean_T CC2Sts;
extern real32_T K1K2RelayCtrl;
extern real32_T K3K4RelayCtrl;
extern real32_T ChargeSysSt;
extern real32_T ChargeProcModeSt;
extern real32_T ChargeEnable;
extern real32_T ChargingTime;
extern CHM *CHMData;
extern CRM *CRMData;
extern CTS *CTSData;
extern CML *CMLData;
extern CRO *CROData;
extern CCS *CCSData;
extern CSD *CSDData;
extern CEM *CEMData;
extern CST *CSTData;



#endif /* APPLICATIONS_MAIN_H_ */
