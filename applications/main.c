/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2015-07-29     Arda.Fu      first implementation
 */
#include <main.h>



/* defined the LED1 pin: PB5 */
#define LED1_PIN    90
#define LED2_PIN    91
#define LED3_PIN    67

RT_MODEL *const rtDCH;
BusA *DchBusA;
BusB *DchBusB;
BusC *DchBusC;
uint8_T OFCKey;
boolean_T DCPlugIn;
real32_T DCRelaySts;
real32_T ChargeCurr;
real32_T ChargeVolt;
real32_T OFC_MIN_VOLT = 50;
real32_T OFC_MIN_CURR = 1;
real32_T OFC_MAX_VOLT = 750;
real32_T OFC_MAX_CURR =400;
real32_T K1K2Volt_ChargerSide;
real32_T K1K2Volt_VehSide;
boolean_T CC2Sts;
real32_T K1K2RelayCtrl;
real32_T K3K4RelayCtrl;
real32_T ChargeSysSt;
real32_T ChargeProcModeSt;
real32_T ChargeEnable;
real32_T ChargingTime;
CHM *CHMData;
CRM *CRMData;
CTS *CTSData;
CML *CMLData;
CRO *CROData;
CCS *CCSData;
CSD *CSDData;
CEM *CEMData;
CST *CSTData;
int8_T taskId = 0;

//For J1939
static rt_timer_t comTimer;
static struct rt_semaphore J1939Sem;

static void comTimeout(void *parameter)
{
    rt_sem_release(&J1939Sem);
}

void comTimerInit(int msT)
{
    comTimer = rt_timer_create("timer1", comTimeout,
                                 RT_NULL, msT,
                                 RT_TIMER_FLAG_PERIODIC);
}
void J1939Task()
{
    while(1)
    {
        rt_sem_take(&J1939Sem, RT_WAITING_FOREVER);
        J1939_Poll();
    }
}





int main(void)
{
    rt_thread_t comTask;
    CANInit();
    comTimerInit(1);
    //初始化J1939协议栈
    J1939_Initialization();
    //在这里启动定时器
    if (comTimer != RT_NULL)
    {
        rt_timer_start(comTimer);
    }
    else {
        return 0;
    }
    rt_sem_init(&J1939Sem, "main Station", 0,RT_IPC_FLAG_FIFO);
    comTask = rt_thread_create("comTask", J1939Task, RT_NULL, 1024, 10, 5);
    if (comTask != RT_NULL)
           rt_thread_startup(comTask);


    uint32_t Speed = 1000;
//    /* set LED1 pin mode to output */
//    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_AFIO, ENABLE);
//    GPIO_ConfigPinRemap(GPIO_RMP_SW_JTAG_SW_ENABLE, ENABLE);
//    rt_pin_mode(LED1_PIN, PIN_MODE_OUTPUT);
//    rt_pin_mode(LED2_PIN, PIN_MODE_OUTPUT);
    rt_pin_mode(LED3_PIN, PIN_MODE_OUTPUT);

    DCHCode_initialize(rtDCH, DchBusB, DchBusC, DchBusA, &OFCKey,
            &DCPlugIn, &DCRelaySts, &ChargeCurr, &ChargeVolt, &OFC_MIN_VOLT,
            &OFC_MIN_CURR, &OFC_MAX_CURR, &OFC_MAX_VOLT, &K1K2Volt_ChargerSide,
            &K1K2Volt_VehSide, &CC2Sts, &K1K2RelayCtrl, &K3K4RelayCtrl, &ChargeSysSt,
            &ChargeProcModeSt, &ChargeEnable, &ChargingTime, CHMData, CRMData,
            CTSData, CMLData, CROData, CCSData, CSDData, CEMData, CSTData);


    while (1)
    {
//        rt_pin_write(LED1_PIN, PIN_LOW);
//        rt_pin_write(LED2_PIN, PIN_LOW);
        rt_pin_write(LED3_PIN, PIN_LOW);
        rt_thread_mdelay(Speed);
//        rt_pin_write(LED1_PIN, PIN_HIGH);
//        rt_pin_write(LED2_PIN, PIN_HIGH);
        rt_pin_write(LED3_PIN, PIN_HIGH);
        rt_thread_mdelay(Speed);

        DCHCode_step(rtDCH, taskId, DchBusB, DchBusC, DchBusA, OFCKey,
                DCPlugIn, DCRelaySts, &ChargeCurr, &ChargeVolt, &OFC_MIN_VOLT,
                &OFC_MIN_CURR, &OFC_MAX_CURR, &OFC_MAX_VOLT, &K1K2Volt_ChargerSide,
                &K1K2Volt_VehSide, &CC2Sts, &K1K2RelayCtrl, &K3K4RelayCtrl, &ChargeSysSt,
                &ChargeProcModeSt, &ChargeEnable, &ChargingTime, CHMData, CRMData,
                CTSData, CMLData, CROData, CCSData, CSDData, CEMData, CSTData);

        taskId = 1;
    }
}

