/*
 * File: DCHCode.c
 *
 * Code generated for Simulink model 'DCHCode'.
 *
 * Model version                  : 6.7
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Sat Feb 12 22:32:44 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex-M
 * Emulation hardware selection:
 *    Differs from embedded hardware (Custom Processor->MATLAB Host Computer)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "DCHCode.h"
#include "DCHCode_private.h"

/* Named constants for Chart: '<S8>/Plug Logic' */
#define IN_CC_Confirm                  ((uint8_T)1U)
#define IN_ELock                       ((uint8_T)2U)
#define IN_Init                        ((uint8_T)3U)
#define IN_NO_ACTIVE_CHILD             ((uint8_T)0U)
#define IN_S_Close                     ((uint8_T)4U)
#define IN_S_Open                      ((uint8_T)5U)

/* Named constants for Chart: '<S3>/Charge Logic Control' */
#define IN_ActDisCharge                ((uint8_T)1U)
#define IN_ActDisChrg                  ((uint8_T)1U)
#define IN_Active                      ((uint8_T)1U)
#define IN_ActiveDischarge             ((uint8_T)1U)
#define IN_BCL_Received                ((uint8_T)1U)
#define IN_BCS_BCL_Received            ((uint8_T)2U)
#define IN_BCS_Received                ((uint8_T)3U)
#define IN_BRMReceived                 ((uint8_T)1U)
#define IN_BRO_RX_00                   ((uint8_T)1U)
#define IN_BSDRecieved                 ((uint8_T)2U)
#define IN_CHM_Send                    ((uint8_T)1U)
#define IN_CRM_Send1                   ((uint8_T)2U)
#define IN_CRM_Send2                   ((uint8_T)3U)
#define IN_CRO_Send1                   ((uint8_T)2U)
#define IN_CTS_CML_Send                ((uint8_T)3U)
#define IN_ChargeStop                  ((uint8_T)3U)
#define IN_Charging                    ((uint8_T)4U)
#define IN_ChargingManage              ((uint8_T)1U)
#define IN_Chart                       ((uint8_T)2U)
#define IN_ChrgPause                   ((uint8_T)5U)
#define IN_ChrgPreStop                 ((uint8_T)4U)
#define IN_ChrgingDuration             ((uint8_T)1U)
#define IN_Default                     ((uint8_T)3U)
#define IN_Default_l                   ((uint8_T)1U)
#define IN_Default_l5                  ((uint8_T)4U)
#define IN_Default_l5v                 ((uint8_T)2U)
#define IN_Default_l5vq                ((uint8_T)6U)
#define IN_ELockClose                  ((uint8_T)2U)
#define IN_End                         ((uint8_T)4U)
#define IN_End_h                       ((uint8_T)3U)
#define IN_HandShake                   ((uint8_T)2U)
#define IN_HandshakeIdentification     ((uint8_T)3U)
#define IN_IS                          ((uint8_T)3U)
#define IN_IS_End                      ((uint8_T)4U)
#define IN_IS_Monitoring_Enable        ((uint8_T)5U)
#define IN_IS_VoltEnable               ((uint8_T)6U)
#define IN_InsulationEnable            ((uint8_T)2U)
#define IN_InsulationMonitoring        ((uint8_T)4U)
#define IN_K1K2RlyClose                ((uint8_T)5U)
#define IN_K1K2RlyOpen                 ((uint8_T)5U)
#define IN_K3K4RlyClose                ((uint8_T)4U)
#define IN_K3K4RlyOpen                 ((uint8_T)6U)
#define IN_MainBlock                   ((uint8_T)2U)
#define IN_PD                          ((uint8_T)5U)
#define IN_PD_End                      ((uint8_T)6U)
#define IN_ParameterConfiguration      ((uint8_T)5U)
#define IN_PhyConnConfirm              ((uint8_T)6U)
#define IN_PowerDown                   ((uint8_T)3U)
#define IN_PowerDownInit               ((uint8_T)7U)
#define IN_PreCharge                   ((uint8_T)6U)
#define IN_Sleep                       ((uint8_T)4U)
#define IN_Sleep_a                     ((uint8_T)8U)
#define IN_SystemOFF                   ((uint8_T)1U)
#define IN_SystemON                    ((uint8_T)2U)
#define IN_VehParaConfirm              ((uint8_T)7U)
#define IN_WaitForBHM                  ((uint8_T)3U)
#define IN_WaitForBSD                  ((uint8_T)9U)
#define IN_WaitForBST                  ((uint8_T)10U)
#define IN_end                         ((uint8_T)8U)
#define IN_end_d                       ((uint8_T)5U)

const CCS DCHCode_rtZCCS = {
  0.0F,                                /* ChrgCur */
  0.0F,                                /* ChrgVolt */
  0.0F,                                /* En_CCS */
  0.0F,                                /* ChargeEnble */
  0.0F                                 /* ChargingTime */
} ;                                    /* CCS ground */

const CEM DCHCode_rtZCEM = {
  0.0F,                                /* En_CEM */
  0.0F,                                /* BRMTimeout */
  0.0F,                                /* BCPTimeoutAA */
  0.0F,                                /* BCSTimeout */
  0.0F,                                /* BCLTimeout00 */
  0.0F,                                /* BSTTimeout */
  0.0F,                                /* BSDTimeout */
  0.0F,                                /* BROTimeout */
  0.0F                                 /* Others */
} ;                                    /* CEM ground */

const CHM DCHCode_rtZCHM = {
  0.0F,                                /* CommProtocolVersion */
  0.0F                                 /* En_CHM */
} ;                                    /* CHM ground */

const CML DCHCode_rtZCML = {
  0.0F,                                /* OFC_MinChrgVolt */
  0.0F,                                /* OFC_MinChrgCur */
  0.0F,                                /* OFC_MaxChrgCur */
  0.0F,                                /* OFC_MaxChrgVolt */
  0.0F                                 /* En_CML */
} ;                                    /* CML ground */

const CRM DCHCode_rtZCRM = {
  0.0F,                                /* CRM_SPN2560 */
  0.0F,                                /* En_CRM */
  0.0F,                                /* Regional_Code */
  0.0F                                 /* ChargerNo */
} ;                                    /* CRM ground */

const CRO DCHCode_rtZCRO = {
  0.0F,                                /* En_CRO */
  0.0F                                 /* CRO_SPN2830 */
} ;                                    /* CRO ground */

const CSD DCHCode_rtZCSD = {
  0.0F,                                /* ChrgEnergy */
  0.0F,                                /* ChargerNo */
  0.0F,                                /* En_CSD */
  0.0F                                 /* ChargingTime */
} ;                                    /* CSD ground */

const CST DCHCode_rtZCST = {
  0.0F,                                /* En_CST */
  0.0F,                                /* ChargeCondiAchieve */
  0.0F,                                /* ManualStop */
  0.0F,                                /* ChargerFaultSt */
  0.0F,                                /* BST_Stop */
  0.0F,                                /* ChargerOverTemp */
  0.0F,                                /* ChargerConnetFault */
  0.0F,                                /* ChargerOvertempInside */
  0.0F,                                /* ElectrictyTransAbort */
  0.0F,                                /* ChargerScram */
  0.0F,                                /* ChargerOtherFault */
  0.0F,                                /* ChargerCurrentNotMatch */
  0.0F                                 /* ChargerVoltageAbnormal */
} ;                                    /* CST ground */

const CTS DCHCode_rtZCTS = {
  0.0F,                                /* Time_Date */
  0.0F,                                /* Time_Hour */
  0.0F,                                /* Time_Minute */
  0.0F,                                /* Time_Month */
  0.0F,                                /* Time_Second */
  0.0F,                                /* Time_Year */
  0.0F                                 /* En_CTS */
} ;                                    /* CTS ground */

/* Forward declaration for local functions */
static void enter_internal_PowerDown(DW *rtDW);
static void ChrgingDuration(const real32_T *DiscreteTimeIntegrator, const
  real32_T *DiscreteTimeIntegrator1, const real32_T *DiscreteTimeIntegrator2,
  const ChrgParaSet *BusConversion_InsertedFor_Charg, DW *rtDW);
static void InsulationMonitoring(const ChrgParaSet
  *BusConversion_InsertedFor_Charg, const boolean_T *DataTypeConversion23, DW
  *rtDW);
static void ParameterConfiguration(const real32_T *DiscreteTimeIntegrator_n,
  const real32_T *DiscreteTimeIntegrator, const ChrgParaSet
  *BusConversion_InsertedFor_Charg, DW *rtDW);
static void exit_internal_MainBlock(DW *rtDW);
static void MainBlock(const real32_T *DiscreteTimeIntegrator_n, const real32_T
                      *DiscreteTimeIntegrator, const real32_T
                      *DiscreteTimeIntegrator1, const real32_T
                      *DiscreteTimeIntegrator2, const ChrgParaSet
                      *BusConversion_InsertedFor_Charg, const boolean_T
                      *DataTypeConversion23, DW *rtDW);
static void exit_internal_PowerDown(DW *rtDW);
static void enter_atomic_Sleep(DW *rtDW);
static void PowerDown(const real32_T *DiscreteTimeIntegrator, const real32_T
                      *DiscreteTimeIntegrator1, const boolean_T
                      *DataTypeConversion39, const real32_T
                      *DiscreteTimeIntegrator2, const ChrgParaSet
                      *BusConversion_InsertedFor_Charg, const real32_T *Delay4,
                      boolean_T rtU_DCPlugIn, DW *rtDW);
static void enter_atomic_SystemOFF(DW *rtDW);
const BusA DCHCode_rtZBusA = { 0.0F,   /* BRM_RCVFlag */
  0.0F,                                /* BHM_ReceiveFlag */
  0.0F,                                /* BRO_ReceiveFlag */
  0.0F,                                /* BCS_RCVFlag */
  0.0F,                                /* BCP_RCVFlag */
  0.0F,                                /* BEM_ReceiveFlag */
  0.0F,                                /* BCL_ReceiveFlag */
  0.0F,                                /* BSD_ReceiveFlag */
  0.0F,                                /* BST_ReceiveFlag */
  0.0F,                               /* Battery_Charge_Ready_BRO_FromRealBMS */
  0.0F,                                /* Charging_Allow_BSM_FromRealBMS */
  0.0F,                                /* BSM_ReceiveFlag */
  0.0F,                                /* Charge_mode_BCL_FromRealBMS */
  0.0F,                                /* Current_Demand_BCL_FromRealBMS */
  0.0F,                                /* Voltage_Demand_BCL_FromRealBMS */
  0.0F,                                /* BCP_Bat_CurVolt */
  0.0F,                            /* BHM_MaxAllow_Chg_TotVol_BHM_FromRealBMS */
  0.0F,                                /* PackCur */
  0.0F                                 /* LinkVolt */
};

const BusB DCHCode_rtZBusB = { 0.0F,   /* Connector_State_BSM_FromRealBMS */
  0.0F,                                /* Isolation_BSM_FromRealBMS */
  0.0F,                                /* Cell_Voltage_BSM_FromRealBMS */
  0.0F,                                /* Battery_SOC_BSM_FromRealBMS */
  0.0F,                                /* Charge_Over_Current_BSM_FromRealBMS */
  0.0F                                 /* Over_Temperature_BSM_FromRealBMS */
};

const BusC DCHCode_rtZBusC = { 0.0F,
                           /* BST_EngChg_Flt_Point2VltMonitor_BST_FromRealBMS */
  0.0F,                      /* BST_EngChg_Flt_HVContactorFlt_BST_FromRealBMS */
  0.0F,                       /* End_Charging_VoltageAbnormal_BST_FromRealBMS */
  0.0F,                            /* End_Charging_OtherFault_BST_FromRealBMS */
  0.0F,                             /* End_Charging_Isolation_BST_FromRealBMS */
  0.0F,                     /* End_Charging_ConnectorOverTemp_BST_FromRealBMS */
  0.0F,                           /* End_Charging_BMUOverTemp_BST_FromRealBMS */
  0.0F,                        /* End_Charging_ConnectorFault_BST_FromRealBMS */
  0.0F,                         /* End_Charging_OverTempFault_BST_FromRealBMS */
  0.0F                            /* End_Charging_OverCurrent_BST_FromRealBMS */
};

real32_T look1_iflf_binlx(real32_T u0, const real32_T bp0[], const real32_T
  table[], uint32_T maxIndex)
{
  real32_T frac;
  real32_T yL_0d0;
  uint32_T bpIdx;
  uint32_T iLeft;
  uint32_T iRght;

  /* Column-major Lookup 1-D
     Search method: 'binary'
     Use previous index: 'off'
     Interpolation method: 'Linear point-slope'
     Extrapolation method: 'Linear'
     Use last breakpoint for index at or above upper limit: 'off'
     Remove protection against out-of-range input in generated code: 'off'
   */
  /* Prelookup - Index and Fraction
     Index Search method: 'binary'
     Extrapolation method: 'Linear'
     Use previous index: 'off'
     Use last breakpoint for index at or above upper limit: 'off'
     Remove protection against out-of-range input in generated code: 'off'
   */
  if (u0 <= bp0[0U]) {
    iLeft = 0U;
    frac = (u0 - bp0[0U]) / (bp0[1U] - bp0[0U]);
  } else if (u0 < bp0[maxIndex]) {
    /* Binary Search */
    bpIdx = maxIndex >> 1U;
    iLeft = 0U;
    iRght = maxIndex;
    while (iRght - iLeft > 1U) {
      if (u0 < bp0[bpIdx]) {
        iRght = bpIdx;
      } else {
        iLeft = bpIdx;
      }

      bpIdx = (iRght + iLeft) >> 1U;
    }

    frac = (u0 - bp0[iLeft]) / (bp0[iLeft + 1U] - bp0[iLeft]);
  } else {
    iLeft = maxIndex - 1U;
    frac = (u0 - bp0[maxIndex - 1U]) / (bp0[maxIndex] - bp0[maxIndex - 1U]);
  }

  /* Column-major Interpolation 1-D
     Interpolation method: 'Linear point-slope'
     Use last breakpoint for index at or above upper limit: 'off'
     Overflow mode: 'wrapping'
   */
  yL_0d0 = table[iLeft];
  return (table[iLeft + 1U] - yL_0d0) * frac + yL_0d0;
}

real32_T rt_powf_snf(real32_T u0, real32_T u1)
{
  real32_T tmp;
  real32_T tmp_0;
  real32_T y;
  if (rtIsNaNF(u0) || rtIsNaNF(u1)) {
    y = (rtNaNF);
  } else {
    tmp = (real32_T)fabs(u0);
    tmp_0 = (real32_T)fabs(u1);
    if (rtIsInfF(u1)) {
      if (tmp == 1.0F) {
        y = 1.0F;
      } else if (tmp > 1.0F) {
        if (u1 > 0.0F) {
          y = (rtInfF);
        } else {
          y = 0.0F;
        }
      } else if (u1 > 0.0F) {
        y = 0.0F;
      } else {
        y = (rtInfF);
      }
    } else if (tmp_0 == 0.0F) {
      y = 1.0F;
    } else if (tmp_0 == 1.0F) {
      if (u1 > 0.0F) {
        y = u0;
      } else {
        y = 1.0F / u0;
      }
    } else if (u1 == 2.0F) {
      y = u0 * u0;
    } else if ((u1 == 0.5F) && (u0 >= 0.0F)) {
      y = (real32_T)sqrt(u0);
    } else if ((u0 < 0.0F) && (u1 > (real32_T)floor(u1))) {
      y = (rtNaNF);
    } else {
      y = (real32_T)pow(u0, u1);
    }
  }

  return y;
}

/* Function for Chart: '<S3>/Charge Logic Control' */
static void enter_internal_PowerDown(DW *rtDW)
{
  rtDW->is_PowerDown = IN_PowerDownInit;
  rtDW->temporalCounter_i1 = 0U;
  rtDW->PowerDownInitFinish = true;
  rtDW->CtimeEn0 = false;
  rtDW->CtimeEn1 = false;
  rtDW->CtimeEn2 = false;
  rtDW->En_CHM = false;
  rtDW->En_CRM = false;
  rtDW->En_CTS = false;
  rtDW->En_CML = false;
  rtDW->En_CRO = false;
  rtDW->En_CCS = false;
  rtDW->En_CST = false;
  rtDW->En_CSD = false;
  rtDW->CRO_SPN2830 = 0U;
  rtDW->CRM_SPN2560 = 0U;
  rtDW->ActDisChrgSw = false;
  rtDW->IS_VoltEn = false;
  rtDW->IS_ActDisChrgEn = false;
  rtDW->PD_ActDisChrgEn = false;
  rtDW->PreChrgEn = false;
  rtDW->BHMTimeout = false;
}

/* Function for Chart: '<S3>/Charge Logic Control' */
static void ChrgingDuration(const real32_T *DiscreteTimeIntegrator, const
  real32_T *DiscreteTimeIntegrator1, const real32_T *DiscreteTimeIntegrator2,
  const ChrgParaSet *BusConversion_InsertedFor_Charg, DW *rtDW)
{
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  rtDW->SysChargSt = 6U;
  guard1 = false;
  guard2 = false;
  switch (rtDW->is_ChrgingDuration) {
   case IN_ChargingManage:
    if (*DiscreteTimeIntegrator >
        BusConversion_InsertedFor_Charg->Time_ReceiveBCL) {
      rtDW->BCLTimeout = true;
      rtDW->ProcessMod = 12U;
      guard2 = true;
    } else if (*DiscreteTimeIntegrator1 >
               BusConversion_InsertedFor_Charg->Time_ReceiveBCS) {
      rtDW->BCSTimeout = true;
      rtDW->ProcessMod = 12U;
      guard2 = true;
    } else if (rtDW->DataTypeConversion36 && (!rtDW->LogicalOperator1)) {
      rtDW->BST_Stop = true;
      if (rtDW->is_ChargingManage == IN_ChrgPause) {
        rtDW->CtimeEn2 = false;
        rtDW->is_ChargingManage = IN_NO_ACTIVE_CHILD;
      } else {
        rtDW->is_ChargingManage = IN_NO_ACTIVE_CHILD;
      }

      rtDW->is_ChrgingDuration = IN_End_h;
      rtDW->CtimeEn0 = false;
      rtDW->CtimeEn1 = false;
      rtDW->ChargeFinish = true;
    } else {
      guard2 = true;
    }
    break;

   case IN_Default_l5v:
    rtDW->is_ChrgingDuration = IN_ChargingManage;
    rtDW->is_ChargingManage = IN_Default_l5vq;
    rtDW->temporalCounter_i1 = 0U;
    break;

   default:
    /* case IN_End: */
    break;
  }

  if (guard2) {
    switch (rtDW->is_ChargingManage) {
     case IN_BCL_Received:
      if ((rtDW->temporalCounter_i1 >= (uint32_T)
           BusConversion_InsertedFor_Charg->CCSSendWaitTime) &&
          rtDW->DataTypeConversion43) {
        rtDW->is_ChargingManage = IN_BCS_BCL_Received;
        rtDW->temporalCounter_i1 = 0U;
        rtDW->En_CRO = false;
        rtDW->En_CCS = true;
      } else {
        rtDW->CtimeEn0 = rtDW->DataTypeConversion38;
      }
      break;

     case IN_BCS_BCL_Received:
      if (rtDW->temporalCounter_i1 >= 2U) {
        rtDW->is_ChargingManage = IN_Charging;
        rtDW->ChargeEnable = true;
      } else {
        rtDW->CtimeEn0 = rtDW->DataTypeConversion38;
        rtDW->CtimeEn1 = rtDW->DataTypeConversion43;
      }
      break;

     case IN_BCS_Received:
      if ((rtDW->temporalCounter_i1 >= (uint32_T)
           BusConversion_InsertedFor_Charg->CCSSendWaitTime) &&
          rtDW->DataTypeConversion38) {
        rtDW->is_ChargingManage = IN_BCS_BCL_Received;
        rtDW->temporalCounter_i1 = 0U;
        rtDW->En_CRO = false;
        rtDW->En_CCS = true;
      } else {
        rtDW->CtimeEn1 = rtDW->DataTypeConversion43;
      }
      break;

     case IN_Charging:
      if ((!rtDW->LogicalOperator) && (!rtDW->DataTypeConversion35) &&
          rtDW->DataTypeConversion51) {
        rtDW->CtimeEn2 = true;
        rtDW->is_ChargingManage = IN_ChrgPause;
        rtDW->ChargeEnable = false;
      } else {
        rtDW->CtimeEn0 = rtDW->DataTypeConversion38;
        rtDW->CtimeEn1 = rtDW->DataTypeConversion43;
      }
      break;

     case IN_ChrgPause:
      if ((!rtDW->LogicalOperator) && rtDW->DataTypeConversion35) {
        rtDW->CtimeEn2 = false;
        rtDW->is_ChargingManage = IN_Charging;
        rtDW->ChargeEnable = true;
      } else {
        if (*DiscreteTimeIntegrator2 >
            BusConversion_InsertedFor_Charg->Time_ChrgWait) {
          rtDW->ProcessMod = 11U;
        }

        rtDW->CtimeEn0 = rtDW->DataTypeConversion38;
        rtDW->CtimeEn1 = rtDW->DataTypeConversion43;
      }
      break;

     default:
      /* case IN_Default: */
      if (rtDW->DataTypeConversion38 && (!rtDW->DataTypeConversion43)) {
        rtDW->is_ChargingManage = IN_BCL_Received;
        rtDW->temporalCounter_i1 = 0U;
      } else if (rtDW->DataTypeConversion38 && rtDW->DataTypeConversion43) {
        rtDW->CtimeEn0 = rtDW->DataTypeConversion38;
        rtDW->CtimeEn1 = rtDW->DataTypeConversion43;
        if (rtDW->temporalCounter_i1 >= (uint32_T)
            BusConversion_InsertedFor_Charg->CCSSendWaitTime) {
          rtDW->is_ChargingManage = IN_BCS_BCL_Received;
          rtDW->temporalCounter_i1 = 0U;
          rtDW->En_CRO = false;
          rtDW->En_CCS = true;
        } else {
          guard1 = true;
        }
      } else {
        guard1 = true;
      }
      break;
    }
  }

  if (guard1) {
    if (rtDW->DataTypeConversion43 && (!rtDW->DataTypeConversion38)) {
      rtDW->is_ChargingManage = IN_BCS_Received;
      rtDW->temporalCounter_i1 = 0U;
    }
  }
}

/* Function for Chart: '<S3>/Charge Logic Control' */
static void InsulationMonitoring(const ChrgParaSet
  *BusConversion_InsertedFor_Charg, const boolean_T *DataTypeConversion23, DW
  *rtDW)
{
  rtDW->SysChargSt = 3U;
  if (rtDW->HandShakeFinish && rtDW->IS_MonFinish) {
    if (rtDW->is_InsulationMonitoring == IN_IS_Monitoring_Enable) {
      rtDW->IS_VoltEn = false;
      rtDW->is_InsulationMonitoring = IN_NO_ACTIVE_CHILD;
    } else {
      rtDW->is_InsulationMonitoring = IN_NO_ACTIVE_CHILD;
    }

    rtDW->is_MainBlock = IN_HandshakeIdentification;
    rtDW->SysChargSt = 4U;
    rtDW->HandShakeIdentFinish = false;
    rtDW->is_HandshakeIdentification = IN_Default_l5;
    rtDW->temporalCounter_i1 = 0U;
  } else if (rtDW->IS_ActDisChrgEn) {
    if (rtDW->is_InsulationMonitoring == IN_IS_Monitoring_Enable) {
      rtDW->IS_VoltEn = false;
      rtDW->is_InsulationMonitoring = IN_NO_ACTIVE_CHILD;
    } else {
      rtDW->is_InsulationMonitoring = IN_NO_ACTIVE_CHILD;
    }

    rtDW->is_MainBlock = IN_NO_ACTIVE_CHILD;
    rtDW->is_SystemON = IN_ActiveDischarge;
    rtDW->SysChargSt = 8U;
    rtDW->ActDisChrgSw = false;
    rtDW->is_ActiveDischarge = IN_Default_l5v;
  } else {
    switch (rtDW->is_InsulationMonitoring) {
     case IN_ActDisChrg:
     case IN_End:
      break;

     case IN_Chart:
      if (rtDW->IS_ActDisChrgFinish && (rtDW->temporalCounter_i1 >= (uint32_T)
           BusConversion_InsertedFor_Charg->CHMEndWaitTime)) {
        rtDW->is_InsulationMonitoring = IN_End;
        rtDW->K1K2RlyCtrl = false;
        rtDW->En_CHM = false;
        rtDW->IS_MonFinish = true;
      } else {
        rtDW->is_InsulationMonitoring = IN_Default;
      }
      break;

     case IN_Default:
      if (rtDW->Delay2 < BusConversion_InsertedFor_Charg->InsulationVolt) {
        rtDW->K1K2RlyCtrl = true;
        rtDW->is_InsulationMonitoring = IN_IS_VoltEnable;
        rtDW->IS_VoltEn = true;
      } else if (rtDW->Delay2 >= BusConversion_InsertedFor_Charg->InsulationVolt)
      {
        rtDW->ProcessMod = 11U;
      }
      break;

     case IN_IS_Monitoring_Enable:
      if (rtDW->DataTypeConversion28 == 3) {
        rtDW->ProcessMod = 10U;
      } else if ((rtDW->DataTypeConversion28 == 1) ||
                 (rtDW->DataTypeConversion28 == 2)) {
        rtDW->IS_VoltEn = false;
        rtDW->is_InsulationMonitoring = IN_ActDisChrg;
        rtDW->IS_ActDisChrgEn = true;
      }
      break;

     default:
      /* case IN_IS_VoltEnable: */
      if (*DataTypeConversion23) {
        rtDW->is_InsulationMonitoring = IN_IS_Monitoring_Enable;
      }
      break;
    }
  }
}

/* Function for Chart: '<S3>/Charge Logic Control' */
static void ParameterConfiguration(const real32_T *DiscreteTimeIntegrator_n,
  const real32_T *DiscreteTimeIntegrator, const ChrgParaSet
  *BusConversion_InsertedFor_Charg, DW *rtDW)
{
  rtDW->SysChargSt = 5U;
  if (rtDW->ParaConfigFinish) {
    if (rtDW->is_ParameterConfiguration == IN_Default_l5) {
      rtDW->CtimeEn0 = false;
      rtDW->is_ParameterConfiguration = IN_NO_ACTIVE_CHILD;
    } else {
      rtDW->is_ParameterConfiguration = IN_NO_ACTIVE_CHILD;
    }

    rtDW->is_MainBlock = IN_ChrgingDuration;
    rtDW->SysChargSt = 6U;
    rtDW->ChargeFinish = false;
    rtDW->is_ChrgingDuration = IN_Default_l5v;
  } else {
    switch (rtDW->is_ParameterConfiguration) {
     case IN_BRO_RX_00:
      if (*DiscreteTimeIntegrator >
          BusConversion_InsertedFor_Charg->Time_ReceiveBROAA) {
        rtDW->BROTimeoutAA = true;
        rtDW->ProcessMod = 12U;
      } else if ((rtDW->temporalCounter_i1 >= (uint32_T)
                  BusConversion_InsertedFor_Charg->CROSend1WaitTime) &&
                 (rtDW->DataTypeConversion31 == 170)) {
        rtDW->is_ParameterConfiguration = IN_CRO_Send1;
        rtDW->CtimeEn0 = false;
        rtDW->En_CTS = false;
        rtDW->En_CML = false;
        rtDW->En_CRO = true;
        rtDW->CRO_SPN2830 = 0U;
      }
      break;

     case IN_CRO_Send1:
      rtDW->CRO_SPN2830 = 0U;
      if (!rtDW->DataTypeConversion24) {
        rtDW->is_ParameterConfiguration = IN_PreCharge;
        rtDW->temporalCounter_i1 = 0U;
        rtDW->PreChrgEn = true;
      } else {
        rtDW->ProcessMod = 11U;
      }
      break;

     case IN_CTS_CML_Send:
      if (*DiscreteTimeIntegrator >
          BusConversion_InsertedFor_Charg->Time_ReceiveBRO) {
        rtDW->BROTimeout00 = true;
        rtDW->ProcessMod = 12U;
      } else if (rtDW->DataTypeConversion30) {
        rtDW->is_ParameterConfiguration = IN_BRO_RX_00;
        rtDW->temporalCounter_i1 = 0U;
      }
      break;

     case IN_Default_l5:
      if (*DiscreteTimeIntegrator >
          BusConversion_InsertedFor_Charg->Time_ReceiveBCP) {
        rtDW->BCPTimeout = true;
        rtDW->ProcessMod = 12U;
      } else if (rtDW->DataTypeConversion40 && (rtDW->temporalCounter_i1 >=
                  (uint32_T)BusConversion_InsertedFor_Charg->CRMEndWaitTime)) {
        rtDW->En_CRM = false;
        rtDW->CtimeEn0 = false;
        rtDW->is_ParameterConfiguration = IN_VehParaConfirm;
        rtDW->temporalCounter_i1 = 0U;
      }
      break;

     case IN_K1K2RlyClose:
      rtDW->CRO_SPN2830 = 170U;
      if (rtDW->temporalCounter_i1 >= 1U) {
        rtDW->is_ParameterConfiguration = IN_end;
        rtDW->ParaConfigFinish = true;
      }
      break;

     case IN_PreCharge:
      if ((rtDW->temporalCounter_i1 >= (uint32_T)
           BusConversion_InsertedFor_Charg->CROSend2WaitTime) && (rtDW->Delay2 -
           *DiscreteTimeIntegrator_n <
           BusConversion_InsertedFor_Charg->DeltaVolt_K1K2)) {
        rtDW->is_ParameterConfiguration = IN_K1K2RlyClose;
        rtDW->temporalCounter_i1 = 0U;
        rtDW->CRO_SPN2830 = 170U;
        rtDW->K1K2RlyCtrl = true;
        rtDW->PreChrgEn = false;
      }
      break;

     case IN_VehParaConfirm:
      if (rtDW->temporalCounter_i1 >= (uint32_T)
          BusConversion_InsertedFor_Charg->CTSCMLSendWaitTime) {
        rtDW->is_ParameterConfiguration = IN_CTS_CML_Send;
        rtDW->En_CTS = true;
        rtDW->En_CML = true;
        rtDW->CtimeEn0 = true;
      }
      break;

     default:
      /* case IN_end: */
      break;
    }
  }
}

/* Function for Chart: '<S3>/Charge Logic Control' */
static void exit_internal_MainBlock(DW *rtDW)
{
  switch (rtDW->is_MainBlock) {
   case IN_ChrgingDuration:
    if (rtDW->is_ChrgingDuration == IN_ChargingManage) {
      if (rtDW->is_ChargingManage == IN_ChrgPause) {
        rtDW->CtimeEn2 = false;
        rtDW->is_ChargingManage = IN_NO_ACTIVE_CHILD;
      } else {
        rtDW->is_ChargingManage = IN_NO_ACTIVE_CHILD;
      }

      rtDW->is_ChrgingDuration = IN_NO_ACTIVE_CHILD;
    } else {
      rtDW->is_ChrgingDuration = IN_NO_ACTIVE_CHILD;
    }

    rtDW->En_CCS = false;
    rtDW->is_MainBlock = IN_NO_ACTIVE_CHILD;
    break;

   case IN_HandShake:
    if (rtDW->is_HandShake == IN_CHM_Send) {
      rtDW->CtimeEn0 = false;
      rtDW->is_HandShake = IN_NO_ACTIVE_CHILD;
    } else {
      rtDW->is_HandShake = IN_NO_ACTIVE_CHILD;
    }

    rtDW->is_MainBlock = IN_NO_ACTIVE_CHILD;
    break;

   case IN_HandshakeIdentification:
    if (rtDW->is_HandshakeIdentification == IN_CRM_Send1) {
      rtDW->CtimeEn0 = false;
      rtDW->ReTrySt = false;
      rtDW->is_HandshakeIdentification = IN_NO_ACTIVE_CHILD;
    } else {
      rtDW->is_HandshakeIdentification = IN_NO_ACTIVE_CHILD;
    }

    rtDW->is_MainBlock = IN_NO_ACTIVE_CHILD;
    break;

   case IN_InsulationMonitoring:
    if (rtDW->is_InsulationMonitoring == IN_IS_Monitoring_Enable) {
      rtDW->IS_VoltEn = false;
      rtDW->is_InsulationMonitoring = IN_NO_ACTIVE_CHILD;
    } else {
      rtDW->is_InsulationMonitoring = IN_NO_ACTIVE_CHILD;
    }

    rtDW->is_MainBlock = IN_NO_ACTIVE_CHILD;
    break;

   case IN_ParameterConfiguration:
    if (rtDW->is_ParameterConfiguration == IN_Default_l5) {
      rtDW->CtimeEn0 = false;
      rtDW->is_ParameterConfiguration = IN_NO_ACTIVE_CHILD;
    } else {
      rtDW->is_ParameterConfiguration = IN_NO_ACTIVE_CHILD;
    }

    rtDW->is_MainBlock = IN_NO_ACTIVE_CHILD;
    break;

   case IN_PhyConnConfirm:
    if (rtDW->is_PhyConnConfirm == IN_ELockClose) {
      rtDW->CtimeEn0 = false;
      rtDW->is_PhyConnConfirm = IN_NO_ACTIVE_CHILD;
    } else {
      rtDW->is_PhyConnConfirm = IN_NO_ACTIVE_CHILD;
    }

    rtDW->is_MainBlock = IN_NO_ACTIVE_CHILD;
    break;
  }
}

/* Function for Chart: '<S3>/Charge Logic Control' */
static void MainBlock(const real32_T *DiscreteTimeIntegrator_n, const real32_T
                      *DiscreteTimeIntegrator, const real32_T
                      *DiscreteTimeIntegrator1, const real32_T
                      *DiscreteTimeIntegrator2, const ChrgParaSet
                      *BusConversion_InsertedFor_Charg, const boolean_T
                      *DataTypeConversion23, DW *rtDW)
{
  int32_T tmp;
  boolean_T guard1 = false;
  guard1 = false;
  if ((rtDW->ProcessMod == 0xA) || (rtDW->ProcessMod == 0xB)) {
    guard1 = true;
  } else if (rtDW->ProcessMod == 0xC) {
    exit_internal_MainBlock(rtDW);
    rtDW->En_CEM = true;
    rtDW->is_SystemON = IN_PowerDown;
    rtDW->SysChargSt = 7U;
    enter_internal_PowerDown(rtDW);
  } else if (rtDW->ChargeFinish) {
    guard1 = true;
  } else if (rtDW->DataTypeConversion37 && (!rtDW->ReTrySt)) {
    rtDW->ProcessMod = 12U;
    guard1 = true;
  } else if ((rtDW->LogicalOperator && rtDW->DataTypeConversion36) || ((rtDW->U1
    != 4.0F) && rtDW->PhyConnFinish) || (rtDW->BHMTimeout &&
              (rtDW->TimeoutCouter >
               BusConversion_InsertedFor_Charg->SendCHMTimes)) ||
             rtDW->LogicalOperator_k) {
    rtDW->ProcessMod = 11U;
    guard1 = true;
  } else if (rtDW->DataTypeConversion36 && rtDW->LogicalOperator1) {
    rtDW->ProcessMod = 11U;
    rtDW->BST_Stop = true;
    guard1 = true;
  } else {
    switch (rtDW->is_MainBlock) {
     case IN_ChrgingDuration:
      ChrgingDuration(DiscreteTimeIntegrator, DiscreteTimeIntegrator1,
                      DiscreteTimeIntegrator2, BusConversion_InsertedFor_Charg,
                      rtDW);
      break;

     case IN_HandShake:
      rtDW->SysChargSt = 2U;
      if (rtDW->HandShakeFinish) {
        if (rtDW->is_HandShake == IN_CHM_Send) {
          rtDW->CtimeEn0 = false;
          rtDW->is_HandShake = IN_NO_ACTIVE_CHILD;
        } else {
          rtDW->is_HandShake = IN_NO_ACTIVE_CHILD;
        }

        rtDW->is_MainBlock = IN_InsulationMonitoring;
        rtDW->SysChargSt = 3U;
        rtDW->IS_MonFinish = false;
        rtDW->is_InsulationMonitoring = IN_Chart;
        rtDW->temporalCounter_i1 = 0U;
      } else {
        switch (rtDW->is_HandShake) {
         case IN_CHM_Send:
          if (rtDW->DataTypeConversion29) {
            rtDW->CtimeEn0 = false;
            rtDW->is_HandShake = IN_InsulationEnable;
            rtDW->HandShakeFinish = true;
          } else if (*DiscreteTimeIntegrator >=
                     BusConversion_InsertedFor_Charg->Time_HandShake) {
            rtDW->BHMTimeout = true;
            tmp = rtDW->TimeoutCouter + 1;
            if (rtDW->TimeoutCouter + 1 > 255) {
              tmp = 255;
            }

            rtDW->TimeoutCouter = (uint8_T)tmp;
            rtDW->is_HandShake = IN_WaitForBHM;
            rtDW->temporalCounter_i1 = 0U;
            rtDW->CtimeEn0 = true;

            /* BHM Time Counter */
          }
          break;

         case IN_InsulationEnable:
          break;

         default:
          /* case IN_WaitForBHM: */
          if (rtDW->temporalCounter_i1 >= (uint32_T)
              BusConversion_InsertedFor_Charg->CHMSendWaitTime) {
            /* T4 */
            rtDW->is_HandShake = IN_CHM_Send;
            rtDW->En_CHM = true;
          }
          break;
        }
      }
      break;

     case IN_HandshakeIdentification:
      rtDW->SysChargSt = 4U;
      if (rtDW->HandShakeIdentFinish) {
        if (rtDW->is_HandshakeIdentification == IN_CRM_Send1) {
          rtDW->CtimeEn0 = false;
          rtDW->ReTrySt = false;
          rtDW->is_HandshakeIdentification = IN_NO_ACTIVE_CHILD;
        } else {
          rtDW->is_HandshakeIdentification = IN_NO_ACTIVE_CHILD;
        }

        rtDW->is_MainBlock = IN_ParameterConfiguration;
        rtDW->SysChargSt = 5U;
        rtDW->ParaConfigFinish = false;
        rtDW->HandShakeIdentFinish = false;
        rtDW->is_ParameterConfiguration = IN_Default_l5;
        rtDW->temporalCounter_i1 = 0U;
      } else {
        switch (rtDW->is_HandshakeIdentification) {
         case IN_BRMReceived:
          if (rtDW->temporalCounter_i1 >= (uint32_T)
              BusConversion_InsertedFor_Charg->CRMSend2WaitTime) {
            rtDW->is_HandshakeIdentification = IN_CRM_Send2;
            rtDW->temporalCounter_i1 = 0U;
            rtDW->En_CEM = false;
            rtDW->BRMTimeout = false;
            rtDW->BSTTimeout = false;
            rtDW->BROTimeoutAA = false;
            rtDW->BROTimeout00 = false;
            rtDW->BCSTimeout = false;
            rtDW->BCPTimeout = false;
            rtDW->BCLTimeout = false;
            rtDW->CRM_SPN2560 = 170U;
            rtDW->CtimeEn0 = true;
          }
          break;

         case IN_CRM_Send1:
          rtDW->CRM_SPN2560 = 0U;
          if (rtDW->DataTypeConversion25) {
            rtDW->CtimeEn0 = false;
            rtDW->ReTrySt = false;
            rtDW->is_HandshakeIdentification = IN_BRMReceived;
            rtDW->temporalCounter_i1 = 0U;
          } else if (*DiscreteTimeIntegrator >=
                     BusConversion_InsertedFor_Charg->Time_HSIdentification) {
            rtDW->BRMTimeout = true;
            rtDW->ProcessMod = 12U;
          }
          break;

         case IN_CRM_Send2:
          rtDW->CRM_SPN2560 = 170U;
          if (rtDW->temporalCounter_i1 >= 1U) {
            rtDW->is_HandshakeIdentification = IN_end_d;
            rtDW->HandShakeIdentFinish = true;
          }
          break;

         case IN_Default_l5:
          if (rtDW->temporalCounter_i1 >= (uint32_T)
              BusConversion_InsertedFor_Charg->CRMSend1WaitTime) {
            rtDW->is_HandshakeIdentification = IN_CRM_Send1;
            rtDW->En_CST = false;
            rtDW->En_CRM = true;
            rtDW->CRM_SPN2560 = 0U;
            rtDW->CtimeEn0 = true;
          }
          break;

         default:
          /* case IN_end: */
          break;
        }
      }
      break;

     case IN_InsulationMonitoring:
      InsulationMonitoring(BusConversion_InsertedFor_Charg, DataTypeConversion23,
                           rtDW);
      break;

     case IN_ParameterConfiguration:
      ParameterConfiguration(DiscreteTimeIntegrator_n, DiscreteTimeIntegrator,
        BusConversion_InsertedFor_Charg, rtDW);
      break;

     default:
      /* case IN_PhyConnConfirm: */
      rtDW->SysChargSt = 1U;
      if (rtDW->PhyConnFinish) {
        if (rtDW->is_PhyConnConfirm == IN_ELockClose) {
          rtDW->is_PhyConnConfirm = IN_NO_ACTIVE_CHILD;
        } else {
          rtDW->is_PhyConnConfirm = IN_NO_ACTIVE_CHILD;
        }

        rtDW->is_MainBlock = IN_HandShake;
        rtDW->SysChargSt = 2U;
        rtDW->TimeoutCouter = 0U;
        rtDW->HandShakeFinish = false;
        rtDW->is_HandShake = IN_WaitForBHM;
        rtDW->temporalCounter_i1 = 0U;
        rtDW->CtimeEn0 = true;

        /* BHM Time Counter */
      } else {
        switch (rtDW->is_PhyConnConfirm) {
         case IN_Default_l:
          if (rtDW->U1 == 4.0F) {
            rtDW->is_PhyConnConfirm = IN_ELockClose;
            rtDW->CtimeEn0 = true;

            /* Elock Time Counter */
            rtDW->ELockCtrl = true;
          }
          break;

         case IN_ELockClose:
          if (rtDW->ELockSt && (rtDW->U1 == 4.0F)) {
            rtDW->CtimeEn0 = false;
            rtDW->is_PhyConnConfirm = IN_K3K4RlyClose;
            rtDW->temporalCounter_i1 = 0U;
            rtDW->K3K4RlyCtrl = true;
          } else if (*DiscreteTimeIntegrator >=
                     BusConversion_InsertedFor_Charg->Time_PhyCon) {
            rtDW->ProcessMod = 11U;
          } else if (rtDW->U1 != 4.0F) {
            rtDW->CtimeEn0 = false;
            rtDW->is_PhyConnConfirm = IN_Default_l;
          }
          break;

         case IN_End_h:
          break;

         default:
          /* case IN_K3K4RlyClose: */
          if ((!rtDW->ELockSt) || (rtDW->U1 != 4.0F)) {
            rtDW->is_PhyConnConfirm = IN_ELockClose;
            rtDW->CtimeEn0 = true;

            /* Elock Time Counter */
            rtDW->ELockCtrl = true;
          } else if (rtDW->temporalCounter_i1 >= 1U) {
            rtDW->is_PhyConnConfirm = IN_End_h;
            rtDW->PhyConnFinish = true;
          }
          break;
        }
      }
      break;
    }
  }

  if (guard1) {
    exit_internal_MainBlock(rtDW);
    rtDW->is_SystemON = IN_PowerDown;
    rtDW->SysChargSt = 7U;
    enter_internal_PowerDown(rtDW);
  }
}

/* Function for Chart: '<S3>/Charge Logic Control' */
static void exit_internal_PowerDown(DW *rtDW)
{
  switch (rtDW->is_PowerDown) {
   case IN_ChargeStop:
    rtDW->CtimeEn2 = false;
    rtDW->is_PowerDown = IN_NO_ACTIVE_CHILD;
    break;

   case IN_WaitForBSD:
    rtDW->CtimeEn1 = false;
    rtDW->is_PowerDown = IN_NO_ACTIVE_CHILD;
    break;

   case IN_WaitForBST:
    rtDW->CtimeEn0 = false;
    rtDW->is_PowerDown = IN_NO_ACTIVE_CHILD;
    break;

   default:
    rtDW->is_PowerDown = IN_NO_ACTIVE_CHILD;
    break;
  }
}

/* Function for Chart: '<S3>/Charge Logic Control' */
static void enter_atomic_Sleep(DW *rtDW)
{
  rtDW->SysChargSt = 0U;
  rtDW->PowerDownInitFinish = false;
  rtDW->BST_Stop = false;
  rtDW->ProcessMod = 0U;
  rtDW->CtimeEn0 = false;
  rtDW->CtimeEn1 = false;
  rtDW->CtimeEn2 = false;
  rtDW->En_CHM = false;
  rtDW->En_CRM = false;
  rtDW->En_CTS = false;
  rtDW->En_CML = false;
  rtDW->En_CRO = false;
  rtDW->En_CCS = false;
  rtDW->En_CST = false;
  rtDW->En_CSD = false;
  rtDW->CRO_SPN2830 = 0U;
  rtDW->CRM_SPN2560 = 0U;
  rtDW->ActDisChrgSw = false;
  rtDW->IS_VoltEn = false;
  rtDW->PD_ActDisChrgFinish = false;
  rtDW->PreChrgEn = false;
  rtDW->PhyConnFinish = false;
  rtDW->K1K2RlyCtrl = false;
  rtDW->K3K4RlyCtrl = false;
  rtDW->ELockCtrl = false;
  rtDW->En_CEM = false;
  rtDW->ChargeEnable = false;
  rtDW->TimeoutCouter = 0U;
  rtDW->ReTrySt = false;
  rtDW->ParaConfigFinish = false;
  rtDW->IS_MonFinish = false;
  rtDW->IS_ActDisChrgFinish = false;
  rtDW->IS_ActDisChrgEn = false;
  rtDW->HandShakeIdentFinish = false;
  rtDW->HandShakeFinish = false;
  rtDW->BRMTimeout = false;
  rtDW->ChargeFinish = false;
  rtDW->BSTTimeout = false;
  rtDW->BSDTimeout = false;
  rtDW->BROTimeoutAA = false;
  rtDW->BROTimeout00 = false;
  rtDW->BHMTimeout = false;
  rtDW->BCSTimeout = false;
  rtDW->BCPTimeout = false;
  rtDW->BCLTimeout = false;
  rtDW->PD_ActDisChrgEn = false;
}

/* Function for Chart: '<S3>/Charge Logic Control' */
static void PowerDown(const real32_T *DiscreteTimeIntegrator, const real32_T
                      *DiscreteTimeIntegrator1, const boolean_T
                      *DataTypeConversion39, const real32_T
                      *DiscreteTimeIntegrator2, const ChrgParaSet
                      *BusConversion_InsertedFor_Charg, const real32_T *Delay4,
                      boolean_T rtU_DCPlugIn, DW *rtDW)
{
  int32_T tmp;
  boolean_T guard1 = false;
  rtDW->SysChargSt = 7U;
  if (rtDW->ReTrySt) {
    exit_internal_PowerDown(rtDW);
    rtDW->PD_ActDisChrgFinish = false;
    rtDW->ProcessMod = 0U;
    rtDW->is_SystemON = IN_MainBlock;
    rtDW->is_MainBlock = IN_HandshakeIdentification;
    rtDW->SysChargSt = 4U;
    rtDW->HandShakeIdentFinish = false;
    rtDW->is_HandshakeIdentification = IN_Default_l5;
    rtDW->temporalCounter_i1 = 0U;
  } else if (rtDW->PD_ActDisChrgEn) {
    exit_internal_PowerDown(rtDW);
    rtDW->is_SystemON = IN_ActiveDischarge;
    rtDW->SysChargSt = 8U;
    rtDW->ActDisChrgSw = false;
    rtDW->is_ActiveDischarge = IN_Default_l5v;
  } else if (!rtU_DCPlugIn) {
    exit_internal_PowerDown(rtDW);
    rtDW->is_SystemON = IN_Sleep;
    enter_atomic_Sleep(rtDW);
  } else {
    guard1 = false;
    switch (rtDW->is_PowerDown) {
     case IN_ActDisCharge:
     case IN_Sleep_a:
      break;

     case IN_BSDRecieved:
      if (rtDW->temporalCounter_i1 >= 300U) {
        rtDW->is_PowerDown = IN_K3K4RlyOpen;
        rtDW->temporalCounter_i1 = 0U;
        rtDW->En_CSD = false;
        rtDW->K3K4RlyCtrl = false;
        rtDW->ELockCtrl = false;
        rtDW->En_CST = false;
        rtDW->En_CEM = false;
      }
      break;

     case IN_ChargeStop:
      if (*DiscreteTimeIntegrator2 >
          BusConversion_InsertedFor_Charg->Time_CurrentDown) {
        rtDW->ProcessMod = 13U;
        rtDW->CtimeEn2 = false;
        rtDW->is_PowerDown = IN_ChargeStop;
        rtDW->ChargeEnable = false;
      } else if ((*Delay4 < BusConversion_InsertedFor_Charg->Cur_Threshold) ||
                 (rtDW->ProcessMod == 0xD)) {
        rtDW->CtimeEn2 = false;
        rtDW->is_PowerDown = IN_K1K2RlyOpen;
        rtDW->K1K2RlyCtrl = false;
      }
      break;

     case IN_ChrgPreStop:
      if ((rtDW->ProcessMod == 0xB) || (rtDW->ChargeFinish && rtDW->BST_Stop)) {
        rtDW->CtimeEn2 = true;
        rtDW->is_PowerDown = IN_ChargeStop;
        rtDW->ChargeEnable = false;
      } else if (rtDW->ChargeFinish) {
        rtDW->is_PowerDown = IN_WaitForBST;
        rtDW->CtimeEn0 = true;
        rtDW->CtimeEn1 = true;
        rtDW->CtimeEn2 = true;
      }
      break;

     case IN_K1K2RlyOpen:
      if ((rtDW->ProcessMod == 0xD) || (rtDW->ProcessMod == 0xA)) {
        rtDW->is_PowerDown = IN_K3K4RlyOpen;
        rtDW->temporalCounter_i1 = 0U;
        rtDW->En_CSD = false;
        rtDW->K3K4RlyCtrl = false;
        rtDW->ELockCtrl = false;
        rtDW->En_CST = false;
        rtDW->En_CEM = false;
      } else if ((rtDW->ProcessMod == 0xC) && (rtDW->TimeoutCouter <
                  BusConversion_InsertedFor_Charg->SendCHMTimes)) {
        tmp = rtDW->TimeoutCouter + 1;
        if (rtDW->TimeoutCouter + 1 > 255) {
          tmp = 255;
        }

        rtDW->TimeoutCouter = (uint8_T)tmp;
        rtDW->ReTrySt = true;
      } else {
        if ((rtDW->ProcessMod == 0xC) && (rtDW->TimeoutCouter ==
             BusConversion_InsertedFor_Charg->SendCHMTimes)) {
          rtDW->ProcessMod = 11U;
        }

        rtDW->is_PowerDown = IN_ActDisCharge;
        rtDW->PD_ActDisChrgEn = true;
      }
      break;

     case IN_K3K4RlyOpen:
      if (rtDW->temporalCounter_i1 >= 500U) {
        rtDW->is_PowerDown = IN_Sleep_a;
      }
      break;

     case IN_PowerDownInit:
      rtDW->CRO_SPN2830 = 0U;
      rtDW->CRM_SPN2560 = 0U;
      if (rtDW->PD_ActDisChrgFinish && rtDW->PowerDownInitFinish) {
        if (rtDW->ProcessMod == 0xB) {
          rtDW->is_PowerDown = IN_K3K4RlyOpen;
          rtDW->temporalCounter_i1 = 0U;
          rtDW->En_CSD = false;
          rtDW->K3K4RlyCtrl = false;
          rtDW->ELockCtrl = false;
          rtDW->En_CST = false;
          rtDW->En_CEM = false;
        } else {
          rtDW->is_PowerDown = IN_WaitForBSD;
          rtDW->temporalCounter_i1 = 0U;
        }
      } else if ((rtDW->ProcessMod == 0xA) && rtDW->PowerDownInitFinish) {
        rtDW->is_PowerDown = IN_K1K2RlyOpen;
        rtDW->K1K2RlyCtrl = false;
      } else if ((rtDW->temporalCounter_i1 >= 1U) && rtDW->PowerDownInitFinish)
      {
        if ((rtDW->ChargeFinish || (rtDW->ProcessMod == 0xB)) &&
            (rtDW->temporalCounter_i1 >= (uint32_T)
             BusConversion_InsertedFor_Charg->CSTSendWaitTime)) {
          rtDW->is_PowerDown = IN_ChrgPreStop;
          rtDW->En_CST = true;
        } else if ((rtDW->ProcessMod == 0xC) && rtDW->PowerDownInitFinish) {
          rtDW->CtimeEn2 = true;
          rtDW->is_PowerDown = IN_ChargeStop;
          rtDW->ChargeEnable = false;
        }
      }
      break;

     case IN_WaitForBSD:
      if (*DiscreteTimeIntegrator1 >
          BusConversion_InsertedFor_Charg->Time_PDReceiveBSD) {
        rtDW->BSDTimeout = true;
        rtDW->En_CEM = true;
        rtDW->ProcessMod = 10U;
        rtDW->CtimeEn1 = false;
        rtDW->is_PowerDown = IN_ChargeStop;
        rtDW->ChargeEnable = false;
      } else if ((rtDW->temporalCounter_i1 >= (uint32_T)
                  BusConversion_InsertedFor_Charg->CSDSendWaitTime) &&
                 (*DataTypeConversion39)) {
        rtDW->CtimeEn1 = false;
        rtDW->is_PowerDown = IN_BSDRecieved;
        rtDW->temporalCounter_i1 = 0U;
        rtDW->En_CSD = true;
      }
      break;

     default:
      /* case IN_WaitForBST: */
      if (rtDW->DataTypeConversion36) {
        rtDW->En_CSD = true;
        guard1 = true;
      } else if (*DiscreteTimeIntegrator >
                 BusConversion_InsertedFor_Charg->Time_ReceiveBST) {
        rtDW->BSTTimeout = true;
        rtDW->ProcessMod = 13U;
        rtDW->En_CEM = true;
        guard1 = true;
      }
      break;
    }

    if (guard1) {
      rtDW->CtimeEn0 = false;
      rtDW->is_PowerDown = IN_ChargeStop;
      rtDW->ChargeEnable = false;
    }
  }
}

/* Function for Chart: '<S3>/Charge Logic Control' */
static void enter_atomic_SystemOFF(DW *rtDW)
{
  rtDW->SysChargSt = 99U;
  rtDW->ProcessMod = 0U;
  rtDW->CtimeEn0 = false;
  rtDW->CtimeEn1 = false;
  rtDW->BST_Stop = false;
  rtDW->CtimeEn2 = false;
  rtDW->En_CHM = false;
  rtDW->En_CRM = false;
  rtDW->En_CTS = false;
  rtDW->En_CML = false;
  rtDW->En_CRO = false;
  rtDW->En_CCS = false;
  rtDW->En_CST = false;
  rtDW->En_CSD = false;
  rtDW->CRO_SPN2830 = 0U;
  rtDW->CRM_SPN2560 = 0U;
  rtDW->ActDisChrgSw = false;
  rtDW->IS_VoltEn = false;
  rtDW->PreChrgEn = false;
  rtDW->PhyConnFinish = false;
  rtDW->K1K2RlyCtrl = false;
  rtDW->K3K4RlyCtrl = false;
  rtDW->ELockCtrl = false;
  rtDW->En_CEM = false;
  rtDW->ChargeEnable = false;
  rtDW->TimeoutCouter = 0U;
  rtDW->ReTrySt = false;
  rtDW->PD_ActDisChrgFinish = false;
  rtDW->ParaConfigFinish = false;
  rtDW->IS_MonFinish = false;
  rtDW->IS_ActDisChrgFinish = false;
  rtDW->IS_ActDisChrgEn = false;
  rtDW->HandShakeIdentFinish = false;
  rtDW->HandShakeFinish = false;
  rtDW->BRMTimeout = false;
  rtDW->ChargeFinish = false;
  rtDW->BSTTimeout = false;
  rtDW->BSDTimeout = false;
  rtDW->BROTimeoutAA = false;
  rtDW->BROTimeout00 = false;
  rtDW->BHMTimeout = false;
  rtDW->BCSTimeout = false;
  rtDW->BCPTimeout = false;
  rtDW->BCLTimeout = false;
  rtDW->PD_ActDisChrgEn = false;
  rtDW->PowerDownInitFinish = false;
}

/* Model step function for TID0 */
void DCHCode_step0(RT_MODEL *const rtM) /* Sample time: [0.01s, 0.0s] */
{
  /* (no output/update code required) */
  UNUSED_PARAMETER(rtM);
}

/* Model step function for TID1 */
void DCHCode_step1(RT_MODEL *const rtM, BusB *rtU_BattFault, BusC *rtU_BMSFault,
                   BusA *rtU_BMSData, uint8_T rtU_OFC_Key, boolean_T
                   rtU_DCPlugIn, real32_T rtU_VCUActuator_DIG_IN_DCPRLY_OV,
                   real32_T *rtY_ChrgCur, real32_T *rtY_ChrgVolt, real32_T
                   *rtY_OFC_MinChrgVolt, real32_T *rtY_OFC_MinChrgCur, real32_T *
                   rtY_OFC_MaxChrgCur, real32_T *rtY_OFC_MaxChrgVolt, real32_T
                   *rtY_Volt_K1K2_ChrgSide, real32_T *rtY_Volt_K1K2_VehSide,
                   boolean_T *rtY_CC2_Ready, real32_T *rtY_K1K2RlyCtrl, real32_T
                   *rtY_K3K4RlyCtrl, real32_T *rtY_SysChargeSt, real32_T
                   *rtY_ProcessMod, real32_T *rtY_ChargeEnble, real32_T
                   *rtY_ChargingTime, CHM *rtY_CHM_e, CRM *rtY_CRM_c, CTS
                   *rtY_CTS_g, CML *rtY_CML_c, CRO *rtY_CRO_c, CCS *rtY_CCS_k,
                   CSD *rtY_CSD_e, CEM *rtY_CEM_b, CST *rtY_CST_k) /* Sample time: [1.0s, 0.0s] */
{
  DW *rtDW = rtM->dwork;
  int32_T rtb_Switch1;
  real32_T Delay4;
  real32_T DiscreteTimeIntegrator;
  real32_T DiscreteTimeIntegrator1;
  real32_T DiscreteTimeIntegrator2;
  real32_T rtb_Abs2;
  real32_T rtb_En_CHM;
  real32_T rtb_En_CRM;
  uint8_T rtb_Compare;
  boolean_T DataTypeConversion23;
  boolean_T DataTypeConversion39;
  boolean_T LogicalOperator_k_tmp;
  boolean_T guard1 = false;
  boolean_T rtb_LogicalOperator2;
  boolean_T rtb_UnitDelay;
  boolean_T rtb_UnitDelay1;
  boolean_T rtb_UnitDelay2;

  /* Outputs for Atomic SubSystem: '<Root>/SoftOFC' */
  /* Abs: '<S5>/Abs2' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  rtb_Abs2 = (real32_T)fabs(rtU_BMSData->PackCur);

  /* UnitDelay: '<S39>/Unit Delay' */
  rtb_UnitDelay = rtDW->UnitDelay_DSTATE;

  /* DiscreteIntegrator: '<S39>/Discrete-Time Integrator' incorporates:
   *  UnitDelay: '<S39>/Unit Delay'
   */
  if (rtDW->UnitDelay_DSTATE || (rtDW->DiscreteTimeIntegrator_PrevRese != 0)) {
    rtDW->DiscreteTimeIntegrator_DSTATE_l = 0.0F;
  }

  /* DiscreteIntegrator: '<S39>/Discrete-Time Integrator' */
  DiscreteTimeIntegrator = rtDW->DiscreteTimeIntegrator_DSTATE_l;

  /* UnitDelay: '<S39>/Unit Delay1' */
  rtb_UnitDelay1 = rtDW->UnitDelay1_DSTATE;

  /* DiscreteIntegrator: '<S39>/Discrete-Time Integrator1' incorporates:
   *  UnitDelay: '<S39>/Unit Delay1'
   */
  if (rtDW->UnitDelay1_DSTATE || (rtDW->DiscreteTimeIntegrator1_PrevRes != 0)) {
    rtDW->DiscreteTimeIntegrator1_DSTATE = 0.0F;
  }

  /* DiscreteIntegrator: '<S39>/Discrete-Time Integrator1' */
  DiscreteTimeIntegrator1 = rtDW->DiscreteTimeIntegrator1_DSTATE;

  /* UnitDelay: '<S39>/Unit Delay2' */
  rtb_UnitDelay2 = rtDW->UnitDelay2_DSTATE;

  /* DiscreteIntegrator: '<S39>/Discrete-Time Integrator2' incorporates:
   *  UnitDelay: '<S39>/Unit Delay2'
   */
  if (rtDW->UnitDelay2_DSTATE || (rtDW->DiscreteTimeIntegrator2_PrevRes != 0)) {
    rtDW->DiscreteTimeIntegrator2_DSTATE = 0.0F;
  }

  /* DiscreteIntegrator: '<S39>/Discrete-Time Integrator2' */
  DiscreteTimeIntegrator2 = rtDW->DiscreteTimeIntegrator2_DSTATE;

  /* BusCreator generated from: '<S3>/Charge Logic Control' incorporates:
   *  Constant: '<S38>/CCSSendWaitTime[ms]'
   *  Constant: '<S38>/CHMEndWaitTime[ms]'
   *  Constant: '<S38>/CHMSendWaitTime[ms]'
   *  Constant: '<S38>/CRMEndWaitTime[ms]'
   *  Constant: '<S38>/CRMSend1WaitTime[ms]'
   *  Constant: '<S38>/CRMSend2WaitTime[ms]'
   *  Constant: '<S38>/CROSend1WaitTime[ms]'
   *  Constant: '<S38>/CROSend2WaitTime[ms]'
   *  Constant: '<S38>/CSDSendWaitTime[ms]'
   *  Constant: '<S38>/CSTSendWaitTime[ms]'
   *  Constant: '<S38>/CTSCMLSendWaitTime[ms]'
   *  Constant: '<S38>/ChgSideVolt_K1K2[V]'
   *  Constant: '<S38>/Cur_Threshold[A]'
   *  Constant: '<S38>/DeltaVolt_K1K2[V]'
   *  Constant: '<S38>/InsulationVolt[V]'
   *  Constant: '<S38>/SendCHMTimes'
   *  Constant: '<S38>/Time_ADReceiveBSD[s]'
   *  Constant: '<S38>/Time_ChrgWait[s]'
   *  Constant: '<S38>/Time_CurrentDown[s]'
   *  Constant: '<S38>/Time_HSIdentification[s]'
   *  Constant: '<S38>/Time_HandShake[s]'
   *  Constant: '<S38>/Time_PDReceiveBSD[s]'
   *  Constant: '<S38>/Time_PhyCon[s]'
   *  Constant: '<S38>/Time_ReceiveActDisChrg[s]'
   *  Constant: '<S38>/Time_ReceiveBCL[s]'
   *  Constant: '<S38>/Time_ReceiveBCP[s]'
   *  Constant: '<S38>/Time_ReceiveBCS[s]'
   *  Constant: '<S38>/Time_ReceiveBROAA[s]'
   *  Constant: '<S38>/Time_ReceiveBRO[s]'
   *  Constant: '<S38>/Time_ReceiveBST[s]'
   */
  rtDW->BusConversion_InsertedFor_Charg.Time_PhyCon = 5.0F;
  rtDW->BusConversion_InsertedFor_Charg.Time_HandShake = 5.0F;
  rtDW->BusConversion_InsertedFor_Charg.Time_HSIdentification = 5.0F;
  rtDW->BusConversion_InsertedFor_Charg.Time_ReceiveBCP = 5.0F;
  rtDW->BusConversion_InsertedFor_Charg.Time_ReceiveBRO = 5.0F;
  rtDW->BusConversion_InsertedFor_Charg.Time_ReceiveBROAA = 60.0F;
  rtDW->BusConversion_InsertedFor_Charg.Time_ChrgWait = 600.0F;
  rtDW->BusConversion_InsertedFor_Charg.Time_ReceiveBCL = 1.0F;
  rtDW->BusConversion_InsertedFor_Charg.Time_ReceiveBCS = 5.0F;
  rtDW->BusConversion_InsertedFor_Charg.Time_ReceiveActDisChrg = 5.0F;
  rtDW->BusConversion_InsertedFor_Charg.Time_ADReceiveBSD = 10.0F;
  rtDW->BusConversion_InsertedFor_Charg.Time_PDReceiveBSD = 10.0F;
  rtDW->BusConversion_InsertedFor_Charg.Time_ReceiveBST = 5.0F;
  rtDW->BusConversion_InsertedFor_Charg.Time_CurrentDown = 0.1F;
  rtDW->BusConversion_InsertedFor_Charg.InsulationVolt = 10.0F;
  rtDW->BusConversion_InsertedFor_Charg.DeltaVolt_K1K2 = 10.0F;
  rtDW->BusConversion_InsertedFor_Charg.ChgSideVolt_K1K2 = 60.0F;
  rtDW->BusConversion_InsertedFor_Charg.Cur_Threshold = 5.0F;
  rtDW->BusConversion_InsertedFor_Charg.SendCHMTimes = 3.0F;
  rtDW->BusConversion_InsertedFor_Charg.CHMSendWaitTime = 1.0F;
  rtDW->BusConversion_InsertedFor_Charg.CHMEndWaitTime = 1.0F;
  rtDW->BusConversion_InsertedFor_Charg.CRMSend1WaitTime = 1.0F;
  rtDW->BusConversion_InsertedFor_Charg.CRMSend2WaitTime = 1.0F;
  rtDW->BusConversion_InsertedFor_Charg.CRMEndWaitTime = 1.0F;
  rtDW->BusConversion_InsertedFor_Charg.CTSCMLSendWaitTime = 1.0F;
  rtDW->BusConversion_InsertedFor_Charg.CROSend1WaitTime = 500.0F;
  rtDW->BusConversion_InsertedFor_Charg.CROSend2WaitTime = 1.0F;
  rtDW->BusConversion_InsertedFor_Charg.CCSSendWaitTime = 1.0F;
  rtDW->BusConversion_InsertedFor_Charg.CSTSendWaitTime = 1.0F;
  rtDW->BusConversion_InsertedFor_Charg.CSDSendWaitTime = 1.0F;

  /* Chart: '<S8>/Plug Logic' incorporates:
   *  Delay: '<S8>/Delay3'
   *  Inport: '<Root>/DCPlugIn'
   */
  if (rtDW->temporalCounter_i1_j < 7U) {
    rtDW->temporalCounter_i1_j++;
  }

  if (rtDW->is_active_c4_DCHCode == 0U) {
    rtDW->is_active_c4_DCHCode = 1U;
    rtDW->is_c4_DCHCode = IN_Init;
    rtDW->ELockSt = false;
    rtDW->U1 = 6.0F;
    rtDW->CC2_Ready = false;
  } else {
    guard1 = false;
    switch (rtDW->is_c4_DCHCode) {
     case IN_CC_Confirm:
      if (rtDW->temporalCounter_i1_j >= 5) {
        rtDW->is_c4_DCHCode = IN_S_Open;
        rtDW->temporalCounter_i1_j = 0U;
        rtDW->U1 = 4.0F;
      } else if (!rtU_DCPlugIn) {
        rtDW->U1 = 12.0F;
        rtDW->CC2_Ready = false;
        if (rtDW->temporalCounter_i1_j >= 5) {
          rtDW->is_c4_DCHCode = IN_Init;
          rtDW->ELockSt = false;
          rtDW->U1 = 6.0F;
          rtDW->CC2_Ready = false;
        }
      }
      break;

     case IN_ELock:
      if (!rtU_DCPlugIn) {
        rtDW->U1 = 6.0F;
        if (rtDW->Delay3_DSTATE == 0.0) {
          rtDW->ELockSt = false;
          if (rtDW->temporalCounter_i1_j >= 5) {
            rtDW->U1 = 12.0F;
            rtDW->CC2_Ready = false;
            if (rtDW->temporalCounter_i1_j >= 5) {
              rtDW->is_c4_DCHCode = IN_Init;
              rtDW->ELockSt = false;
              rtDW->U1 = 6.0F;
              rtDW->CC2_Ready = false;
            }
          }
        }
      }
      break;

     case IN_Init:
      if (rtU_DCPlugIn) {
        rtDW->is_c4_DCHCode = IN_S_Close;
        rtDW->temporalCounter_i1_j = 0U;
        rtDW->U1 = 12.0F;
      }
      break;

     case IN_S_Close:
      if (rtDW->temporalCounter_i1_j >= 5) {
        rtDW->is_c4_DCHCode = IN_CC_Confirm;
        rtDW->temporalCounter_i1_j = 0U;
        rtDW->U1 = 6.0F;
        rtDW->CC2_Ready = true;
      } else if ((!rtU_DCPlugIn) && (rtDW->temporalCounter_i1_j >= 5)) {
        rtDW->is_c4_DCHCode = IN_Init;
        rtDW->ELockSt = false;
        rtDW->U1 = 6.0F;
        rtDW->CC2_Ready = false;
      }
      break;

     default:
      /* case IN_S_Open: */
      if (!rtU_DCPlugIn) {
        rtDW->U1 = 6.0F;
        if (rtDW->temporalCounter_i1_j >= 5) {
          rtDW->U1 = 12.0F;
          rtDW->CC2_Ready = false;
          if (rtDW->temporalCounter_i1_j >= 5) {
            rtDW->is_c4_DCHCode = IN_Init;
            rtDW->ELockSt = false;
            rtDW->U1 = 6.0F;
            rtDW->CC2_Ready = false;
          } else {
            guard1 = true;
          }
        } else {
          guard1 = true;
        }
      } else {
        guard1 = true;
      }
      break;
    }

    if (guard1) {
      if (rtDW->Delay3_DSTATE == 1.0) {
        rtDW->is_c4_DCHCode = IN_ELock;
        rtDW->temporalCounter_i1_j = 0U;
        rtDW->ELockSt = true;
      }
    }
  }

  /* End of Chart: '<S8>/Plug Logic' */

  /* DataTypeConversion: '<S3>/Data Type Conversion23' incorporates:
   *  Delay: '<S3>/Delay'
   */
  DataTypeConversion23 = (rtDW->Delay_DSTATE != 0.0F);

  /* Delay: '<S3>/Delay2' */
  rtDW->Delay2 = rtDW->Delay2_DSTATE;

  /* Delay: '<S9>/Delay' */
  rtb_En_CRM = rtDW->Delay_DSTATE_m;

  /* Product: '<S9>/Divide' incorporates:
   *  Constant: '<S2>/IS_PosResSet'
   *  Fcn: '<S9>/Fcn'
   *  Product: '<S9>/Divide1'
   */
  rtb_En_CRM = rtb_En_CRM / (rt_powf_snf(rtb_En_CRM, 2.0F) + 2.22044605e-16F) *
    200000.0F;

  /* Switch: '<S9>/Switch1' incorporates:
   *  Constant: '<S27>/Constant'
   *  Constant: '<S28>/Constant'
   *  Constant: '<S9>/ok'
   *  RelationalOperator: '<S27>/Compare'
   *  RelationalOperator: '<S28>/Compare'
   *  Switch: '<S9>/Switch2'
   */
  if (rtb_En_CRM >= 100000.0F) {
    rtb_Switch1 = 1;
  } else if (!(rtb_En_CRM < 100000.0F)) {
    /* Switch: '<S9>/Switch2' incorporates:
     *  Constant: '<S9>/Warning'
     */
    rtb_Switch1 = 2;
  } else {
    /* Switch: '<S9>/Switch' incorporates:
     *  Constant: '<S9>/Fault'
     *  Switch: '<S9>/Switch2'
     */
    rtb_Switch1 = 3;
  }

  /* End of Switch: '<S9>/Switch1' */

  /* DataTypeConversion: '<S3>/Data Type Conversion28' */
  rtDW->DataTypeConversion28 = (uint8_T)rtb_Switch1;

  /* Delay: '<S2>/Delay' */
  rtb_En_CHM = rtDW->Delay_DSTATE_p;

  /* Logic: '<S6>/Logical Operator2' incorporates:
   *  Delay: '<S2>/Delay1'
   *  Inport: '<Root>/VCUActuator_DIG_IN_DCPRLY_OV'
   */
  rtb_LogicalOperator2 = ((rtb_En_CHM != 0.0F) && (rtDW->Delay1_DSTATE != 0.0F) &&
    (rtU_VCUActuator_DIG_IN_DCPRLY_OV != 0.0F));

  /* Delay: '<S6>/Delay' */
  rtb_En_CRM = rtDW->Delay_DSTATE_n;

  /* DiscreteIntegrator: '<S6>/Discrete-Time Integrator' */
  if (rtDW->DiscreteTimeIntegrator_IC_LOADI != 0) {
    rtDW->DiscreteTimeIntegrator_DSTAT_o5 = rtb_En_CRM;
  }

  if (rtb_LogicalOperator2 || (rtDW->DiscreteTimeIntegrator_PrevRe_h != 0)) {
    rtDW->DiscreteTimeIntegrator_DSTAT_o5 = rtb_En_CRM;
  }

  /* DiscreteIntegrator: '<S6>/Discrete-Time Integrator' */
  rtb_En_CRM = rtDW->DiscreteTimeIntegrator_DSTAT_o5;

  /* DataTypeConversion: '<S3>/Data Type Conversion24' incorporates:
   *  Delay: '<S3>/Delay1'
   */
  rtDW->DataTypeConversion24 = (rtDW->Delay1_DSTATE_c != 0.0F);

  /* Delay: '<S3>/Delay4' */
  Delay4 = rtDW->Delay4_DSTATE;

  /* DataTypeConversion: '<S3>/Data Type Conversion25' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  rtDW->DataTypeConversion25 = (rtU_BMSData->BRM_RCVFlag != 0.0F);

  /* DataTypeConversion: '<S3>/Data Type Conversion29' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  rtDW->DataTypeConversion29 = (rtU_BMSData->BHM_ReceiveFlag != 0.0F);

  /* DataTypeConversion: '<S3>/Data Type Conversion30' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  rtDW->DataTypeConversion30 = (rtU_BMSData->BRO_ReceiveFlag != 0.0F);

  /* DiscreteIntegrator: '<S32>/Discrete-Time Integrator' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  if ((rtU_BMSData->BCS_RCVFlag != 0.0F) ||
      (rtDW->DiscreteTimeIntegrator_PrevR_hq != 0)) {
    rtDW->DiscreteTimeIntegrator_DSTATE = 0.0;
  }

  /* DataTypeConversion: '<S3>/Data Type Conversion43' incorporates:
   *  Constant: '<S44>/Constant'
   *  Constant: '<S45>/Constant'
   *  DiscreteIntegrator: '<S32>/Discrete-Time Integrator'
   *  Logic: '<S32>/Logical Operator'
   *  RelationalOperator: '<S44>/Compare'
   *  RelationalOperator: '<S45>/Compare'
   */
  rtDW->DataTypeConversion43 = ((rtDW->DiscreteTimeIntegrator_DSTATE <= 5.01) &&
    (rtDW->DiscreteTimeIntegrator_DSTATE != 0.0));

  /* DiscreteIntegrator: '<S31>/Discrete-Time Integrator' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  if ((rtU_BMSData->BCP_RCVFlag != 0.0F) ||
      (rtDW->DiscreteTimeIntegrator_PrevRe_e != 0)) {
    rtDW->DiscreteTimeIntegrator_DSTATE_o = 0.0;
  }

  /* DataTypeConversion: '<S3>/Data Type Conversion40' incorporates:
   *  Constant: '<S42>/Constant'
   *  Constant: '<S43>/Constant'
   *  DiscreteIntegrator: '<S31>/Discrete-Time Integrator'
   *  Logic: '<S31>/Logical Operator'
   *  RelationalOperator: '<S42>/Compare'
   *  RelationalOperator: '<S43>/Compare'
   */
  rtDW->DataTypeConversion40 = ((rtDW->DiscreteTimeIntegrator_DSTATE_o <= 5.01) &&
    (rtDW->DiscreteTimeIntegrator_DSTATE_o != 0.0));

  /* DataTypeConversion: '<S3>/Data Type Conversion37' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  rtDW->DataTypeConversion37 = (rtU_BMSData->BEM_ReceiveFlag != 0.0F);

  /* DiscreteIntegrator: '<S30>/Discrete-Time Integrator' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  if ((rtU_BMSData->BCL_ReceiveFlag != 0.0F) ||
      (rtDW->DiscreteTimeIntegrator_PrevR_ev != 0)) {
    rtDW->DiscreteTimeIntegrator_DSTATE_n = 0.0;
  }

  /* DataTypeConversion: '<S3>/Data Type Conversion38' incorporates:
   *  Constant: '<S40>/Constant'
   *  Constant: '<S41>/Constant'
   *  DiscreteIntegrator: '<S30>/Discrete-Time Integrator'
   *  Logic: '<S30>/Logical Operator'
   *  RelationalOperator: '<S40>/Compare'
   *  RelationalOperator: '<S41>/Compare'
   */
  rtDW->DataTypeConversion38 = ((rtDW->DiscreteTimeIntegrator_DSTATE_n <= 1.01) &&
    (rtDW->DiscreteTimeIntegrator_DSTATE_n != 0.0));

  /* DataTypeConversion: '<S3>/Data Type Conversion39' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  DataTypeConversion39 = (rtU_BMSData->BSD_ReceiveFlag != 0.0F);

  /* DataTypeConversion: '<S3>/Data Type Conversion36' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  rtDW->DataTypeConversion36 = (rtU_BMSData->BST_ReceiveFlag != 0.0F);

  /* DataTypeConversion: '<S3>/Data Type Conversion31' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  rtDW->DataTypeConversion31 = (uint8_T)
    rtU_BMSData->Battery_Charge_Ready_BRO_FromRealBMS;

  /* DataTypeConversion: '<S3>/Data Type Conversion35' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  rtDW->DataTypeConversion35 = (rtU_BMSData->Charging_Allow_BSM_FromRealBMS !=
    0.0F);

  /* Logic: '<S37>/Logical Operator' incorporates:
   *  Constant: '<S46>/Constant'
   *  Constant: '<S47>/Constant'
   *  Constant: '<S54>/Constant'
   *  Constant: '<S55>/Constant'
   *  Constant: '<S56>/Constant'
   *  Constant: '<S57>/Constant'
   *  Inport: '<Root>/BattFault'
   *  RelationalOperator: '<S46>/Compare'
   *  RelationalOperator: '<S47>/Compare'
   *  RelationalOperator: '<S54>/Compare'
   *  RelationalOperator: '<S55>/Compare'
   *  RelationalOperator: '<S56>/Compare'
   *  RelationalOperator: '<S57>/Compare'
   */
  rtDW->LogicalOperator = ((rtU_BattFault->Connector_State_BSM_FromRealBMS ==
    1.0F) || (rtU_BattFault->Isolation_BSM_FromRealBMS == 1.0F) ||
    (rtU_BattFault->Cell_Voltage_BSM_FromRealBMS == 1.0F) ||
    (rtU_BattFault->Battery_SOC_BSM_FromRealBMS == 1.0F) ||
    (rtU_BattFault->Charge_Over_Current_BSM_FromRealBMS == 1.0F) ||
    (rtU_BattFault->Over_Temperature_BSM_FromRealBMS == 1.0F));

  /* DataTypeConversion: '<S3>/Data Type Conversion51' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  rtDW->DataTypeConversion51 = (rtU_BMSData->BSM_ReceiveFlag != 0.0F);

  /* Logic: '<S37>/Logical Operator1' incorporates:
   *  Constant: '<S48>/Constant'
   *  Constant: '<S49>/Constant'
   *  Constant: '<S50>/Constant'
   *  Constant: '<S51>/Constant'
   *  Constant: '<S52>/Constant'
   *  Constant: '<S53>/Constant'
   *  Constant: '<S58>/Constant'
   *  Constant: '<S59>/Constant'
   *  Constant: '<S60>/Constant'
   *  Constant: '<S61>/Constant'
   *  Inport: '<Root>/BMSFault'
   *  RelationalOperator: '<S48>/Compare'
   *  RelationalOperator: '<S49>/Compare'
   *  RelationalOperator: '<S50>/Compare'
   *  RelationalOperator: '<S51>/Compare'
   *  RelationalOperator: '<S52>/Compare'
   *  RelationalOperator: '<S53>/Compare'
   *  RelationalOperator: '<S58>/Compare'
   *  RelationalOperator: '<S59>/Compare'
   *  RelationalOperator: '<S60>/Compare'
   *  RelationalOperator: '<S61>/Compare'
   */
  rtDW->LogicalOperator1 =
    ((rtU_BMSFault->BST_EngChg_Flt_Point2VltMonitor_BST_FromRealBMS == 1.0F) ||
     (rtU_BMSFault->BST_EngChg_Flt_HVContactorFlt_BST_FromRealBMS == 1.0F) ||
     (rtU_BMSFault->End_Charging_VoltageAbnormal_BST_FromRealBMS == 1.0F) ||
     (rtU_BMSFault->End_Charging_OtherFault_BST_FromRealBMS == 1.0F) ||
     (rtU_BMSFault->End_Charging_Isolation_BST_FromRealBMS == 1.0F) ||
     (rtU_BMSFault->End_Charging_ConnectorOverTemp_BST_FromRealBMS == 1.0F) ||
     (rtU_BMSFault->End_Charging_BMUOverTemp_BST_FromRealBMS == 1.0F) ||
     (rtU_BMSFault->End_Charging_ConnectorFault_BST_FromRealBMS == 1.0F) ||
     (rtU_BMSFault->End_Charging_OverTempFault_BST_FromRealBMS == 1.0F) ||
     (rtU_BMSFault->End_Charging_OverCurrent_BST_FromRealBMS == 1.0F));

  /* Logic: '<S33>/DisConnect' incorporates:
   *  Inport: '<Root>/DCPlugIn'
   */
  LogicalOperator_k_tmp = !rtU_DCPlugIn;

  /* Logic: '<S33>/Logical Operator' incorporates:
   *  Logic: '<S33>/DisConnect'
   */
  rtDW->LogicalOperator_k = LogicalOperator_k_tmp;

  /* Chart: '<S3>/Charge Logic Control' incorporates:
   *  DiscreteIntegrator: '<S6>/Discrete-Time Integrator'
   *  Inport: '<Root>/DCPlugIn'
   *  Inport: '<Root>/OFC_Key'
   */
  if (rtDW->temporalCounter_i1 < MAX_uint32_T) {
    rtDW->temporalCounter_i1++;
  }

  if (rtDW->is_active_c5_DCHCode == 0U) {
    rtDW->is_active_c5_DCHCode = 1U;
    rtDW->is_c5_DCHCode = IN_SystemOFF;
    enter_atomic_SystemOFF(rtDW);
  } else if (rtDW->is_c5_DCHCode == 1) {
    rtDW->SysChargSt = 99U;
    rtDW->CRO_SPN2830 = 0U;
    rtDW->CRM_SPN2560 = 0U;
    if (rtU_OFC_Key == 1) {
      rtDW->is_c5_DCHCode = IN_SystemON;
      rtDW->is_SystemON = IN_Sleep;
      enter_atomic_Sleep(rtDW);
    }

    /* case IN_SystemON: */
  } else if (rtU_OFC_Key == 0) {
    switch (rtDW->is_SystemON) {
     case IN_ActiveDischarge:
      if (rtDW->is_ActiveDischarge == 1) {
        rtDW->is_ActiveDischarge = IN_NO_ACTIVE_CHILD;
      } else {
        rtDW->is_ActiveDischarge = IN_NO_ACTIVE_CHILD;
      }

      rtDW->is_SystemON = IN_NO_ACTIVE_CHILD;
      break;

     case IN_MainBlock:
      exit_internal_MainBlock(rtDW);
      rtDW->is_SystemON = IN_NO_ACTIVE_CHILD;
      break;

     case IN_PowerDown:
      exit_internal_PowerDown(rtDW);
      rtDW->is_SystemON = IN_NO_ACTIVE_CHILD;
      break;

     default:
      rtDW->is_SystemON = IN_NO_ACTIVE_CHILD;
      break;
    }

    rtDW->is_c5_DCHCode = IN_SystemOFF;
    enter_atomic_SystemOFF(rtDW);
  } else {
    guard1 = false;
    switch (rtDW->is_SystemON) {
     case IN_ActiveDischarge:
      rtDW->SysChargSt = 8U;
      if (rtDW->IS_ActDisChrgFinish && rtDW->IS_ActDisChrgEn) {
        rtDW->IS_ActDisChrgEn = false;
        if (rtDW->is_ActiveDischarge == 1) {
          rtDW->CtimeEn0 = false;
          rtDW->is_ActiveDischarge = IN_NO_ACTIVE_CHILD;
        } else {
          rtDW->is_ActiveDischarge = IN_NO_ACTIVE_CHILD;
        }

        rtDW->is_SystemON = IN_MainBlock;
        rtDW->is_MainBlock = IN_InsulationMonitoring;
        rtDW->SysChargSt = 3U;
        rtDW->IS_MonFinish = false;
        rtDW->is_InsulationMonitoring = IN_Chart;
        rtDW->temporalCounter_i1 = 0U;
      } else if (rtDW->PD_ActDisChrgFinish && rtDW->PD_ActDisChrgEn) {
        if (rtDW->is_ActiveDischarge == 1) {
          rtDW->is_ActiveDischarge = IN_NO_ACTIVE_CHILD;
        } else {
          rtDW->is_ActiveDischarge = IN_NO_ACTIVE_CHILD;
        }

        rtDW->is_SystemON = IN_PowerDown;
        rtDW->SysChargSt = 7U;
        enter_internal_PowerDown(rtDW);
      } else {
        switch (rtDW->is_ActiveDischarge) {
         case IN_Active:
          if (rtDW->DiscreteTimeIntegrator_DSTATE_l > 5.0F) {
            rtDW->ProcessMod = 10U;
            guard1 = true;
          } else if (rtDW->DiscreteTimeIntegrator1_DSTATE > 10.0F) {
            rtDW->BSDTimeout = true;
            rtDW->ProcessMod = 10U;
            guard1 = true;
          } else if ((rtDW->DiscreteTimeIntegrator_DSTAT_o5 < 60.0F) &&
                     rtDW->IS_ActDisChrgEn) {
            rtDW->CtimeEn0 = false;
            rtDW->is_ActiveDischarge = IN_IS_End;
            rtDW->ActDisChrgSw = false;
            rtDW->IS_ActDisChrgFinish = true;
          } else if ((rtDW->DiscreteTimeIntegrator_DSTAT_o5 < 60.0F) &&
                     rtDW->PD_ActDisChrgEn) {
            rtDW->CtimeEn0 = false;
            rtDW->is_ActiveDischarge = IN_PD_End;
            rtDW->ActDisChrgSw = false;
            rtDW->PD_ActDisChrgFinish = true;
          }
          break;

         case IN_Default_l5v:
          if (rtDW->PD_ActDisChrgEn) {
            rtDW->is_ActiveDischarge = IN_PD;
            rtDW->PD_ActDisChrgFinish = false;
          } else if (rtDW->IS_ActDisChrgEn) {
            rtDW->is_ActiveDischarge = IN_IS;
            rtDW->IS_ActDisChrgFinish = false;
          }
          break;

         case IN_IS:
          rtDW->is_ActiveDischarge = IN_Active;
          rtDW->ActDisChrgSw = true;
          rtDW->CtimeEn0 = true;
          break;

         case IN_IS_End:
          break;

         case IN_PD:
          rtDW->is_ActiveDischarge = IN_Active;
          rtDW->ActDisChrgSw = true;
          rtDW->CtimeEn0 = true;
          break;

         default:
          /* case IN_PD_End: */
          break;
        }
      }
      break;

     case IN_MainBlock:
      MainBlock(&rtb_En_CRM, &DiscreteTimeIntegrator, &DiscreteTimeIntegrator1,
                &DiscreteTimeIntegrator2, &rtDW->BusConversion_InsertedFor_Charg,
                &DataTypeConversion23, rtDW);
      break;

     case IN_PowerDown:
      PowerDown(&DiscreteTimeIntegrator, &DiscreteTimeIntegrator1,
                &DataTypeConversion39, &DiscreteTimeIntegrator2,
                &rtDW->BusConversion_InsertedFor_Charg, &Delay4, rtU_DCPlugIn,
                rtDW);
      break;

     default:
      /* case IN_Sleep: */
      rtDW->SysChargSt = 0U;
      rtDW->CRO_SPN2830 = 0U;
      rtDW->CRM_SPN2560 = 0U;
      if (rtU_DCPlugIn) {
        rtDW->is_SystemON = IN_MainBlock;
        rtDW->is_MainBlock = IN_PhyConnConfirm;
        rtDW->SysChargSt = 1U;
        rtDW->PhyConnFinish = false;
        rtDW->is_PhyConnConfirm = IN_Default_l;
      }
      break;
    }

    if (guard1) {
      rtDW->is_ActiveDischarge = IN_NO_ACTIVE_CHILD;
      rtDW->En_CEM = true;
      rtDW->is_SystemON = IN_PowerDown;
      rtDW->SysChargSt = 7U;
      enter_internal_PowerDown(rtDW);
    }
  }

  /* End of Chart: '<S3>/Charge Logic Control' */

  /* RelationalOperator: '<S13>/Compare' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion20'
   */
  rtb_Compare = (uint8_T)!rtDW->ChargeEnable;

  /* Abs: '<S5>/Abs' incorporates:
   *  Inport: '<Root>/BMSData'
   *  Switch: '<S5>/Switch1'
   */
  DiscreteTimeIntegrator = (real32_T)fabs
    (rtU_BMSData->Current_Demand_BCL_FromRealBMS);

  /* MinMax: '<S5>/MinMax' */
  if (!(DiscreteTimeIntegrator < 100.0F)) {
    DiscreteTimeIntegrator = 100.0F;
  }

  /* End of MinMax: '<S5>/MinMax' */

  /* Sum: '<S11>/Add1' incorporates:
   *  Abs: '<S11>/Abs2'
   */
  DiscreteTimeIntegrator1 = DiscreteTimeIntegrator - rtb_Abs2;

  /* MinMax: '<S5>/MinMax2' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  if ((800.0F < rtU_BMSData->Voltage_Demand_BCL_FromRealBMS) || rtIsNaNF
      (rtU_BMSData->Voltage_Demand_BCL_FromRealBMS)) {
    DiscreteTimeIntegrator2 = 800.0F;
  } else {
    DiscreteTimeIntegrator2 = rtU_BMSData->Voltage_Demand_BCL_FromRealBMS;
  }

  /* End of MinMax: '<S5>/MinMax2' */

  /* Sum: '<S11>/Add2' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  Delay4 = DiscreteTimeIntegrator2 - rtU_BMSData->LinkVolt;

  /* Logic: '<S5>/Logical Operator' incorporates:
   *  Abs: '<S5>/Abs3'
   *  Constant: '<S14>/Constant'
   *  Constant: '<S2>/OFC_MinChrgCur'
   *  Constant: '<S2>/OFC_MinChrgVolt'
   *  Inport: '<Root>/BMSData'
   *  Logic: '<S10>/Logical Operator1'
   *  RelationalOperator: '<S14>/Compare'
   *  RelationalOperator: '<S5>/Relational Operator'
   *  RelationalOperator: '<S5>/Relational Operator1'
   */
  DataTypeConversion39 = (((rtb_Compare != 0) && (rtb_Abs2 < 0.1F)) || (rtb_Abs2
    < 400.0F) || (rtU_BMSData->LinkVolt < 0.0F));

  /* DiscreteIntegrator: '<S5>/Discrete-Time Integrator' */
  if (DataTypeConversion39 || (rtDW->DiscreteTimeIntegrator_PrevRe_c != 0)) {
    rtDW->DiscreteTimeIntegrator_DSTATE_p = 0.0F;
  }

  /* Sum: '<S11>/Add3' incorporates:
   *  DiscreteIntegrator: '<S5>/Discrete-Time Integrator'
   */
  DiscreteTimeIntegrator = rtDW->DiscreteTimeIntegrator_DSTATE_p -
    DiscreteTimeIntegrator;

  /* Relay: '<S11>/Relay1' */
  rtDW->Relay1_Mode = ((DiscreteTimeIntegrator1 >= 1.0F) ||
                       ((!(DiscreteTimeIntegrator1 <= -1.0F)) &&
                        rtDW->Relay1_Mode));

  /* Relay: '<S11>/Relay' */
  rtDW->Relay_Mode = ((Delay4 >= 2.0F) || ((!(Delay4 <= -2.0F)) &&
    rtDW->Relay_Mode));

  /* Relay: '<S11>/Relay2' */
  rtDW->Relay2_Mode = ((DiscreteTimeIntegrator >= 3.0F) ||
                       ((!(DiscreteTimeIntegrator <= 0.0F)) && rtDW->Relay2_Mode));

  /* Gain: '<S5>/Gain' incorporates:
   *  DiscreteIntegrator: '<S5>/Discrete-Time Integrator'
   */
  DiscreteTimeIntegrator = -rtDW->DiscreteTimeIntegrator_DSTATE_p;

  /* Switch: '<S5>/Switch' incorporates:
   *  Constant: '<S10>/ChrgStop'
   *  Constant: '<S12>/Constant'
   *  DataTypeConversion: '<S3>/Data Type Conversion20'
   *  Logic: '<S10>/Logical Operator'
   *  RelationalOperator: '<S12>/Compare'
   *  Switch: '<S10>/Switch'
   */
  if (rtDW->ChargeEnable) {
    /* Relay: '<S11>/Relay2' */
    if (rtDW->Relay2_Mode) {
      DiscreteTimeIntegrator2 = 1.0F;
    } else {
      DiscreteTimeIntegrator2 = -1.0F;
    }

    /* Switch: '<S11>/Switch4' incorporates:
     *  Constant: '<S11>/Constant1'
     *  Constant: '<S18>/Constant'
     *  Constant: '<S19>/Constant'
     *  Logic: '<S11>/Logical Operator4'
     *  Logic: '<S11>/Logical Operator5'
     *  RelationalOperator: '<S18>/Compare'
     *  RelationalOperator: '<S19>/Compare'
     */
    if ((!(DiscreteTimeIntegrator1 > 0.0F)) || (!(DiscreteTimeIntegrator2 > 0.0F)))
    {
      /* Switch: '<S11>/Switch2' incorporates:
       *  Constant: '<S11>/Constant1'
       */
      if (rtDW->ChargeEnable) {
        /* MultiPortSwitch: '<S11>/Multiport Switch' incorporates:
         *  Constant: '<S11>/Constant1'
         *  Inport: '<Root>/BMSData'
         */
        switch ((int32_T)rtU_BMSData->Charge_mode_BCL_FromRealBMS) {
         case 0:
          rtb_Abs2 = 0.0F;
          break;

         case 1:
          /* Relay: '<S11>/Relay' */
          if (rtDW->Relay_Mode) {
            rtb_Switch1 = 1;
          } else {
            rtb_Switch1 = -1;
          }

          /* Relay: '<S11>/Relay1' */
          if (rtDW->Relay1_Mode) {
            DiscreteTimeIntegrator2 = 1.0F;
          } else {
            DiscreteTimeIntegrator2 = -1.0F;
          }

          /* Switch: '<S11>/Switch3' incorporates:
           *  Constant: '<S16>/Constant'
           *  Constant: '<S20>/Constant'
           *  DataTypeConversion: '<S33>/Data Type Conversion2'
           *  Logic: '<S11>/Logical Operator2'
           *  Logic: '<S11>/Logical Operator3'
           *  Lookup_n-D: '<S11>/BasedOnCurrentPermittion'
           *  Lookup_n-D: '<S11>/BasedOnVoltagePermittion'
           *  RelationalOperator: '<S16>/Compare'
           *  RelationalOperator: '<S20>/Compare'
           *  Sum: '<S11>/Add1'
           */
          if ((rtb_Switch1 <= 0) || (!(DiscreteTimeIntegrator2 < 0.0F))) {
            rtb_Abs2 = look1_iflf_binlx(Delay4, rtConstP.pooled14,
              rtConstP.BasedOnVoltagePermittion_tableD, 2U);
          } else {
            rtb_Abs2 = look1_iflf_binlx(DiscreteTimeIntegrator1,
              rtConstP.pooled14, rtConstP.BasedOnCurrentPermittion_tableD, 2U);
          }

          /* End of Switch: '<S11>/Switch3' */
          break;

         default:
          /* Relay: '<S11>/Relay' */
          if (rtDW->Relay_Mode) {
            rtb_Switch1 = 1;
          } else {
            rtb_Switch1 = -1;
          }

          /* Relay: '<S11>/Relay1' */
          if (rtDW->Relay1_Mode) {
            DiscreteTimeIntegrator2 = 1.0F;
          } else {
            DiscreteTimeIntegrator2 = -1.0F;
          }

          /* Switch: '<S11>/Switch1' incorporates:
           *  Constant: '<S15>/Constant'
           *  Constant: '<S17>/Constant'
           *  DataTypeConversion: '<S33>/Data Type Conversion2'
           *  Logic: '<S11>/Logical Operator1'
           *  Lookup_n-D: '<S11>/BasedOnCurrentPermittion'
           *  Lookup_n-D: '<S11>/BasedOnVoltagePermittion'
           *  RelationalOperator: '<S15>/Compare'
           *  RelationalOperator: '<S17>/Compare'
           *  Sum: '<S11>/Add1'
           */
          if ((rtb_Switch1 < 0) && (DiscreteTimeIntegrator2 > 0.0F)) {
            rtb_Abs2 = look1_iflf_binlx(Delay4, rtConstP.pooled14,
              rtConstP.BasedOnVoltagePermittion_tableD, 2U);
          } else {
            rtb_Abs2 = look1_iflf_binlx(DiscreteTimeIntegrator1,
              rtConstP.pooled14, rtConstP.BasedOnCurrentPermittion_tableD, 2U);
          }

          /* End of Switch: '<S11>/Switch1' */
          break;
        }

        /* End of MultiPortSwitch: '<S11>/Multiport Switch' */
      } else {
        rtb_Abs2 = 0.0F;
      }

      /* End of Switch: '<S11>/Switch2' */
    } else {
      rtb_Abs2 = 0.0F;
    }

    /* End of Switch: '<S11>/Switch4' */
  } else if ((rtb_Compare != 0) && (rtb_Abs2 > 2.22044605E-16F)) {
    /* Switch: '<S10>/Switch' incorporates:
     *  Constant: '<S10>/CurDnRate'
     */
    rtb_Abs2 = -100.0F;
  } else {
    rtb_Abs2 = 0.0F;
  }

  /* End of Switch: '<S5>/Switch' */

  /* MinMax: '<S2>/MinMax' incorporates:
   *  Inport: '<Root>/BMSData'
   */
  if ((800.0F < rtU_BMSData->BHM_MaxAllow_Chg_TotVol_BHM_FromRealBMS) ||
      rtIsNaNF(rtU_BMSData->BHM_MaxAllow_Chg_TotVol_BHM_FromRealBMS)) {
    DiscreteTimeIntegrator2 = 800.0F;
  } else {
    DiscreteTimeIntegrator2 =
      rtU_BMSData->BHM_MaxAllow_Chg_TotVol_BHM_FromRealBMS;
  }

  /* End of MinMax: '<S2>/MinMax' */

  /* RelationalOperator: '<S23>/Compare' incorporates:
   *  Constant: '<S23>/Constant'
   *  DiscreteIntegrator: '<S6>/Discrete-Time Integrator'
   *  Sum: '<S6>/Add1'
   */
  rtb_Compare = (uint8_T)(DiscreteTimeIntegrator2 -
    rtDW->DiscreteTimeIntegrator_DSTAT_o5 <= 0.0F);

  /* Switch: '<S6>/Switch2' incorporates:
   *  Constant: '<S6>/InitVolt[V]'
   *  Delay: '<S2>/Delay1'
   *  Switch: '<S6>/Switch1'
   */
  if (rtDW->Delay1_DSTATE != 0.0F) {
    /* Switch: '<S6>/Switch7' incorporates:
     *  Constant: '<S6>/InitVolt[V]'
     *  Inport: '<Root>/BMSData'
     *  Inport: '<Root>/VCUActuator_DIG_IN_DCPRLY_OV'
     *  Switch: '<S6>/Switch1'
     */
    if (rtU_VCUActuator_DIG_IN_DCPRLY_OV != 0.0F) {
      DiscreteTimeIntegrator1 = rtU_BMSData->LinkVolt;
    } else if (rtb_En_CHM != 0.0F) {
      /* Switch: '<S6>/Switch1' incorporates:
       *  DiscreteIntegrator: '<S6>/Discrete-Time Integrator'
       */
      DiscreteTimeIntegrator1 = rtDW->DiscreteTimeIntegrator_DSTAT_o5;
    } else {
      DiscreteTimeIntegrator1 = 0.0F;
    }

    /* End of Switch: '<S6>/Switch7' */
  } else if (rtb_En_CHM != 0.0F) {
    /* Switch: '<S6>/Switch1' incorporates:
     *  DiscreteIntegrator: '<S6>/Discrete-Time Integrator'
     */
    DiscreteTimeIntegrator1 = rtDW->DiscreteTimeIntegrator_DSTAT_o5;
  } else {
    DiscreteTimeIntegrator1 = 0.0F;
  }

  /* End of Switch: '<S6>/Switch2' */

  /* Update for Delay: '<S3>/Delay1' incorporates:
   *  Abs: '<S24>/Abs'
   *  Constant: '<S25>/Constant'
   *  Constant: '<S2>/OFC_MaxChrgVolt'
   *  Constant: '<S2>/OFC_MinChrgVolt'
   *  Fcn: '<S24>/Fcn'
   *  Inport: '<Root>/BMSData'
   *  Logic: '<S24>/Logical Operator1'
   *  Product: '<S24>/Divide1'
   *  RelationalOperator: '<S24>/Relational Operator2'
   *  RelationalOperator: '<S24>/Relational Operator3'
   *  RelationalOperator: '<S25>/Compare'
   *  Sum: '<S24>/Add3'
   *  Switch: '<S22>/Switch1'
   */
  rtDW->Delay1_DSTATE_c = (real32_T)((800.0F < DiscreteTimeIntegrator1) || (0.0F
    > DiscreteTimeIntegrator1) || (rtU_BMSData->BCP_Bat_CurVolt / (rt_powf_snf
    (rtU_BMSData->BCP_Bat_CurVolt, 2.0F) + 2.22044605e-16F) * (real32_T)fabs
    (DiscreteTimeIntegrator1 - rtU_BMSData->BCP_Bat_CurVolt) > 0.05F));

  /* DiscreteIntegrator: '<S7>/Discrete-Time Integrator' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion20'
   */
  if (rtDW->ChargeEnable && (rtDW->DiscreteTimeIntegrator_PrevR_c3 <= 0)) {
    rtDW->DiscreteTimeIntegrator_DSTATE_i = 0.0F;
  }

  /* Switch: '<S7>/Switch' incorporates:
   *  Abs: '<S7>/Abs'
   *  Constant: '<S7>/Constant'
   *  DataTypeConversion: '<S3>/Data Type Conversion20'
   *  DiscreteIntegrator: '<S5>/Discrete-Time Integrator'
   *  DiscreteIntegrator: '<S6>/Discrete-Time Integrator'
   *  Gain: '<S5>/Gain'
   *  Product: '<S7>/Divide'
   */
  if (rtDW->ChargeEnable) {
    rtb_En_CHM = rtDW->DiscreteTimeIntegrator_DSTAT_o5 * (real32_T)fabs
      (-rtDW->DiscreteTimeIntegrator_DSTATE_p);
  } else {
    rtb_En_CHM = 0.0F;
  }

  /* End of Switch: '<S7>/Switch' */

  /* Outport: '<Root>/CSD' incorporates:
   *  DiscreteIntegrator: '<S7>/Discrete-Time Integrator'
   *  Gain: '<S7>/wh->Kwh'
   */
  rtY_CSD_e->ChrgEnergy = 0.001F * rtDW->DiscreteTimeIntegrator_DSTATE_i;

  /* DiscreteIntegrator: '<S35>/Discrete-Time Integrator' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion20'
   */
  if (rtDW->ChargeEnable && (rtDW->DiscreteTimeIntegrator_PrevR_cy <= 0)) {
    rtDW->DiscreteTimeIntegrator_DSTATE_c = 0.0F;
  }

  /* Outport: '<Root>/ChargingTime' incorporates:
   *  DiscreteIntegrator: '<S35>/Discrete-Time Integrator'
   */
  *rtY_ChargingTime = rtDW->DiscreteTimeIntegrator_DSTATE_c;

  /* Outport: '<Root>/CCS' incorporates:
   *  BusCreator: '<S64>/Bus Creator'
   *  DiscreteIntegrator: '<S35>/Discrete-Time Integrator'
   */
  rtY_CCS_k->ChargingTime = rtDW->DiscreteTimeIntegrator_DSTATE_c;

  /* Outport: '<Root>/CSD' incorporates:
   *  BusCreator: '<S70>/Bus Creator'
   *  DiscreteIntegrator: '<S35>/Discrete-Time Integrator'
   */
  rtY_CSD_e->ChargingTime = rtDW->DiscreteTimeIntegrator_DSTATE_c;

  /* Update for UnitDelay: '<S39>/Unit Delay' incorporates:
   *  Logic: '<S39>/Logical Operator'
   */
  rtDW->UnitDelay_DSTATE = !rtDW->CtimeEn0;

  /* Update for DiscreteIntegrator: '<S39>/Discrete-Time Integrator' incorporates:
   *  Constant: '<S39>/Constant'
   */
  rtDW->DiscreteTimeIntegrator_DSTATE_l++;
  rtDW->DiscreteTimeIntegrator_PrevRese = (int8_T)rtb_UnitDelay;

  /* Update for UnitDelay: '<S39>/Unit Delay1' incorporates:
   *  Logic: '<S39>/Logical Operator1'
   */
  rtDW->UnitDelay1_DSTATE = !rtDW->CtimeEn1;

  /* Update for DiscreteIntegrator: '<S39>/Discrete-Time Integrator1' incorporates:
   *  Constant: '<S39>/Constant'
   */
  rtDW->DiscreteTimeIntegrator1_DSTATE++;
  rtDW->DiscreteTimeIntegrator1_PrevRes = (int8_T)rtb_UnitDelay1;

  /* Update for UnitDelay: '<S39>/Unit Delay2' incorporates:
   *  Logic: '<S39>/Logical Operator2'
   */
  rtDW->UnitDelay2_DSTATE = !rtDW->CtimeEn2;

  /* Update for DiscreteIntegrator: '<S39>/Discrete-Time Integrator2' incorporates:
   *  Constant: '<S39>/Constant'
   */
  rtDW->DiscreteTimeIntegrator2_DSTATE++;
  rtDW->DiscreteTimeIntegrator2_PrevRes = (int8_T)rtb_UnitDelay2;

  /* Update for Delay: '<S8>/Delay3' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion9'
   */
  rtDW->Delay3_DSTATE = rtDW->ELockCtrl;

  /* Update for Delay: '<S3>/Delay' incorporates:
   *  Switch: '<S21>/Switch1'
   */
  rtDW->Delay_DSTATE = rtb_Compare;

  /* Update for Delay: '<S3>/Delay2' */
  rtDW->Delay2_DSTATE = DiscreteTimeIntegrator1;

  /* Update for Delay: '<S9>/Delay' */
  rtDW->Delay_DSTATE_m = DiscreteTimeIntegrator1;

  /* Update for Delay: '<S2>/Delay' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion7'
   */
  rtDW->Delay_DSTATE_p = rtDW->K1K2RlyCtrl;

  /* Update for Delay: '<S2>/Delay1' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion6'
   */
  rtDW->Delay1_DSTATE = rtDW->PhyConnFinish;

  /* Update for Delay: '<S6>/Delay' */
  rtDW->Delay_DSTATE_n = DiscreteTimeIntegrator1;

  /* Update for DiscreteIntegrator: '<S6>/Discrete-Time Integrator' */
  rtDW->DiscreteTimeIntegrator_IC_LOADI = 0U;

  /* Switch: '<S6>/Switch3' incorporates:
   *  Constant: '<S6>/VoltDnRate'
   *  DataTypeConversion: '<S3>/Data Type Conversion12'
   *  DataTypeConversion: '<S3>/Data Type Conversion14'
   *  DataTypeConversion: '<S3>/Data Type Conversion42'
   *  Switch: '<S6>/Switch5'
   *  Switch: '<S6>/Switch6'
   */
  if (rtDW->IS_VoltEn) {
    /* Switch: '<S6>/Switch4' incorporates:
     *  Constant: '<S6>/IS_VoltUpRate'
     *  Constant: '<S6>/Stop'
     */
    if (rtb_Compare != 0) {
      rtb_Switch1 = 0;
    } else {
      rtb_Switch1 = 500;
    }

    /* End of Switch: '<S6>/Switch4' */
  } else if (rtDW->PreChrgEn) {
    /* Switch: '<S6>/Switch6' incorporates:
     *  Constant: '<S6>/PreChrg_VoltUpRate'
     */
    rtb_Switch1 = 500;
  } else if (rtDW->ActDisChrgSw) {
    /* Switch: '<S6>/Switch5' incorporates:
     *  Constant: '<S6>/ActDisChrgRate'
     *  Switch: '<S6>/Switch6'
     */
    rtb_Switch1 = -1000;
  } else {
    rtb_Switch1 = -20;
  }

  /* End of Switch: '<S6>/Switch3' */

  /* Update for DiscreteIntegrator: '<S6>/Discrete-Time Integrator' */
  rtDW->DiscreteTimeIntegrator_DSTAT_o5 += (real32_T)rtb_Switch1;
  rtDW->DiscreteTimeIntegrator_PrevRe_h = (int8_T)rtb_LogicalOperator2;

  /* Update for Delay: '<S3>/Delay4' incorporates:
   *  DiscreteIntegrator: '<S5>/Discrete-Time Integrator'
   *  Gain: '<S5>/Gain'
   */
  rtDW->Delay4_DSTATE = -rtDW->DiscreteTimeIntegrator_DSTATE_p;

  /* Update for DiscreteIntegrator: '<S32>/Discrete-Time Integrator' incorporates:
   *  Constant: '<S32>/Constant'
   *  Inport: '<Root>/BMSData'
   */
  rtDW->DiscreteTimeIntegrator_DSTATE++;
  if (rtU_BMSData->BCS_RCVFlag > 0.0F) {
    rtDW->DiscreteTimeIntegrator_PrevR_hq = 1;
  } else if (rtU_BMSData->BCS_RCVFlag < 0.0F) {
    rtDW->DiscreteTimeIntegrator_PrevR_hq = -1;
  } else if (rtU_BMSData->BCS_RCVFlag == 0.0F) {
    rtDW->DiscreteTimeIntegrator_PrevR_hq = 0;
  } else {
    rtDW->DiscreteTimeIntegrator_PrevR_hq = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S32>/Discrete-Time Integrator' */

  /* Update for DiscreteIntegrator: '<S31>/Discrete-Time Integrator' incorporates:
   *  Constant: '<S31>/Constant'
   *  Inport: '<Root>/BMSData'
   */
  rtDW->DiscreteTimeIntegrator_DSTATE_o++;
  if (rtU_BMSData->BCP_RCVFlag > 0.0F) {
    rtDW->DiscreteTimeIntegrator_PrevRe_e = 1;
  } else if (rtU_BMSData->BCP_RCVFlag < 0.0F) {
    rtDW->DiscreteTimeIntegrator_PrevRe_e = -1;
  } else if (rtU_BMSData->BCP_RCVFlag == 0.0F) {
    rtDW->DiscreteTimeIntegrator_PrevRe_e = 0;
  } else {
    rtDW->DiscreteTimeIntegrator_PrevRe_e = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S31>/Discrete-Time Integrator' */

  /* Update for DiscreteIntegrator: '<S30>/Discrete-Time Integrator' incorporates:
   *  Constant: '<S30>/Constant'
   *  Inport: '<Root>/BMSData'
   */
  rtDW->DiscreteTimeIntegrator_DSTATE_n++;
  if (rtU_BMSData->BCL_ReceiveFlag > 0.0F) {
    rtDW->DiscreteTimeIntegrator_PrevR_ev = 1;
  } else if (rtU_BMSData->BCL_ReceiveFlag < 0.0F) {
    rtDW->DiscreteTimeIntegrator_PrevR_ev = -1;
  } else if (rtU_BMSData->BCL_ReceiveFlag == 0.0F) {
    rtDW->DiscreteTimeIntegrator_PrevR_ev = 0;
  } else {
    rtDW->DiscreteTimeIntegrator_PrevR_ev = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S30>/Discrete-Time Integrator' */

  /* Update for DiscreteIntegrator: '<S5>/Discrete-Time Integrator' */
  rtDW->DiscreteTimeIntegrator_DSTATE_p += rtb_Abs2;
  rtDW->DiscreteTimeIntegrator_PrevRe_c = (int8_T)DataTypeConversion39;

  /* Update for DiscreteIntegrator: '<S7>/Discrete-Time Integrator' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion20'
   */
  rtDW->DiscreteTimeIntegrator_DSTATE_i += rtb_En_CHM;
  if (rtDW->ChargeEnable) {
    rtDW->DiscreteTimeIntegrator_PrevR_c3 = 1;
  } else if (!rtDW->ChargeEnable) {
    rtDW->DiscreteTimeIntegrator_PrevR_c3 = 0;
  } else {
    rtDW->DiscreteTimeIntegrator_PrevR_c3 = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S7>/Discrete-Time Integrator' */

  /* Switch: '<S35>/Switch' incorporates:
   *  Constant: '<S35>/Constant'
   *  Constant: '<S35>/min//s'
   *  DataTypeConversion: '<S3>/Data Type Conversion20'
   */
  if (rtDW->ChargeEnable) {
    DiscreteTimeIntegrator2 = 0.0166666675F;
  } else {
    DiscreteTimeIntegrator2 = 0.0F;
  }

  /* End of Switch: '<S35>/Switch' */

  /* Update for DiscreteIntegrator: '<S35>/Discrete-Time Integrator' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion20'
   */
  rtDW->DiscreteTimeIntegrator_DSTATE_c += DiscreteTimeIntegrator2;
  if (rtDW->ChargeEnable) {
    rtDW->DiscreteTimeIntegrator_PrevR_cy = 1;
  } else if (!rtDW->ChargeEnable) {
    rtDW->DiscreteTimeIntegrator_PrevR_cy = 0;
  } else {
    rtDW->DiscreteTimeIntegrator_PrevR_cy = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S35>/Discrete-Time Integrator' */

  /* Outport: '<Root>/ChrgCur' incorporates:
   *  Gain: '<S5>/Gain'
   */
  *rtY_ChrgCur = DiscreteTimeIntegrator;

  /* Outport: '<Root>/ChrgVolt' incorporates:
   *  Switch: '<S2>/Switch1'
   */
  *rtY_ChrgVolt = rtb_En_CRM;

  /* End of Outputs for SubSystem: '<Root>/SoftOFC' */

  /* Outport: '<Root>/OFC_MinChrgVolt' incorporates:
   *  Constant: '<S2>/OFC_MinChrgVolt'
   */
  *rtY_OFC_MinChrgVolt = 0.0F;

  /* Outport: '<Root>/OFC_MinChrgCur' incorporates:
   *  Constant: '<S2>/OFC_MinChrgCur'
   */
  *rtY_OFC_MinChrgCur = -400.0F;

  /* Outport: '<Root>/OFC_MaxChrgCur' incorporates:
   *  Constant: '<S2>/OFC_MaxChrgCur'
   */
  *rtY_OFC_MaxChrgCur = 100.0F;

  /* Outport: '<Root>/OFC_MaxChrgVolt' incorporates:
   *  Constant: '<S2>/OFC_MaxChrgVolt'
   */
  *rtY_OFC_MaxChrgVolt = 800.0F;

  /* Outport: '<Root>/Volt_K1K2_ChrgSide' */
  *rtY_Volt_K1K2_ChrgSide = rtb_En_CRM;

  /* Outport: '<Root>/Volt_K1K2_VehSide' */
  *rtY_Volt_K1K2_VehSide = DiscreteTimeIntegrator1;

  /* Outport: '<Root>/CC2_Ready' */
  *rtY_CC2_Ready = rtDW->CC2_Ready;

  /* Outputs for Atomic SubSystem: '<Root>/SoftOFC' */
  /* Outport: '<Root>/K1K2RlyCtrl' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion7'
   */
  *rtY_K1K2RlyCtrl = rtDW->K1K2RlyCtrl;

  /* Outport: '<Root>/K3K4RlyCtrl' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion8'
   */
  *rtY_K3K4RlyCtrl = rtDW->K3K4RlyCtrl;

  /* Outport: '<Root>/SysChargeSt' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion41'
   */
  *rtY_SysChargeSt = rtDW->SysChargSt;

  /* Outport: '<Root>/ProcessMod' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion71'
   */
  *rtY_ProcessMod = rtDW->ProcessMod;

  /* Outport: '<Root>/ChargeEnble' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion20'
   */
  *rtY_ChargeEnble = rtDW->ChargeEnable;

  /* Outport: '<Root>/CHM' incorporates:
   *  Constant: '<S66>/ComuProtocalVertion2015'
   *  DataTypeConversion: '<S3>/Data Type Conversion10'
   *  Switch: '<S66>/Switch1'
   */
  rtY_CHM_e->CommProtocolVersion = 257.0F;
  rtY_CHM_e->En_CHM = rtDW->En_CHM;

  /* Outport: '<Root>/CRM' incorporates:
   *  BusCreator: '<S68>/Bus Creator'
   *  Constant: '<S68>/ChargerNo'
   *  Constant: '<S68>/Regional_Code'
   *  DataTypeConversion: '<S3>/Data Type Conversion13'
   *  DataTypeConversion: '<S3>/Data Type Conversion22'
   */
  rtY_CRM_c->CRM_SPN2560 = rtDW->CRM_SPN2560;
  rtY_CRM_c->En_CRM = rtDW->En_CRM;
  rtY_CRM_c->Regional_Code = 12.0F;
  rtY_CRM_c->ChargerNo = 23.0F;

  /* Outport: '<Root>/CTS' incorporates:
   *  BusCreator: '<S72>/Bus Creator'
   *  Constant: '<S72>/Time_Date'
   *  Constant: '<S72>/Time_Hour'
   *  Constant: '<S72>/Time_Minute'
   *  Constant: '<S72>/Time_Month'
   *  Constant: '<S72>/Time_Second'
   *  Constant: '<S72>/Time_Year'
   *  DataTypeConversion: '<S3>/Data Type Conversion15'
   */
  rtY_CTS_g->Time_Date = 25.0F;
  rtY_CTS_g->Time_Hour = 23.0F;
  rtY_CTS_g->Time_Minute = 42.0F;
  rtY_CTS_g->Time_Month = 5.0F;
  rtY_CTS_g->Time_Second = 47.0F;
  rtY_CTS_g->Time_Year = 2017.0F;
  rtY_CTS_g->En_CTS = rtDW->En_CTS;

  /* Outport: '<Root>/CML' incorporates:
   *  BusCreator: '<S67>/Bus Creator'
   *  Constant: '<S2>/OFC_MaxChrgCur'
   *  Constant: '<S2>/OFC_MaxChrgVolt'
   *  Constant: '<S2>/OFC_MinChrgCur'
   *  Constant: '<S2>/OFC_MinChrgVolt'
   *  DataTypeConversion: '<S3>/Data Type Conversion18'
   */
  rtY_CML_c->OFC_MinChrgVolt = 0.0F;
  rtY_CML_c->OFC_MinChrgCur = -400.0F;
  rtY_CML_c->OFC_MaxChrgCur = 100.0F;
  rtY_CML_c->OFC_MaxChrgVolt = 800.0F;
  rtY_CML_c->En_CML = rtDW->En_CML;

  /* Outport: '<Root>/CRO' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion16'
   *  DataTypeConversion: '<S3>/Data Type Conversion34'
   *  Switch: '<S29>/Switch1'
   */
  rtY_CRO_c->En_CRO = rtDW->En_CRO;
  rtY_CRO_c->CRO_SPN2830 = rtDW->CRO_SPN2830;

  /* Outport: '<Root>/CCS' incorporates:
   *  DataTypeConversion: '<S3>/Data Type Conversion20'
   *  DataTypeConversion: '<S3>/Data Type Conversion44'
   *  Gain: '<S5>/Gain'
   *  Switch: '<S2>/Switch1'
   */
  rtY_CCS_k->ChrgCur = DiscreteTimeIntegrator;
  rtY_CCS_k->ChrgVolt = rtb_En_CRM;
  rtY_CCS_k->En_CCS = rtDW->En_CCS;
  rtY_CCS_k->ChargeEnble = rtDW->ChargeEnable;

  /* Outport: '<Root>/CSD' incorporates:
   *  BusCreator: '<S70>/Bus Creator'
   *  Constant: '<S70>/Constant'
   *  DataTypeConversion: '<S3>/Data Type Conversion19'
   */
  rtY_CSD_e->ChargerNo = 0.0F;
  rtY_CSD_e->En_CSD = rtDW->En_CSD;

  /* Outport: '<Root>/CEM' incorporates:
   *  BusCreator: '<S65>/Bus Creator'
   *  Constant: '<S65>/Others'
   *  DataTypeConversion: '<S3>/Data Type Conversion17'
   *  DataTypeConversion: '<S3>/Data Type Conversion59'
   *  DataTypeConversion: '<S3>/Data Type Conversion60'
   *  DataTypeConversion: '<S3>/Data Type Conversion61'
   *  DataTypeConversion: '<S3>/Data Type Conversion65'
   *  DataTypeConversion: '<S3>/Data Type Conversion66'
   *  DataTypeConversion: '<S3>/Data Type Conversion67'
   *  DataTypeConversion: '<S65>/Data Type Conversion'
   *  Logic: '<S65>/Logical Operator'
   */
  rtY_CEM_b->En_CEM = rtDW->En_CEM;
  rtY_CEM_b->BRMTimeout = rtDW->BRMTimeout;
  rtY_CEM_b->BCPTimeoutAA = rtDW->BCPTimeout;
  rtY_CEM_b->BCSTimeout = rtDW->BCSTimeout;
  rtY_CEM_b->BCLTimeout00 = rtDW->BCLTimeout;
  rtY_CEM_b->BSTTimeout = rtDW->BSTTimeout;
  rtY_CEM_b->BSDTimeout = rtDW->BSDTimeout;
  rtY_CEM_b->BROTimeout = (real32_T)(rtDW->BROTimeout00 || rtDW->BROTimeoutAA);
  rtY_CEM_b->Others = 0.0F;

  /* Outport: '<Root>/CST' incorporates:
   *  BusCreator: '<S71>/Bus Creator'
   *  Constant: '<S33>/ChargeCondiAchieve[0|1]'
   *  Constant: '<S33>/ChargerConnetFault[0|1]'
   *  Constant: '<S33>/ChargerCurrentNotMatch[0|1]'
   *  Constant: '<S33>/ChargerOtherFault[0|1]'
   *  Constant: '<S33>/ChargerOverTemp[0|1]'
   *  Constant: '<S33>/ChargerOvertempInside[0|1]'
   *  Constant: '<S33>/ChargerScram[0|1]'
   *  Constant: '<S33>/ChargerVoltageAbnormal[0|1]'
   *  Constant: '<S33>/ElectrictyTransAbort[0|1]'
   *  DataTypeConversion: '<S33>/Data Type Conversion1'
   *  DataTypeConversion: '<S33>/Data Type Conversion2'
   *  DataTypeConversion: '<S3>/Data Type Conversion21'
   *  DataTypeConversion: '<S3>/Data Type Conversion50'
   */
  rtY_CST_k->En_CST = rtDW->En_CST;
  rtY_CST_k->ChargeCondiAchieve = 0.0F;
  rtY_CST_k->ManualStop = LogicalOperator_k_tmp;
  rtY_CST_k->ChargerFaultSt = rtDW->LogicalOperator_k;
  rtY_CST_k->BST_Stop = rtDW->BST_Stop;
  rtY_CST_k->ChargerOverTemp = 0.0F;
  rtY_CST_k->ChargerConnetFault = 0.0F;
  rtY_CST_k->ChargerOvertempInside = 0.0F;
  rtY_CST_k->ElectrictyTransAbort = 0.0F;
  rtY_CST_k->ChargerScram = 0.0F;
  rtY_CST_k->ChargerOtherFault = 0.0F;
  rtY_CST_k->ChargerCurrentNotMatch = 0.0F;
  rtY_CST_k->ChargerVoltageAbnormal = 0.0F;

  /* End of Outputs for SubSystem: '<Root>/SoftOFC' */
}

/* Model wrapper function for compatibility with a static main program */
void DCHCode_step(RT_MODEL *const rtM, int_T tid, BusB *rtU_BattFault, BusC
                  *rtU_BMSFault, BusA *rtU_BMSData, uint8_T rtU_OFC_Key,
                  boolean_T rtU_DCPlugIn, real32_T
                  rtU_VCUActuator_DIG_IN_DCPRLY_OV, real32_T *rtY_ChrgCur,
                  real32_T *rtY_ChrgVolt, real32_T *rtY_OFC_MinChrgVolt,
                  real32_T *rtY_OFC_MinChrgCur, real32_T *rtY_OFC_MaxChrgCur,
                  real32_T *rtY_OFC_MaxChrgVolt, real32_T
                  *rtY_Volt_K1K2_ChrgSide, real32_T *rtY_Volt_K1K2_VehSide,
                  boolean_T *rtY_CC2_Ready, real32_T *rtY_K1K2RlyCtrl, real32_T *
                  rtY_K3K4RlyCtrl, real32_T *rtY_SysChargeSt, real32_T
                  *rtY_ProcessMod, real32_T *rtY_ChargeEnble, real32_T
                  *rtY_ChargingTime, CHM *rtY_CHM_e, CRM *rtY_CRM_c, CTS
                  *rtY_CTS_g, CML *rtY_CML_c, CRO *rtY_CRO_c, CCS *rtY_CCS_k,
                  CSD *rtY_CSD_e, CEM *rtY_CEM_b, CST *rtY_CST_k)
{
  switch (tid) {
   case 0 :
    DCHCode_step0(rtM);
    break;

   case 1 :
    DCHCode_step1(rtM, rtU_BattFault, rtU_BMSFault, rtU_BMSData, rtU_OFC_Key,
                  rtU_DCPlugIn, rtU_VCUActuator_DIG_IN_DCPRLY_OV, rtY_ChrgCur,
                  rtY_ChrgVolt, rtY_OFC_MinChrgVolt, rtY_OFC_MinChrgCur,
                  rtY_OFC_MaxChrgCur, rtY_OFC_MaxChrgVolt,
                  rtY_Volt_K1K2_ChrgSide, rtY_Volt_K1K2_VehSide, rtY_CC2_Ready,
                  rtY_K1K2RlyCtrl, rtY_K3K4RlyCtrl, rtY_SysChargeSt,
                  rtY_ProcessMod, rtY_ChargeEnble, rtY_ChargingTime, rtY_CHM_e,
                  rtY_CRM_c, rtY_CTS_g, rtY_CML_c, rtY_CRO_c, rtY_CCS_k,
                  rtY_CSD_e, rtY_CEM_b, rtY_CST_k);
    break;

   default :
    break;
  }
}

/* Model initialize function */
void DCHCode_initialize(RT_MODEL *const rtM, BusB *rtU_BattFault, BusC
  *rtU_BMSFault, BusA *rtU_BMSData, uint8_T *rtU_OFC_Key, boolean_T
  *rtU_DCPlugIn, real32_T *rtU_VCUActuator_DIG_IN_DCPRLY_OV, real32_T
  *rtY_ChrgCur, real32_T *rtY_ChrgVolt, real32_T *rtY_OFC_MinChrgVolt, real32_T *
  rtY_OFC_MinChrgCur, real32_T *rtY_OFC_MaxChrgCur, real32_T
  *rtY_OFC_MaxChrgVolt, real32_T *rtY_Volt_K1K2_ChrgSide, real32_T
  *rtY_Volt_K1K2_VehSide, boolean_T *rtY_CC2_Ready, real32_T *rtY_K1K2RlyCtrl,
  real32_T *rtY_K3K4RlyCtrl, real32_T *rtY_SysChargeSt, real32_T *rtY_ProcessMod,
  real32_T *rtY_ChargeEnble, real32_T *rtY_ChargingTime, CHM *rtY_CHM_e, CRM
  *rtY_CRM_c, CTS *rtY_CTS_g, CML *rtY_CML_c, CRO *rtY_CRO_c, CCS *rtY_CCS_k,
  CSD *rtY_CSD_e, CEM *rtY_CEM_b, CST *rtY_CST_k)
{
  DW *rtDW = rtM->dwork;

  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));
  (rtM)->Timing.TaskCounters.cLimit[0] = 1;
  (rtM)->Timing.TaskCounters.cLimit[1] = 100;

  /* states (dwork) */
  (void) memset((void *)rtDW, 0,
                sizeof(DW));

  /* external inputs */
  *rtU_BattFault = DCHCode_rtZBusB;
  *rtU_BMSFault = DCHCode_rtZBusC;
  *rtU_BMSData = DCHCode_rtZBusA;
  *rtU_OFC_Key = 0U;
  *rtU_DCPlugIn = false;
  *rtU_VCUActuator_DIG_IN_DCPRLY_OV = 0.0F;

  /* external outputs */
  (*rtY_ChrgCur) = 0.0F;
  (*rtY_ChrgVolt) = 0.0F;
  (*rtY_OFC_MinChrgVolt) = 0.0F;
  (*rtY_OFC_MinChrgCur) = 0.0F;
  (*rtY_OFC_MaxChrgCur) = 0.0F;
  (*rtY_OFC_MaxChrgVolt) = 0.0F;
  (*rtY_Volt_K1K2_ChrgSide) = 0.0F;
  (*rtY_Volt_K1K2_VehSide) = 0.0F;
  (*rtY_CC2_Ready) = false;
  (*rtY_K1K2RlyCtrl) = 0.0F;
  (*rtY_K3K4RlyCtrl) = 0.0F;
  (*rtY_SysChargeSt) = 0.0F;
  (*rtY_ProcessMod) = 0.0F;
  (*rtY_ChargeEnble) = 0.0F;
  (*rtY_ChargingTime) = 0.0F;
  (*rtY_CHM_e) = DCHCode_rtZCHM;
  (*rtY_CRM_c) = DCHCode_rtZCRM;
  (*rtY_CTS_g) = DCHCode_rtZCTS;
  (*rtY_CML_c) = DCHCode_rtZCML;
  (*rtY_CRO_c) = DCHCode_rtZCRO;
  (*rtY_CCS_k) = DCHCode_rtZCCS;
  (*rtY_CSD_e) = DCHCode_rtZCSD;
  (*rtY_CEM_b) = DCHCode_rtZCEM;
  (*rtY_CST_k) = DCHCode_rtZCST;

  /* SystemInitialize for Atomic SubSystem: '<Root>/SoftOFC' */
  /* InitializeConditions for DiscreteIntegrator: '<S39>/Discrete-Time Integrator' */
  rtDW->DiscreteTimeIntegrator_PrevRese = 0;

  /* InitializeConditions for DiscreteIntegrator: '<S39>/Discrete-Time Integrator1' */
  rtDW->DiscreteTimeIntegrator1_PrevRes = 0;

  /* InitializeConditions for DiscreteIntegrator: '<S39>/Discrete-Time Integrator2' */
  rtDW->DiscreteTimeIntegrator2_PrevRes = 0;

  /* InitializeConditions for DiscreteIntegrator: '<S6>/Discrete-Time Integrator' */
  rtDW->DiscreteTimeIntegrator_PrevRe_h = 0;
  rtDW->DiscreteTimeIntegrator_IC_LOADI = 1U;

  /* InitializeConditions for DiscreteIntegrator: '<S32>/Discrete-Time Integrator' */
  rtDW->DiscreteTimeIntegrator_DSTATE = 0.0;
  rtDW->DiscreteTimeIntegrator_PrevR_hq = 0;

  /* InitializeConditions for DiscreteIntegrator: '<S31>/Discrete-Time Integrator' */
  rtDW->DiscreteTimeIntegrator_DSTATE_o = 0.0;
  rtDW->DiscreteTimeIntegrator_PrevRe_e = 0;

  /* InitializeConditions for DiscreteIntegrator: '<S30>/Discrete-Time Integrator' */
  rtDW->DiscreteTimeIntegrator_DSTATE_n = 0.0;
  rtDW->DiscreteTimeIntegrator_PrevR_ev = 0;

  /* InitializeConditions for DiscreteIntegrator: '<S5>/Discrete-Time Integrator' */
  rtDW->DiscreteTimeIntegrator_DSTATE_p = 0.0F;
  rtDW->DiscreteTimeIntegrator_PrevRe_c = 0;

  /* InitializeConditions for DiscreteIntegrator: '<S7>/Discrete-Time Integrator' */
  rtDW->DiscreteTimeIntegrator_PrevR_c3 = 2;

  /* InitializeConditions for DiscreteIntegrator: '<S35>/Discrete-Time Integrator' */
  rtDW->DiscreteTimeIntegrator_PrevR_cy = 2;

  /* SystemInitialize for Chart: '<S8>/Plug Logic' */
  rtDW->temporalCounter_i1_j = 0U;
  rtDW->is_active_c4_DCHCode = 0U;
  rtDW->is_c4_DCHCode = IN_NO_ACTIVE_CHILD;
  rtDW->U1 = 0.0F;
  rtDW->CC2_Ready = false;
  rtDW->ELockSt = false;

  /* SystemInitialize for Chart: '<S3>/Charge Logic Control' */
  rtDW->is_SystemON = IN_NO_ACTIVE_CHILD;
  rtDW->is_ActiveDischarge = IN_NO_ACTIVE_CHILD;
  rtDW->is_MainBlock = IN_NO_ACTIVE_CHILD;
  rtDW->is_ChrgingDuration = IN_NO_ACTIVE_CHILD;
  rtDW->is_ChargingManage = IN_NO_ACTIVE_CHILD;
  rtDW->is_HandShake = IN_NO_ACTIVE_CHILD;
  rtDW->is_HandshakeIdentification = IN_NO_ACTIVE_CHILD;
  rtDW->is_InsulationMonitoring = IN_NO_ACTIVE_CHILD;
  rtDW->is_ParameterConfiguration = IN_NO_ACTIVE_CHILD;
  rtDW->is_PhyConnConfirm = IN_NO_ACTIVE_CHILD;
  rtDW->is_PowerDown = IN_NO_ACTIVE_CHILD;
  rtDW->temporalCounter_i1 = 0U;
  rtDW->is_active_c5_DCHCode = 0U;
  rtDW->is_c5_DCHCode = IN_NO_ACTIVE_CHILD;
  rtDW->ReTrySt = false;
  rtDW->ParaConfigFinish = false;
  rtDW->IS_MonFinish = false;
  rtDW->IS_ActDisChrgFinish = false;
  rtDW->IS_ActDisChrgEn = false;
  rtDW->HandShakeIdentFinish = false;
  rtDW->HandShakeFinish = false;
  rtDW->ChargeFinish = false;
  rtDW->PD_ActDisChrgEn = false;
  rtDW->PD_ActDisChrgFinish = false;
  rtDW->TimeoutCouter = 0U;
  rtDW->PowerDownInitFinish = false;
  rtDW->CtimeEn0 = false;
  rtDW->CtimeEn1 = false;
  rtDW->CtimeEn2 = false;
  rtDW->PhyConnFinish = false;
  rtDW->K1K2RlyCtrl = false;
  rtDW->K3K4RlyCtrl = false;
  rtDW->ELockCtrl = false;
  rtDW->En_CTS = false;
  rtDW->En_CST = false;
  rtDW->En_CSD = false;
  rtDW->En_CRO = false;
  rtDW->En_CRM = false;
  rtDW->En_CML = false;
  rtDW->En_CHM = false;
  rtDW->En_CEM = false;
  rtDW->En_CCS = false;
  rtDW->SysChargSt = 0U;
  rtDW->PreChrgEn = false;
  rtDW->IS_VoltEn = false;
  rtDW->CRO_SPN2830 = 0U;
  rtDW->CRM_SPN2560 = 0U;
  rtDW->BRMTimeout = false;
  rtDW->BSTTimeout = false;
  rtDW->BSDTimeout = false;
  rtDW->BROTimeoutAA = false;
  rtDW->BROTimeout00 = false;
  rtDW->BHMTimeout = false;
  rtDW->BCSTimeout = false;
  rtDW->BCPTimeout = false;
  rtDW->BCLTimeout = false;
  rtDW->ActDisChrgSw = false;
  rtDW->ProcessMod = 0U;
  rtDW->BST_Stop = false;
  rtDW->ChargeEnable = false;

  /* End of SystemInitialize for SubSystem: '<Root>/SoftOFC' */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
