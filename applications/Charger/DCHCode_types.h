/*
 * File: DCHCode_types.h
 *
 * Code generated for Simulink model 'DCHCode'.
 *
 * Model version                  : 6.7
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Sat Feb 12 22:32:44 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex-M
 * Emulation hardware selection:
 *    Differs from embedded hardware (Custom Processor->MATLAB Host Computer)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_DCHCode_types_h_
#define RTW_HEADER_DCHCode_types_h_
#include "rtwtypes.h"

/* Model Code Variants */
#ifndef DEFINED_TYPEDEF_FOR_BusB_
#define DEFINED_TYPEDEF_FOR_BusB_

typedef struct {
  real32_T Connector_State_BSM_FromRealBMS;
  real32_T Isolation_BSM_FromRealBMS;
  real32_T Cell_Voltage_BSM_FromRealBMS;
  real32_T Battery_SOC_BSM_FromRealBMS;
  real32_T Charge_Over_Current_BSM_FromRealBMS;
  real32_T Over_Temperature_BSM_FromRealBMS;
} BusB;

#endif

#ifndef DEFINED_TYPEDEF_FOR_BusC_
#define DEFINED_TYPEDEF_FOR_BusC_

typedef struct {
  real32_T BST_EngChg_Flt_Point2VltMonitor_BST_FromRealBMS;
  real32_T BST_EngChg_Flt_HVContactorFlt_BST_FromRealBMS;
  real32_T End_Charging_VoltageAbnormal_BST_FromRealBMS;
  real32_T End_Charging_OtherFault_BST_FromRealBMS;
  real32_T End_Charging_Isolation_BST_FromRealBMS;
  real32_T End_Charging_ConnectorOverTemp_BST_FromRealBMS;
  real32_T End_Charging_BMUOverTemp_BST_FromRealBMS;
  real32_T End_Charging_ConnectorFault_BST_FromRealBMS;
  real32_T End_Charging_OverTempFault_BST_FromRealBMS;
  real32_T End_Charging_OverCurrent_BST_FromRealBMS;
} BusC;

#endif

#ifndef DEFINED_TYPEDEF_FOR_BusA_
#define DEFINED_TYPEDEF_FOR_BusA_

typedef struct {
  real32_T BRM_RCVFlag;
  real32_T BHM_ReceiveFlag;
  real32_T BRO_ReceiveFlag;
  real32_T BCS_RCVFlag;
  real32_T BCP_RCVFlag;
  real32_T BEM_ReceiveFlag;
  real32_T BCL_ReceiveFlag;
  real32_T BSD_ReceiveFlag;
  real32_T BST_ReceiveFlag;
  real32_T Battery_Charge_Ready_BRO_FromRealBMS;
  real32_T Charging_Allow_BSM_FromRealBMS;
  real32_T BSM_ReceiveFlag;
  real32_T Charge_mode_BCL_FromRealBMS;
  real32_T Current_Demand_BCL_FromRealBMS;
  real32_T Voltage_Demand_BCL_FromRealBMS;
  real32_T BCP_Bat_CurVolt;
  real32_T BHM_MaxAllow_Chg_TotVol_BHM_FromRealBMS;
  real32_T PackCur;
  real32_T LinkVolt;
} BusA;

#endif

#ifndef DEFINED_TYPEDEF_FOR_ChrgParaSet_
#define DEFINED_TYPEDEF_FOR_ChrgParaSet_

typedef struct {
  real32_T Time_PhyCon;
  real32_T Time_HandShake;
  real32_T Time_HSIdentification;
  real32_T Time_ReceiveBCP;
  real32_T Time_ReceiveBRO;
  real32_T Time_ReceiveBROAA;
  real32_T Time_ChrgWait;
  real32_T Time_ReceiveBCL;
  real32_T Time_ReceiveBCS;
  real32_T Time_ReceiveActDisChrg;
  real32_T Time_ADReceiveBSD;
  real32_T Time_PDReceiveBSD;
  real32_T Time_ReceiveBST;
  real32_T Time_CurrentDown;
  real32_T InsulationVolt;
  real32_T DeltaVolt_K1K2;
  real32_T ChgSideVolt_K1K2;
  real32_T Cur_Threshold;
  real32_T SendCHMTimes;
  real32_T CHMSendWaitTime;
  real32_T CHMEndWaitTime;
  real32_T CRMSend1WaitTime;
  real32_T CRMSend2WaitTime;
  real32_T CRMEndWaitTime;
  real32_T CTSCMLSendWaitTime;
  real32_T CROSend1WaitTime;
  real32_T CROSend2WaitTime;
  real32_T CCSSendWaitTime;
  real32_T CSTSendWaitTime;
  real32_T CSDSendWaitTime;
} ChrgParaSet;

#endif

#ifndef DEFINED_TYPEDEF_FOR_CCS_
#define DEFINED_TYPEDEF_FOR_CCS_

typedef struct {
  real32_T ChrgCur;
  real32_T ChrgVolt;
  real32_T En_CCS;
  real32_T ChargeEnble;
  real32_T ChargingTime;
} CCS;

#endif

#ifndef DEFINED_TYPEDEF_FOR_CEM_
#define DEFINED_TYPEDEF_FOR_CEM_

typedef struct {
  real32_T En_CEM;
  real32_T BRMTimeout;
  real32_T BCPTimeoutAA;
  real32_T BCSTimeout;
  real32_T BCLTimeout00;
  real32_T BSTTimeout;
  real32_T BSDTimeout;
  real32_T BROTimeout;
  real32_T Others;
} CEM;

#endif

#ifndef DEFINED_TYPEDEF_FOR_CHM_
#define DEFINED_TYPEDEF_FOR_CHM_

typedef struct {
  real32_T CommProtocolVersion;
  real32_T En_CHM;
} CHM;

#endif

#ifndef DEFINED_TYPEDEF_FOR_CML_
#define DEFINED_TYPEDEF_FOR_CML_

typedef struct {
  real32_T OFC_MinChrgVolt;
  real32_T OFC_MinChrgCur;
  real32_T OFC_MaxChrgCur;
  real32_T OFC_MaxChrgVolt;
  real32_T En_CML;
} CML;

#endif

#ifndef DEFINED_TYPEDEF_FOR_CRM_
#define DEFINED_TYPEDEF_FOR_CRM_

typedef struct {
  real32_T CRM_SPN2560;
  real32_T En_CRM;
  real32_T Regional_Code;
  real32_T ChargerNo;
} CRM;

#endif

#ifndef DEFINED_TYPEDEF_FOR_CRO_
#define DEFINED_TYPEDEF_FOR_CRO_

typedef struct {
  real32_T En_CRO;
  real32_T CRO_SPN2830;
} CRO;

#endif

#ifndef DEFINED_TYPEDEF_FOR_CSD_
#define DEFINED_TYPEDEF_FOR_CSD_

typedef struct {
  real32_T ChrgEnergy;
  real32_T ChargerNo;
  real32_T En_CSD;
  real32_T ChargingTime;
} CSD;

#endif

#ifndef DEFINED_TYPEDEF_FOR_CST_
#define DEFINED_TYPEDEF_FOR_CST_

typedef struct {
  real32_T En_CST;
  real32_T ChargeCondiAchieve;
  real32_T ManualStop;
  real32_T ChargerFaultSt;
  real32_T BST_Stop;
  real32_T ChargerOverTemp;
  real32_T ChargerConnetFault;
  real32_T ChargerOvertempInside;
  real32_T ElectrictyTransAbort;
  real32_T ChargerScram;
  real32_T ChargerOtherFault;
  real32_T ChargerCurrentNotMatch;
  real32_T ChargerVoltageAbnormal;
} CST;

#endif

#ifndef DEFINED_TYPEDEF_FOR_CTS_
#define DEFINED_TYPEDEF_FOR_CTS_

typedef struct {
  real32_T Time_Date;
  real32_T Time_Hour;
  real32_T Time_Minute;
  real32_T Time_Month;
  real32_T Time_Second;
  real32_T Time_Year;
  real32_T En_CTS;
} CTS;

#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM RT_MODEL;

#endif                                 /* RTW_HEADER_DCHCode_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
