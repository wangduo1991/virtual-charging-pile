/*
 * File: DCHCode_private.h
 *
 * Code generated for Simulink model 'DCHCode'.
 *
 * Model version                  : 6.7
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Sat Feb 12 22:32:44 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex-M
 * Emulation hardware selection:
 *    Differs from embedded hardware (Custom Processor->MATLAB Host Computer)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_DCHCode_private_h_
#define RTW_HEADER_DCHCode_private_h_
#include "rtwtypes.h"

extern real32_T rt_powf_snf(real32_T u0, real32_T u1);
extern real32_T look1_iflf_binlx(real32_T u0, const real32_T bp0[], const
  real32_T table[], uint32_T maxIndex);

#endif                                 /* RTW_HEADER_DCHCode_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
