/*
 * File: DCHCode.h
 *
 * Code generated for Simulink model 'DCHCode'.
 *
 * Model version                  : 6.7
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Sat Feb 12 22:32:44 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex-M
 * Emulation hardware selection:
 *    Differs from embedded hardware (Custom Processor->MATLAB Host Computer)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_DCHCode_h_
#define RTW_HEADER_DCHCode_h_
#include <math.h>
#include <string.h>
#ifndef DCHCode_COMMON_INCLUDES_
#define DCHCode_COMMON_INCLUDES_
#include "rtwtypes.h"
#endif                                 /* DCHCode_COMMON_INCLUDES_ */

#include "DCHCode_types.h"
#include "rt_nonfinite.h"
#include "rt_defines.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetRootDWork
#define rtmGetRootDWork(rtm)           ((rtm)->dwork)
#endif

#ifndef rtmSetRootDWork
#define rtmSetRootDWork(rtm, val)      ((rtm)->dwork = (val))
#endif

#ifndef rtmCounterLimit
#define rtmCounterLimit(rtm, idx)      ((rtm)->Timing.TaskCounters.cLimit[(idx)])
#endif

#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

#ifndef rtmStepTask
#define rtmStepTask(rtm, idx)          ((rtm)->Timing.TaskCounters.TID[(idx)] == 0)
#endif

#ifndef rtmTaskCounter
#define rtmTaskCounter(rtm, idx)       ((rtm)->Timing.TaskCounters.TID[(idx)])
#endif

#define DCHCode_M                      (rtM)

/* Block signals and states (default storage) for system '<Root>' */
typedef struct {
  ChrgParaSet BusConversion_InsertedFor_Charg;
  real_T Delay3_DSTATE;                /* '<S8>/Delay3' */
  real_T DiscreteTimeIntegrator_DSTATE;/* '<S32>/Discrete-Time Integrator' */
  real_T DiscreteTimeIntegrator_DSTATE_o;/* '<S31>/Discrete-Time Integrator' */
  real_T DiscreteTimeIntegrator_DSTATE_n;/* '<S30>/Discrete-Time Integrator' */
  real32_T Delay2;                     /* '<S3>/Delay2' */
  real32_T U1;                         /* '<S8>/Plug Logic' */
  real32_T DiscreteTimeIntegrator_DSTATE_l;/* '<S39>/Discrete-Time Integrator' */
  real32_T DiscreteTimeIntegrator1_DSTATE;/* '<S39>/Discrete-Time Integrator1' */
  real32_T DiscreteTimeIntegrator2_DSTATE;/* '<S39>/Discrete-Time Integrator2' */
  real32_T Delay_DSTATE;               /* '<S3>/Delay' */
  real32_T Delay2_DSTATE;              /* '<S3>/Delay2' */
  real32_T Delay_DSTATE_m;             /* '<S9>/Delay' */
  real32_T Delay_DSTATE_p;             /* '<S2>/Delay' */
  real32_T Delay1_DSTATE;              /* '<S2>/Delay1' */
  real32_T Delay_DSTATE_n;             /* '<S6>/Delay' */
  real32_T DiscreteTimeIntegrator_DSTAT_o5;/* '<S6>/Discrete-Time Integrator' */
  real32_T Delay1_DSTATE_c;            /* '<S3>/Delay1' */
  real32_T Delay4_DSTATE;              /* '<S3>/Delay4' */
  real32_T DiscreteTimeIntegrator_DSTATE_p;/* '<S5>/Discrete-Time Integrator' */
  real32_T DiscreteTimeIntegrator_DSTATE_i;/* '<S7>/Discrete-Time Integrator' */
  real32_T DiscreteTimeIntegrator_DSTATE_c;/* '<S35>/Discrete-Time Integrator' */
  uint32_T temporalCounter_i1;         /* '<S3>/Charge Logic Control' */
  int8_T DiscreteTimeIntegrator_PrevRese;/* '<S39>/Discrete-Time Integrator' */
  int8_T DiscreteTimeIntegrator1_PrevRes;/* '<S39>/Discrete-Time Integrator1' */
  int8_T DiscreteTimeIntegrator2_PrevRes;/* '<S39>/Discrete-Time Integrator2' */
  int8_T DiscreteTimeIntegrator_PrevRe_h;/* '<S6>/Discrete-Time Integrator' */
  int8_T DiscreteTimeIntegrator_PrevR_hq;/* '<S32>/Discrete-Time Integrator' */
  int8_T DiscreteTimeIntegrator_PrevRe_e;/* '<S31>/Discrete-Time Integrator' */
  int8_T DiscreteTimeIntegrator_PrevR_ev;/* '<S30>/Discrete-Time Integrator' */
  int8_T DiscreteTimeIntegrator_PrevRe_c;/* '<S5>/Discrete-Time Integrator' */
  int8_T DiscreteTimeIntegrator_PrevR_c3;/* '<S7>/Discrete-Time Integrator' */
  int8_T DiscreteTimeIntegrator_PrevR_cy;/* '<S35>/Discrete-Time Integrator' */
  uint8_T DataTypeConversion28;        /* '<S3>/Data Type Conversion28' */
  uint8_T DataTypeConversion31;        /* '<S3>/Data Type Conversion31' */
  uint8_T SysChargSt;                  /* '<S3>/Charge Logic Control' */
  uint8_T CRO_SPN2830;                 /* '<S3>/Charge Logic Control' */
  uint8_T CRM_SPN2560;                 /* '<S3>/Charge Logic Control' */
  uint8_T ProcessMod;                  /* '<S3>/Charge Logic Control' */
  uint8_T DiscreteTimeIntegrator_IC_LOADI;/* '<S6>/Discrete-Time Integrator' */
  uint8_T is_active_c5_DCHCode;        /* '<S3>/Charge Logic Control' */
  uint8_T is_c5_DCHCode;               /* '<S3>/Charge Logic Control' */
  uint8_T is_SystemON;                 /* '<S3>/Charge Logic Control' */
  uint8_T is_MainBlock;                /* '<S3>/Charge Logic Control' */
  uint8_T is_InsulationMonitoring;     /* '<S3>/Charge Logic Control' */
  uint8_T is_HandShake;                /* '<S3>/Charge Logic Control' */
  uint8_T is_PhyConnConfirm;           /* '<S3>/Charge Logic Control' */
  uint8_T is_ParameterConfiguration;   /* '<S3>/Charge Logic Control' */
  uint8_T is_ChrgingDuration;          /* '<S3>/Charge Logic Control' */
  uint8_T is_ChargingManage;           /* '<S3>/Charge Logic Control' */
  uint8_T is_HandshakeIdentification;  /* '<S3>/Charge Logic Control' */
  uint8_T is_ActiveDischarge;          /* '<S3>/Charge Logic Control' */
  uint8_T is_PowerDown;                /* '<S3>/Charge Logic Control' */
  uint8_T TimeoutCouter;               /* '<S3>/Charge Logic Control' */
  uint8_T is_active_c4_DCHCode;        /* '<S8>/Plug Logic' */
  uint8_T is_c4_DCHCode;               /* '<S8>/Plug Logic' */
  uint8_T temporalCounter_i1_j;        /* '<S8>/Plug Logic' */
  boolean_T DataTypeConversion24;      /* '<S3>/Data Type Conversion24' */
  boolean_T DataTypeConversion25;      /* '<S3>/Data Type Conversion25' */
  boolean_T DataTypeConversion29;      /* '<S3>/Data Type Conversion29' */
  boolean_T DataTypeConversion30;      /* '<S3>/Data Type Conversion30' */
  boolean_T DataTypeConversion43;      /* '<S3>/Data Type Conversion43' */
  boolean_T DataTypeConversion40;      /* '<S3>/Data Type Conversion40' */
  boolean_T DataTypeConversion37;      /* '<S3>/Data Type Conversion37' */
  boolean_T DataTypeConversion38;      /* '<S3>/Data Type Conversion38' */
  boolean_T DataTypeConversion36;      /* '<S3>/Data Type Conversion36' */
  boolean_T DataTypeConversion35;      /* '<S3>/Data Type Conversion35' */
  boolean_T LogicalOperator;           /* '<S37>/Logical Operator' */
  boolean_T DataTypeConversion51;      /* '<S3>/Data Type Conversion51' */
  boolean_T LogicalOperator1;          /* '<S37>/Logical Operator1' */
  boolean_T LogicalOperator_k;         /* '<S33>/Logical Operator' */
  boolean_T CtimeEn0;                  /* '<S3>/Charge Logic Control' */
  boolean_T CtimeEn1;                  /* '<S3>/Charge Logic Control' */
  boolean_T CtimeEn2;                  /* '<S3>/Charge Logic Control' */
  boolean_T PhyConnFinish;             /* '<S3>/Charge Logic Control' */
  boolean_T K1K2RlyCtrl;               /* '<S3>/Charge Logic Control' */
  boolean_T K3K4RlyCtrl;               /* '<S3>/Charge Logic Control' */
  boolean_T ELockCtrl;                 /* '<S3>/Charge Logic Control' */
  boolean_T En_CTS;                    /* '<S3>/Charge Logic Control' */
  boolean_T En_CST;                    /* '<S3>/Charge Logic Control' */
  boolean_T En_CSD;                    /* '<S3>/Charge Logic Control' */
  boolean_T En_CRO;                    /* '<S3>/Charge Logic Control' */
  boolean_T En_CRM;                    /* '<S3>/Charge Logic Control' */
  boolean_T En_CML;                    /* '<S3>/Charge Logic Control' */
  boolean_T En_CHM;                    /* '<S3>/Charge Logic Control' */
  boolean_T En_CEM;                    /* '<S3>/Charge Logic Control' */
  boolean_T En_CCS;                    /* '<S3>/Charge Logic Control' */
  boolean_T PreChrgEn;                 /* '<S3>/Charge Logic Control' */
  boolean_T IS_VoltEn;                 /* '<S3>/Charge Logic Control' */
  boolean_T BRMTimeout;                /* '<S3>/Charge Logic Control' */
  boolean_T BSTTimeout;                /* '<S3>/Charge Logic Control' */
  boolean_T BSDTimeout;                /* '<S3>/Charge Logic Control' */
  boolean_T BROTimeoutAA;              /* '<S3>/Charge Logic Control' */
  boolean_T BROTimeout00;              /* '<S3>/Charge Logic Control' */
  boolean_T BHMTimeout;                /* '<S3>/Charge Logic Control' */
  boolean_T BCSTimeout;                /* '<S3>/Charge Logic Control' */
  boolean_T BCPTimeout;                /* '<S3>/Charge Logic Control' */
  boolean_T BCLTimeout;                /* '<S3>/Charge Logic Control' */
  boolean_T ActDisChrgSw;              /* '<S3>/Charge Logic Control' */
  boolean_T BST_Stop;                  /* '<S3>/Charge Logic Control' */
  boolean_T ChargeEnable;              /* '<S3>/Charge Logic Control' */
  boolean_T CC2_Ready;                 /* '<S8>/Plug Logic' */
  boolean_T ELockSt;                   /* '<S8>/Plug Logic' */
  boolean_T UnitDelay_DSTATE;          /* '<S39>/Unit Delay' */
  boolean_T UnitDelay1_DSTATE;         /* '<S39>/Unit Delay1' */
  boolean_T UnitDelay2_DSTATE;         /* '<S39>/Unit Delay2' */
  boolean_T Relay1_Mode;               /* '<S11>/Relay1' */
  boolean_T Relay_Mode;                /* '<S11>/Relay' */
  boolean_T Relay2_Mode;               /* '<S11>/Relay2' */
  boolean_T ReTrySt;                   /* '<S3>/Charge Logic Control' */
  boolean_T ParaConfigFinish;          /* '<S3>/Charge Logic Control' */
  boolean_T IS_MonFinish;              /* '<S3>/Charge Logic Control' */
  boolean_T IS_ActDisChrgFinish;       /* '<S3>/Charge Logic Control' */
  boolean_T IS_ActDisChrgEn;           /* '<S3>/Charge Logic Control' */
  boolean_T HandShakeIdentFinish;      /* '<S3>/Charge Logic Control' */
  boolean_T HandShakeFinish;           /* '<S3>/Charge Logic Control' */
  boolean_T ChargeFinish;              /* '<S3>/Charge Logic Control' */
  boolean_T PD_ActDisChrgEn;           /* '<S3>/Charge Logic Control' */
  boolean_T PD_ActDisChrgFinish;       /* '<S3>/Charge Logic Control' */
  boolean_T PowerDownInitFinish;       /* '<S3>/Charge Logic Control' */
} DW;

/* Constant parameters (default storage) */
typedef struct {
  /* Computed Parameter: BasedOnCurrentPermittion_tableD
   * Referenced by: '<S11>/BasedOnCurrentPermittion'
   */
  real32_T BasedOnCurrentPermittion_tableD[3];

  /* Pooled Parameter (Expression: [-5,0,5])
   * Referenced by:
   *   '<S11>/BasedOnCurrentPermittion'
   *   '<S11>/BasedOnVoltagePermittion'
   */
  real32_T pooled14[3];

  /* Computed Parameter: BasedOnVoltagePermittion_tableD
   * Referenced by: '<S11>/BasedOnVoltagePermittion'
   */
  real32_T BasedOnVoltagePermittion_tableD[3];
} ConstP;

/* Real-time Model Data Structure */
struct tag_RTM {
  const char_T *errorStatus;
  DW *dwork;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    struct {
      uint8_T TID[2];
      uint8_T cLimit[2];
    } TaskCounters;
  } Timing;
};

/* External data declarations for dependent source files */
extern const BusB DCHCode_rtZBusB;     /* BusB ground */
extern const BusC DCHCode_rtZBusC;     /* BusC ground */
extern const BusA DCHCode_rtZBusA;     /* BusA ground */
extern const CCS DCHCode_rtZCCS;       /* CCS ground */
extern const CEM DCHCode_rtZCEM;       /* CEM ground */
extern const CHM DCHCode_rtZCHM;       /* CHM ground */
extern const CML DCHCode_rtZCML;       /* CML ground */
extern const CRM DCHCode_rtZCRM;       /* CRM ground */
extern const CRO DCHCode_rtZCRO;       /* CRO ground */
extern const CSD DCHCode_rtZCSD;       /* CSD ground */
extern const CST DCHCode_rtZCST;       /* CST ground */
extern const CTS DCHCode_rtZCTS;       /* CTS ground */

/* Constant parameters (default storage) */
extern const ConstP rtConstP;

/* Model entry point functions */
extern void DCHCode_initialize(RT_MODEL *const rtM, BusB *rtU_BattFault, BusC
  *rtU_BMSFault, BusA *rtU_BMSData, uint8_T *rtU_OFC_Key, boolean_T
  *rtU_DCPlugIn, real32_T *rtU_VCUActuator_DIG_IN_DCPRLY_OV, real32_T
  *rtY_ChrgCur, real32_T *rtY_ChrgVolt, real32_T *rtY_OFC_MinChrgVolt, real32_T *
  rtY_OFC_MinChrgCur, real32_T *rtY_OFC_MaxChrgCur, real32_T
  *rtY_OFC_MaxChrgVolt, real32_T *rtY_Volt_K1K2_ChrgSide, real32_T
  *rtY_Volt_K1K2_VehSide, boolean_T *rtY_CC2_Ready, real32_T *rtY_K1K2RlyCtrl,
  real32_T *rtY_K3K4RlyCtrl, real32_T *rtY_SysChargeSt, real32_T *rtY_ProcessMod,
  real32_T *rtY_ChargeEnble, real32_T *rtY_ChargingTime, CHM *rtY_CHM_e, CRM
  *rtY_CRM_c, CTS *rtY_CTS_g, CML *rtY_CML_c, CRO *rtY_CRO_c, CCS *rtY_CCS_k,
  CSD *rtY_CSD_e, CEM *rtY_CEM_b, CST *rtY_CST_k);
extern void DCHCode_step0(RT_MODEL *const rtM);
extern void DCHCode_step1(RT_MODEL *const rtM, BusB *rtU_BattFault, BusC
  *rtU_BMSFault, BusA *rtU_BMSData, uint8_T rtU_OFC_Key, boolean_T rtU_DCPlugIn,
  real32_T rtU_VCUActuator_DIG_IN_DCPRLY_OV, real32_T *rtY_ChrgCur, real32_T
  *rtY_ChrgVolt, real32_T *rtY_OFC_MinChrgVolt, real32_T *rtY_OFC_MinChrgCur,
  real32_T *rtY_OFC_MaxChrgCur, real32_T *rtY_OFC_MaxChrgVolt, real32_T
  *rtY_Volt_K1K2_ChrgSide, real32_T *rtY_Volt_K1K2_VehSide, boolean_T
  *rtY_CC2_Ready, real32_T *rtY_K1K2RlyCtrl, real32_T *rtY_K3K4RlyCtrl, real32_T
  *rtY_SysChargeSt, real32_T *rtY_ProcessMod, real32_T *rtY_ChargeEnble,
  real32_T *rtY_ChargingTime, CHM *rtY_CHM_e, CRM *rtY_CRM_c, CTS *rtY_CTS_g,
  CML *rtY_CML_c, CRO *rtY_CRO_c, CCS *rtY_CCS_k, CSD *rtY_CSD_e, CEM *rtY_CEM_b,
  CST *rtY_CST_k);
extern void DCHCode_step(RT_MODEL *const rtM, int_T tid, BusB *rtU_BattFault,
  BusC *rtU_BMSFault, BusA *rtU_BMSData, uint8_T rtU_OFC_Key, boolean_T
  rtU_DCPlugIn, real32_T rtU_VCUActuator_DIG_IN_DCPRLY_OV, real32_T *rtY_ChrgCur,
  real32_T *rtY_ChrgVolt, real32_T *rtY_OFC_MinChrgVolt, real32_T
  *rtY_OFC_MinChrgCur, real32_T *rtY_OFC_MaxChrgCur, real32_T
  *rtY_OFC_MaxChrgVolt, real32_T *rtY_Volt_K1K2_ChrgSide, real32_T
  *rtY_Volt_K1K2_VehSide, boolean_T *rtY_CC2_Ready, real32_T *rtY_K1K2RlyCtrl,
  real32_T *rtY_K3K4RlyCtrl, real32_T *rtY_SysChargeSt, real32_T *rtY_ProcessMod,
  real32_T *rtY_ChargeEnble, real32_T *rtY_ChargingTime, CHM *rtY_CHM_e, CRM
  *rtY_CRM_c, CTS *rtY_CTS_g, CML *rtY_CML_c, CRO *rtY_CRO_c, CCS *rtY_CCS_k,
  CSD *rtY_CSD_e, CEM *rtY_CEM_b, CST *rtY_CST_k);

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S3>/Charge' : Unused code path elimination
 * Block '<S36>/Compare' : Unused code path elimination
 * Block '<S36>/Constant' : Unused code path elimination
 * Block '<S3>/Data Type Conversion11' : Unused code path elimination
 * Block '<S3>/Data Type Conversion53' : Unused code path elimination
 * Block '<S3>/Data Type Conversion56' : Unused code path elimination
 * Block '<S3>/Data Type Conversion57' : Unused code path elimination
 * Block '<S3>/Data Type Conversion64' : Unused code path elimination
 * Block '<S3>/Data Type Conversion68' : Unused code path elimination
 * Block '<S3>/Data Type Conversion69' : Unused code path elimination
 * Block '<S3>/Gain' : Unused code path elimination
 * Block '<S3>/Gain1' : Unused code path elimination
 * Block '<S24>/Data Type Conversion' : Eliminate redundant data type conversion
 * Block '<S8>/Data Type Conversion' : Eliminate redundant data type conversion
 * Block '<S8>/Data Type Conversion1' : Eliminate redundant data type conversion
 * Block '<S2>/Gain' : Eliminated nontunable gain of 1
 * Block '<S1>/Data Type Conversion' : Eliminate redundant data type conversion
 * Block '<S33>/Data Type Conversion' : Eliminate redundant data type conversion
 * Block '<S3>/Data Type Conversion' : Eliminate redundant data type conversion
 * Block '<S3>/Data Type Conversion1' : Eliminate redundant data type conversion
 * Block '<S3>/Data Type Conversion2' : Eliminate redundant data type conversion
 * Block '<S3>/Data Type Conversion27' : Eliminate redundant data type conversion
 * Block '<S3>/Data Type Conversion3' : Eliminate redundant data type conversion
 * Block '<S3>/Data Type Conversion32' : Eliminate redundant data type conversion
 * Block '<S3>/Data Type Conversion33' : Eliminate redundant data type conversion
 * Block '<S3>/Data Type Conversion4' : Eliminate redundant data type conversion
 * Block '<S3>/Data Type Conversion45' : Eliminate redundant data type conversion
 * Block '<S3>/Data Type Conversion46' : Eliminate redundant data type conversion
 * Block '<S3>/Data Type Conversion47' : Eliminate redundant data type conversion
 * Block '<S3>/Data Type Conversion49' : Eliminate redundant data type conversion
 * Block '<S3>/Data Type Conversion5' : Eliminate redundant data type conversion
 * Block '<S3>/Data Type Conversion62' : Eliminate redundant data type conversion
 * Block '<S3>/Data Type Conversion63' : Eliminate redundant data type conversion
 * Block '<S5>/Saturation' : Unused code path elimination
 * Block '<S66>/ComuProtocalVertion2011' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'DCHCode'
 * '<S1>'   : 'DCHCode/SoftOFC'
 * '<S2>'   : 'DCHCode/SoftOFC/DC Charger Output Control'
 * '<S3>'   : 'DCHCode/SoftOFC/OFC Logic Control'
 * '<S4>'   : 'DCHCode/SoftOFC/SignalSelect'
 * '<S5>'   : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Current Output'
 * '<S6>'   : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Voltage output'
 * '<S7>'   : 'DCHCode/SoftOFC/DC Charger Output Control/ChargeEnergy'
 * '<S8>'   : 'DCHCode/SoftOFC/DC Charger Output Control/ChargerPlugIN'
 * '<S9>'   : 'DCHCode/SoftOFC/DC Charger Output Control/IS_Result'
 * '<S10>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Current Output/ChrgStop'
 * '<S11>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Current Output/CurrentRate'
 * '<S12>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Current Output/ChrgStop/Compare To Constant'
 * '<S13>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Current Output/ChrgStop/Compare To Constant1'
 * '<S14>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Current Output/ChrgStop/Compare To Constant2'
 * '<S15>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Current Output/CurrentRate/Compare To Constant1'
 * '<S16>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Current Output/CurrentRate/Compare To Constant2'
 * '<S17>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Current Output/CurrentRate/Compare To Constant3'
 * '<S18>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Current Output/CurrentRate/Compare To Constant4'
 * '<S19>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Current Output/CurrentRate/Compare To Constant5'
 * '<S20>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Current Output/CurrentRate/Compare To Constant7'
 * '<S21>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Voltage output/AM_Switch_IS_Ready'
 * '<S22>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Voltage output/AM_Switch_StartPreChrgFail'
 * '<S23>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Voltage output/Compare To Constant'
 * '<S24>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Voltage output/VoltageMonitoring'
 * '<S25>'  : 'DCHCode/SoftOFC/DC Charger Output Control/Charge Voltage output/VoltageMonitoring/Compare To Constant2'
 * '<S26>'  : 'DCHCode/SoftOFC/DC Charger Output Control/ChargerPlugIN/Plug Logic'
 * '<S27>'  : 'DCHCode/SoftOFC/DC Charger Output Control/IS_Result/Max_IS[ohm|V]'
 * '<S28>'  : 'DCHCode/SoftOFC/DC Charger Output Control/IS_Result/Min_IS[ohm|V]'
 * '<S29>'  : 'DCHCode/SoftOFC/OFC Logic Control/AM_Switch_CRO_EN'
 * '<S30>'  : 'DCHCode/SoftOFC/OFC Logic Control/BCL_RCVFlag'
 * '<S31>'  : 'DCHCode/SoftOFC/OFC Logic Control/BCP_RCVFlag'
 * '<S32>'  : 'DCHCode/SoftOFC/OFC Logic Control/BCS_RCVFlag'
 * '<S33>'  : 'DCHCode/SoftOFC/OFC Logic Control/Cbarger Charging Stop'
 * '<S34>'  : 'DCHCode/SoftOFC/OFC Logic Control/Charge Logic Control'
 * '<S35>'  : 'DCHCode/SoftOFC/OFC Logic Control/ChrgingTime'
 * '<S36>'  : 'DCHCode/SoftOFC/OFC Logic Control/Compare To Constant'
 * '<S37>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState'
 * '<S38>'  : 'DCHCode/SoftOFC/OFC Logic Control/Parameter'
 * '<S39>'  : 'DCHCode/SoftOFC/OFC Logic Control/Time Counter'
 * '<S40>'  : 'DCHCode/SoftOFC/OFC Logic Control/BCL_RCVFlag/Compare To Constant'
 * '<S41>'  : 'DCHCode/SoftOFC/OFC Logic Control/BCL_RCVFlag/Compare To Constant1'
 * '<S42>'  : 'DCHCode/SoftOFC/OFC Logic Control/BCP_RCVFlag/Compare To Constant'
 * '<S43>'  : 'DCHCode/SoftOFC/OFC Logic Control/BCP_RCVFlag/Compare To Constant1'
 * '<S44>'  : 'DCHCode/SoftOFC/OFC Logic Control/BCS_RCVFlag/Compare To Constant'
 * '<S45>'  : 'DCHCode/SoftOFC/OFC Logic Control/BCS_RCVFlag/Compare To Constant1'
 * '<S46>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant'
 * '<S47>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant1'
 * '<S48>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant10'
 * '<S49>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant11'
 * '<S50>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant12'
 * '<S51>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant13'
 * '<S52>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant14'
 * '<S53>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant15'
 * '<S54>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant2'
 * '<S55>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant3'
 * '<S56>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant4'
 * '<S57>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant5'
 * '<S58>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant6'
 * '<S59>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant7'
 * '<S60>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant8'
 * '<S61>'  : 'DCHCode/SoftOFC/OFC Logic Control/FaultState/Compare To Constant9'
 * '<S62>'  : 'DCHCode/SoftOFC/SignalSelect/CANMsgSelect'
 * '<S63>'  : 'DCHCode/SoftOFC/SignalSelect/OFCInfoSelect'
 * '<S64>'  : 'DCHCode/SoftOFC/SignalSelect/CANMsgSelect/CCS'
 * '<S65>'  : 'DCHCode/SoftOFC/SignalSelect/CANMsgSelect/CEM'
 * '<S66>'  : 'DCHCode/SoftOFC/SignalSelect/CANMsgSelect/CHM'
 * '<S67>'  : 'DCHCode/SoftOFC/SignalSelect/CANMsgSelect/CML'
 * '<S68>'  : 'DCHCode/SoftOFC/SignalSelect/CANMsgSelect/CRM'
 * '<S69>'  : 'DCHCode/SoftOFC/SignalSelect/CANMsgSelect/CRO'
 * '<S70>'  : 'DCHCode/SoftOFC/SignalSelect/CANMsgSelect/CSD'
 * '<S71>'  : 'DCHCode/SoftOFC/SignalSelect/CANMsgSelect/CST'
 * '<S72>'  : 'DCHCode/SoftOFC/SignalSelect/CANMsgSelect/CTS'
 */
#endif                                 /* RTW_HEADER_DCHCode_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
