/*
 * File: DCHCode_data.c
 *
 * Code generated for Simulink model 'DCHCode'.
 *
 * Model version                  : 6.7
 * Simulink Coder version         : 9.5 (R2021a) 14-Nov-2020
 * C/C++ source code generated on : Sat Feb 12 22:32:44 2022
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex-M
 * Emulation hardware selection:
 *    Differs from embedded hardware (Custom Processor->MATLAB Host Computer)
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "DCHCode.h"
#include "DCHCode_private.h"

/* Constant parameters (default storage) */
const ConstP rtConstP = {
  /* Computed Parameter: BasedOnCurrentPermittion_tableD
   * Referenced by: '<S11>/BasedOnCurrentPermittion'
   */
  { -20.0F, 0.0F, 20.0F },

  /* Pooled Parameter (Expression: [-5,0,5])
   * Referenced by:
   *   '<S11>/BasedOnCurrentPermittion'
   *   '<S11>/BasedOnVoltagePermittion'
   */
  { -5.0F, 0.0F, 5.0F },

  /* Computed Parameter: BasedOnVoltagePermittion_tableD
   * Referenced by: '<S11>/BasedOnVoltagePermittion'
   */
  { -60.0F, 0.0F, 60.0F }
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
