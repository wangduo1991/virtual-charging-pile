#include <dev_init.h>
#include <stdint.h>
#include <rtthread.h>
#include <rtdevice.h>
#include <n32g45x_rcc.h>

#define LED1_PIN    90
#define LED2_PIN    91
#define LED3_PIN    67

void LEDInit()
{
    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_AFIO, ENABLE);
    GPIO_ConfigPinRemap(GPIO_RMP_SW_JTAG_SW_ENABLE, ENABLE);
    rt_pin_mode(LED1_PIN, PIN_MODE_OUTPUT);
    rt_pin_mode(LED2_PIN, PIN_MODE_OUTPUT);
    rt_pin_mode(LED3_PIN, PIN_MODE_OUTPUT);
}

void LED_ON(int a)
{
    int Speed = 1000;
    LEDInit();
    rt_kprintf("Hello LED");
    rt_kprintf("%d",2*a);
    while(1)
    {
        rt_pin_write(LED1_PIN, PIN_LOW);
        rt_pin_write(LED2_PIN, PIN_LOW);
        rt_pin_write(LED3_PIN, PIN_LOW);
        rt_thread_mdelay(Speed);
        rt_pin_write(LED1_PIN, PIN_HIGH);
        rt_pin_write(LED2_PIN, PIN_HIGH);
        rt_pin_write(LED3_PIN, PIN_HIGH);
        rt_thread_mdelay(Speed);
    }
}

MSH_CMD_EXPORT(LED_ON,my command with a);
