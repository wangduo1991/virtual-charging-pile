/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-02-20     wd       the first version
 */
#ifndef APPLICATIONS_CANIF_H_
#define APPLICATIONS_CANIF_H_
#include "drv_can.h"

#define CAN_DEVICE_NAME "can1"
struct rt_can_msg gs_rxmsg;
//struct rt_semaphore read_sem;
rt_device_t can_dev;
//struct rt_semaphore rx_sem;
struct rt_semaphore trigger_tx_sem;
extern rt_err_t can1_rx_call(rt_device_t dev,rt_size_t size);
extern void CANInit();
extern uint8_t rFlag;

#endif /* APPLICATIONS_CANIF_H_ */
