#include "msgType.h"
#include "stdint.h"

DM3_t msgDM3;
DM2_t msgDM2;
DM1_t msgDM1;
CEM_t msgCEM;
BEM_t msgBEM;
CSD_t msgCSD;
BSD_t msgBSD;
CST_t msgCST;
BST_t msgBST;
BSP_t msgBSP;
BMT_t msgBMT;
BMV_t msgBMV;
BSM_t msgBSM;
CCS_t msgCCS;
BCS_t msgBCS;
BCL_t msgBCL;
CRO_t msgCRO;
BRO_t msgBRO;
CML_t msgCML;
CTS_t msgCTS;
BCP_t msgBCP;
BRM_t msgBRM;
CRM_t msgCRM;
BHM_t msgBHM;
CHM_t msgCHM;

uint8_t rawDM3[2];
uint8_t rawDM2[20];
uint8_t rawDM1[20];
uint8_t rawCEM[4];
uint8_t rawBEM[4];
uint8_t rawCSD[8];
uint8_t rawBSD[7];
uint8_t rawCST[4];
uint8_t rawBST[4];
uint8_t rawBSP[16];
uint8_t rawBMT[128];
uint8_t rawBMV[512];
uint8_t rawBSM[7];
uint8_t rawCCS[8];
uint8_t rawBCS[9];
uint8_t rawBCL[5];
uint8_t rawCRO[1];
uint8_t rawBRO[1];
uint8_t rawCML[8];
uint8_t rawCTS[7];
uint8_t rawBCP[13];
uint8_t rawBRM[49];
uint8_t rawCRM[8];
uint8_t rawBHM[2];
uint8_t rawCHM[3];

void DM32Raw()
{
	msgDM3.NumberOfHistoricErrorCodes =((rawDM3[1]) & 0xff);
	msgDM3.NumberOfCurrentErrorCodes =((rawDM3[0]) & 0xff);
}

void Raw2DM3()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgDM3.NumberOfHistoricErrorCodes);
		rawDM3[1] &= (~(0xff)); 
		rawDM3[1] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgDM3.NumberOfCurrentErrorCodes);
		rawDM3[0] &= (~(0xff)); 
		rawDM3[0] ^= (raw<<0);
	}

}

void DM22Raw()
{
	msgDM2.DTC5 =(((rawDM2[19]<<24) + (rawDM2[18]<<16) + (rawDM2[17]<<8) + rawDM2[16])>>0) & 0xffffffff;
	msgDM2.DTC4 =(((rawDM2[15]<<24) + (rawDM2[14]<<16) + (rawDM2[13]<<8) + rawDM2[12])>>0) & 0xffffffff;
	msgDM2.DTC3 =(((rawDM2[11]<<24) + (rawDM2[10]<<16) + (rawDM2[9]<<8) + rawDM2[8])>>0) & 0xffffffff;
	msgDM2.DTC2 =(((rawDM2[7]<<24) + (rawDM2[6]<<16) + (rawDM2[5]<<8) + rawDM2[4])>>0) & 0xffffffff;
	msgDM2.DTC1 =(((rawDM2[3]<<24) + (rawDM2[2]<<16) + (rawDM2[1]<<8) + rawDM2[0])>>0) & 0xffffffff;
}

void Raw2DM2()
{
	{
		uint32_t raw;
		raw = (uint32_t)(msgDM2.DTC5); 
		rawDM2[16] &= 0xff&(raw<<0);
		rawDM2[17] &= 0xff00&(raw<<0);
		rawDM2[18] &= 0xff0000&(raw<<0);
		rawDM2[19] &= (~0xff);
		rawDM2[19] ^= ((0xff000000&(raw<<0))>>24);
	}
	{
		uint32_t raw;
		raw = (uint32_t)(msgDM2.DTC4); 
		rawDM2[12] &= 0xff&(raw<<0);
		rawDM2[13] &= 0xff00&(raw<<0);
		rawDM2[14] &= 0xff0000&(raw<<0);
		rawDM2[15] &= (~0xff);
		rawDM2[15] ^= ((0xff000000&(raw<<0))>>24);
	}
	{
		uint32_t raw;
		raw = (uint32_t)(msgDM2.DTC3); 
		rawDM2[8] &= 0xff&(raw<<0);
		rawDM2[9] &= 0xff00&(raw<<0);
		rawDM2[10] &= 0xff0000&(raw<<0);
		rawDM2[11] &= (~0xff);
		rawDM2[11] ^= ((0xff000000&(raw<<0))>>24);
	}
	{
		uint32_t raw;
		raw = (uint32_t)(msgDM2.DTC2); 
		rawDM2[4] &= 0xff&(raw<<0);
		rawDM2[5] &= 0xff00&(raw<<0);
		rawDM2[6] &= 0xff0000&(raw<<0);
		rawDM2[7] &= (~0xff);
		rawDM2[7] ^= ((0xff000000&(raw<<0))>>24);
	}
	{
		uint32_t raw;
		raw = (uint32_t)(msgDM2.DTC1); 
		rawDM2[0] &= 0xff&(raw<<0);
		rawDM2[1] &= 0xff00&(raw<<0);
		rawDM2[2] &= 0xff0000&(raw<<0);
		rawDM2[3] &= (~0xff);
		rawDM2[3] ^= ((0xff000000&(raw<<0))>>24);
	}

}

void DM12Raw()
{
	msgDM1.DTC5 =(((rawDM1[19]<<24) + (rawDM1[18]<<16) + (rawDM1[17]<<8) + rawDM1[16])>>0) & 0xffffffff;
	msgDM1.DTC4 =(((rawDM1[15]<<24) + (rawDM1[14]<<16) + (rawDM1[13]<<8) + rawDM1[12])>>0) & 0xffffffff;
	msgDM1.DTC3 =(((rawDM1[11]<<24) + (rawDM1[10]<<16) + (rawDM1[9]<<8) + rawDM1[8])>>0) & 0xffffffff;
	msgDM1.DTC2 =(((rawDM1[7]<<24) + (rawDM1[6]<<16) + (rawDM1[5]<<8) + rawDM1[4])>>0) & 0xffffffff;
	msgDM1.DTC1 =(((rawDM1[3]<<24) + (rawDM1[2]<<16) + (rawDM1[1]<<8) + rawDM1[0])>>0) & 0xffffffff;
}

void Raw2DM1()
{
	{
		uint32_t raw;
		raw = (uint32_t)(msgDM1.DTC5); 
		rawDM1[16] &= 0xff&(raw<<0);
		rawDM1[17] &= 0xff00&(raw<<0);
		rawDM1[18] &= 0xff0000&(raw<<0);
		rawDM1[19] &= (~0xff);
		rawDM1[19] ^= ((0xff000000&(raw<<0))>>24);
	}
	{
		uint32_t raw;
		raw = (uint32_t)(msgDM1.DTC4); 
		rawDM1[12] &= 0xff&(raw<<0);
		rawDM1[13] &= 0xff00&(raw<<0);
		rawDM1[14] &= 0xff0000&(raw<<0);
		rawDM1[15] &= (~0xff);
		rawDM1[15] ^= ((0xff000000&(raw<<0))>>24);
	}
	{
		uint32_t raw;
		raw = (uint32_t)(msgDM1.DTC3); 
		rawDM1[8] &= 0xff&(raw<<0);
		rawDM1[9] &= 0xff00&(raw<<0);
		rawDM1[10] &= 0xff0000&(raw<<0);
		rawDM1[11] &= (~0xff);
		rawDM1[11] ^= ((0xff000000&(raw<<0))>>24);
	}
	{
		uint32_t raw;
		raw = (uint32_t)(msgDM1.DTC2); 
		rawDM1[4] &= 0xff&(raw<<0);
		rawDM1[5] &= 0xff00&(raw<<0);
		rawDM1[6] &= 0xff0000&(raw<<0);
		rawDM1[7] &= (~0xff);
		rawDM1[7] ^= ((0xff000000&(raw<<0))>>24);
	}
	{
		uint32_t raw;
		raw = (uint32_t)(msgDM1.DTC1); 
		rawDM1[0] &= 0xff&(raw<<0);
		rawDM1[1] &= 0xff00&(raw<<0);
		rawDM1[2] &= 0xff0000&(raw<<0);
		rawDM1[3] &= (~0xff);
		rawDM1[3] ^= ((0xff000000&(raw<<0))>>24);
	}

}

void CEM2Raw()
{
	msgCEM.Timeout_BSD =((rawCEM[3]) & 0x03);
	msgCEM.Timeout_BST =((rawCEM[2]>>4) & 0x03);
	msgCEM.Timeout_BCL =((rawCEM[2]>>2) & 0x03);
	msgCEM.Timeout_BCS =((rawCEM[2]) & 0x03);
	msgCEM.Timeout_BRO =((rawCEM[1]>>2) & 0x03);
	msgCEM.Timeout_BCP =((rawCEM[1]) & 0x03);
	msgCEM.Timeout_BRM =((rawCEM[0]) & 0x03);
}

void Raw2CEM()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCEM.Timeout_BSD);
		rawCEM[3] &= (~(0x03)); 
		rawCEM[3] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCEM.Timeout_BST);
		rawCEM[2] &= (~(0x03<<4)); 
		rawCEM[2] ^= (raw<<4);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCEM.Timeout_BCL);
		rawCEM[2] &= (~(0x03<<2)); 
		rawCEM[2] ^= (raw<<2);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCEM.Timeout_BCS);
		rawCEM[2] &= (~(0x03)); 
		rawCEM[2] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCEM.Timeout_BRO);
		rawCEM[1] &= (~(0x03<<2)); 
		rawCEM[1] ^= (raw<<2);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCEM.Timeout_BCP);
		rawCEM[1] &= (~(0x03)); 
		rawCEM[1] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCEM.Timeout_BRM);
		rawCEM[0] &= (~(0x03)); 
		rawCEM[0] ^= (raw<<0);
	}

}

void BEM2Raw()
{
	msgBEM.Timeout_CSD =((rawBEM[3]) & 0x03);
	msgBEM.Timeout_CST =((rawBEM[2]>>2) & 0x03);
	msgBEM.Timeout_CCS =((rawBEM[2]) & 0x03);
	msgBEM.Timeout_CRO =((rawBEM[1]>>2) & 0x03);
	msgBEM.Timeout_CTS_CML =((rawBEM[1]) & 0x03);
	msgBEM.Timeout_CRM_Recognized =((rawBEM[0]>>2) & 0x03);
	msgBEM.Timeout_CRM_NotRecognized =((rawBEM[0]) & 0x03);
}

void Raw2BEM()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBEM.Timeout_CSD);
		rawBEM[3] &= (~(0x03)); 
		rawBEM[3] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBEM.Timeout_CST);
		rawBEM[2] &= (~(0x03<<2)); 
		rawBEM[2] ^= (raw<<2);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBEM.Timeout_CCS);
		rawBEM[2] &= (~(0x03)); 
		rawBEM[2] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBEM.Timeout_CRO);
		rawBEM[1] &= (~(0x03<<2)); 
		rawBEM[1] ^= (raw<<2);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBEM.Timeout_CTS_CML);
		rawBEM[1] &= (~(0x03)); 
		rawBEM[1] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBEM.Timeout_CRM_Recognized);
		rawBEM[0] &= (~(0x03<<2)); 
		rawBEM[0] ^= (raw<<2);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBEM.Timeout_CRM_NotRecognized);
		rawBEM[0] &= (~(0x03)); 
		rawBEM[0] ^= (raw<<0);
	}

}

void CSD2Raw()
{
	msgCSD.ChargerNo =(((rawCSD[7]<<24) + (rawCSD[6]<<16) + (rawCSD[5]<<8) + rawCSD[4])>>0) & 0xffffffff + 1;
	msgCSD.OutputEnergy = ((((rawCSD[3]<<8) + rawCSD[2])>>0) & 0xffff) * 0.1;
	msgCSD.CumulativeChargingTime =(((rawCSD[1]<<8) + rawCSD[0])>>0) & 0xffff;
}

void Raw2CSD()
{
	{
		uint32_t raw;
		raw = (uint32_t)(msgCSD.ChargerNo-1.000000); 
		rawCSD[4] &= 0xff&(raw<<0);
		rawCSD[5] &= 0xff00&(raw<<0);
		rawCSD[6] &= 0xff0000&(raw<<0);
		rawCSD[7] &= (~0xff);
		rawCSD[7] ^= ((0xff000000&(raw<<0))>>24);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgCSD.OutputEnergy)/0.100000;
		rawCSD[2] &= 0xff&(raw<<0);
		rawCSD[3] &= (~0xff);
		rawCSD[3] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgCSD.CumulativeChargingTime);
		rawCSD[0] &= 0xff&(raw<<0);
		rawCSD[1] &= (~0xff);
		rawCSD[1] ^= ((0xff00&(raw<<0))>>8);
	}

}

void BSD2Raw()
{
	msgBSD.SingleBatteryMaxTemperature =((rawBSD[6]) & 0xff) - 50;
	msgBSD.SingleBatteryMinTemperature =((rawBSD[5]) & 0xff) - 50;
	msgBSD.SingleBatteryMaxVoltage = ((((rawBSD[4]<<8) + rawBSD[3])>>0) & 0xffff) * 0.01;
	msgBSD.SingleBatteryMinVoltage = ((((rawBSD[2]<<8) + rawBSD[1])>>0) & 0xffff) * 0.01;
	msgBSD.SuspendStateOfChargeSOC =((rawBSD[0]) & 0xff);
}

void Raw2BSD()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSD.SingleBatteryMaxTemperature+50.000000);
		rawBSD[6] &= (~(0xff)); 
		rawBSD[6] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSD.SingleBatteryMinTemperature+50.000000);
		rawBSD[5] &= (~(0xff)); 
		rawBSD[5] ^= (raw<<0);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBSD.SingleBatteryMaxVoltage)/0.010000;
		rawBSD[3] &= 0xff&(raw<<0);
		rawBSD[4] &= (~0xff);
		rawBSD[4] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBSD.SingleBatteryMinVoltage)/0.010000;
		rawBSD[1] &= 0xff&(raw<<0);
		rawBSD[2] &= (~0xff);
		rawBSD[2] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSD.SuspendStateOfChargeSOC);
		rawBSD[0] &= (~(0xff)); 
		rawBSD[0] ^= (raw<<0);
	}

}

void CST2Raw()
{
	msgCST.AbnormalVoltageError =((rawCST[3]>>2) & 0x03);
	msgCST.CurrentMismatchError =((rawCST[3]) & 0x03);
	msgCST.OtherFault =((rawCST[2]>>2) & 0x03);
	msgCST.SuddenStop =((rawCST[2]) & 0x03);
	msgCST.CannotSupplyReqElQuantity =((rawCST[1]>>6) & 0x03);
	msgCST.ChargerIntPartOverTemp =((rawCST[1]>>4) & 0x03);
	msgCST.ChargingConnectorFault =((rawCST[1]>>2) & 0x03);
	msgCST.ChargerOverTemp =((rawCST[1]) & 0x03);
	msgCST.BmsActivelySuspends =((rawCST[0]>>6) & 0x03);
	msgCST.FaultSuspension =((rawCST[0]>>4) & 0x03);
	msgCST.SuspendsArtificially =((rawCST[0]>>2) & 0x03);
	msgCST.ConditionReached =((rawCST[0]) & 0x03);
}

void Raw2CST()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCST.AbnormalVoltageError);
		rawCST[3] &= (~(0x03<<2)); 
		rawCST[3] ^= (raw<<2);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCST.CurrentMismatchError);
		rawCST[3] &= (~(0x03)); 
		rawCST[3] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCST.OtherFault);
		rawCST[2] &= (~(0x03<<2)); 
		rawCST[2] ^= (raw<<2);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCST.SuddenStop);
		rawCST[2] &= (~(0x03)); 
		rawCST[2] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCST.CannotSupplyReqElQuantity);
		rawCST[1] &= (~(0x03<<6)); 
		rawCST[1] ^= (raw<<6);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCST.ChargerIntPartOverTemp);
		rawCST[1] &= (~(0x03<<4)); 
		rawCST[1] ^= (raw<<4);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCST.ChargingConnectorFault);
		rawCST[1] &= (~(0x03<<2)); 
		rawCST[1] ^= (raw<<2);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCST.ChargerOverTemp);
		rawCST[1] &= (~(0x03)); 
		rawCST[1] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCST.BmsActivelySuspends);
		rawCST[0] &= (~(0x03<<6)); 
		rawCST[0] ^= (raw<<6);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCST.FaultSuspension);
		rawCST[0] &= (~(0x03<<4)); 
		rawCST[0] ^= (raw<<4);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCST.SuspendsArtificially);
		rawCST[0] &= (~(0x03<<2)); 
		rawCST[0] ^= (raw<<2);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCST.ConditionReached);
		rawCST[0] &= (~(0x03)); 
		rawCST[0] ^= (raw<<0);
	}

}

void BST2Raw()
{
	msgBST.AbnormalVoltageError =((rawBST[3]>>2) & 0x03);
	msgBST.ExcessiveCurrentError =((rawBST[3]) & 0x03);
	msgBST.OtherFaults =((rawBST[2]>>6) & 0x03);
	msgBST.BmsConnectorOverTemp =((rawBST[1]>>4) & 0x03);
	msgBST.VoltageDetectionFault =((rawBST[2]>>4) & 0x03);
	msgBST.HighVoltageRelayFault =((rawBST[2]>>2) & 0x03);
	msgBST.BatterySetOverTemp =((rawBST[2]) & 0x03);
	msgBST.ChargingConnectorFault =((rawBST[1]>>6) & 0x03);
	msgBST.OutputConnectorOverTemp =((rawBST[1]>>2) & 0x03);
	msgBST.InsulationFault =((rawBST[1]) & 0x03);
	msgBST.ChargerSuspendsActively =((rawBST[0]>>6) & 0x03);
	msgBST.SettingValueOfSingleVoltage =((rawBST[0]>>4) & 0x03);
	msgBST.SettingValueOfTotalVoltage =((rawBST[0]>>2) & 0x03);
	msgBST.RequiredSocTargetValue =((rawBST[0]) & 0x03);
}

void Raw2BST()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBST.AbnormalVoltageError);
		rawBST[3] &= (~(0x03<<2)); 
		rawBST[3] ^= (raw<<2);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBST.ExcessiveCurrentError);
		rawBST[3] &= (~(0x03)); 
		rawBST[3] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBST.OtherFaults);
		rawBST[2] &= (~(0x03<<6)); 
		rawBST[2] ^= (raw<<6);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBST.BmsConnectorOverTemp);
		rawBST[1] &= (~(0x03<<4)); 
		rawBST[1] ^= (raw<<4);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBST.VoltageDetectionFault);
		rawBST[2] &= (~(0x03<<4)); 
		rawBST[2] ^= (raw<<4);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBST.HighVoltageRelayFault);
		rawBST[2] &= (~(0x03<<2)); 
		rawBST[2] ^= (raw<<2);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBST.BatterySetOverTemp);
		rawBST[2] &= (~(0x03)); 
		rawBST[2] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBST.ChargingConnectorFault);
		rawBST[1] &= (~(0x03<<6)); 
		rawBST[1] ^= (raw<<6);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBST.OutputConnectorOverTemp);
		rawBST[1] &= (~(0x03<<2)); 
		rawBST[1] ^= (raw<<2);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBST.InsulationFault);
		rawBST[1] &= (~(0x03)); 
		rawBST[1] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBST.ChargerSuspendsActively);
		rawBST[0] &= (~(0x03<<6)); 
		rawBST[0] ^= (raw<<6);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBST.SettingValueOfSingleVoltage);
		rawBST[0] &= (~(0x03<<4)); 
		rawBST[0] ^= (raw<<4);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBST.SettingValueOfTotalVoltage);
		rawBST[0] &= (~(0x03<<2)); 
		rawBST[0] ^= (raw<<2);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBST.RequiredSocTargetValue);
		rawBST[0] &= (~(0x03)); 
		rawBST[0] ^= (raw<<0);
	}

}

void BSP2Raw()
{
	msgBSP.BatteryReserved16 =((rawBSP[15]) & 0xff);
	msgBSP.BatteryReserved15 =((rawBSP[14]) & 0xff);
	msgBSP.BatteryReserved14 =((rawBSP[13]) & 0xff);
	msgBSP.BatteryReserved13 =((rawBSP[12]) & 0xff);
	msgBSP.BatteryReserved12 =((rawBSP[11]) & 0xff);
	msgBSP.BatteryReserved11 =((rawBSP[10]) & 0xff);
	msgBSP.BatteryReserved10 =((rawBSP[9]) & 0xff);
	msgBSP.BatteryReserved09 =((rawBSP[8]) & 0xff);
	msgBSP.BatteryReserved08 =((rawBSP[7]) & 0xff);
	msgBSP.BatteryReserved07 =((rawBSP[6]) & 0xff);
	msgBSP.BatteryReserved06 =((rawBSP[5]) & 0xff);
	msgBSP.BatteryReserved05 =((rawBSP[4]) & 0xff);
	msgBSP.BatteryReserved04 =((rawBSP[3]) & 0xff);
	msgBSP.BatteryReserved03 =((rawBSP[2]) & 0xff);
	msgBSP.BatteryReserved02 =((rawBSP[1]) & 0xff);
	msgBSP.BatteryReserved01 =((rawBSP[0]) & 0xff);
}

void Raw2BSP()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved16);
		rawBSP[15] &= (~(0xff)); 
		rawBSP[15] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved15);
		rawBSP[14] &= (~(0xff)); 
		rawBSP[14] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved14);
		rawBSP[13] &= (~(0xff)); 
		rawBSP[13] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved13);
		rawBSP[12] &= (~(0xff)); 
		rawBSP[12] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved12);
		rawBSP[11] &= (~(0xff)); 
		rawBSP[11] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved11);
		rawBSP[10] &= (~(0xff)); 
		rawBSP[10] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved10);
		rawBSP[9] &= (~(0xff)); 
		rawBSP[9] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved09);
		rawBSP[8] &= (~(0xff)); 
		rawBSP[8] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved08);
		rawBSP[7] &= (~(0xff)); 
		rawBSP[7] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved07);
		rawBSP[6] &= (~(0xff)); 
		rawBSP[6] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved06);
		rawBSP[5] &= (~(0xff)); 
		rawBSP[5] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved05);
		rawBSP[4] &= (~(0xff)); 
		rawBSP[4] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved04);
		rawBSP[3] &= (~(0xff)); 
		rawBSP[3] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved03);
		rawBSP[2] &= (~(0xff)); 
		rawBSP[2] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved02);
		rawBSP[1] &= (~(0xff)); 
		rawBSP[1] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSP.BatteryReserved01);
		rawBSP[0] &= (~(0xff)); 
		rawBSP[0] ^= (raw<<0);
	}

}

void BMT2Raw()
{
	msgBMT.BatteryTemperature128 =((rawBMT[127]) & 0xff) - 50;
	msgBMT.BatteryTemperature127 =((rawBMT[126]) & 0xff) - 50;
	msgBMT.BatteryTemperature126 =((rawBMT[125]) & 0xff) - 50;
	msgBMT.BatteryTemperature125 =((rawBMT[124]) & 0xff) - 50;
	msgBMT.BatteryTemperature124 =((rawBMT[123]) & 0xff) - 50;
	msgBMT.BatteryTemperature123 =((rawBMT[122]) & 0xff) - 50;
	msgBMT.BatteryTemperature122 =((rawBMT[121]) & 0xff) - 50;
	msgBMT.BatteryTemperature121 =((rawBMT[120]) & 0xff) - 50;
	msgBMT.BatteryTemperature120 =((rawBMT[119]) & 0xff) - 50;
	msgBMT.BatteryTemperature119 =((rawBMT[118]) & 0xff) - 50;
	msgBMT.BatteryTemperature118 =((rawBMT[117]) & 0xff) - 50;
	msgBMT.BatteryTemperature117 =((rawBMT[116]) & 0xff) - 50;
	msgBMT.BatteryTemperature116 =((rawBMT[115]) & 0xff) - 50;
	msgBMT.BatteryTemperature115 =((rawBMT[114]) & 0xff) - 50;
	msgBMT.BatteryTemperature114 =((rawBMT[113]) & 0xff) - 50;
	msgBMT.BatteryTemperature113 =((rawBMT[112]) & 0xff) - 50;
	msgBMT.BatteryTemperature112 =((rawBMT[111]) & 0xff) - 50;
	msgBMT.BatteryTemperature111 =((rawBMT[110]) & 0xff) - 50;
	msgBMT.BatteryTemperature110 =((rawBMT[109]) & 0xff) - 50;
	msgBMT.BatteryTemperature109 =((rawBMT[108]) & 0xff) - 50;
	msgBMT.BatteryTemperature108 =((rawBMT[107]) & 0xff) - 50;
	msgBMT.BatteryTemperature107 =((rawBMT[106]) & 0xff) - 50;
	msgBMT.BatteryTemperature106 =((rawBMT[105]) & 0xff) - 50;
	msgBMT.BatteryTemperature105 =((rawBMT[104]) & 0xff) - 50;
	msgBMT.BatteryTemperature104 =((rawBMT[103]) & 0xff) - 50;
	msgBMT.BatteryTemperature103 =((rawBMT[102]) & 0xff) - 50;
	msgBMT.BatteryTemperature102 =((rawBMT[101]) & 0xff) - 50;
	msgBMT.BatteryTemperature101 =((rawBMT[100]) & 0xff) - 50;
	msgBMT.BatteryTemperature100 =((rawBMT[99]) & 0xff) - 50;
	msgBMT.BatteryTemperature099 =((rawBMT[98]) & 0xff) - 50;
	msgBMT.BatteryTemperature098 =((rawBMT[97]) & 0xff) - 50;
	msgBMT.BatteryTemperature097 =((rawBMT[96]) & 0xff) - 50;
	msgBMT.BatteryTemperature096 =((rawBMT[95]) & 0xff) - 50;
	msgBMT.BatteryTemperature095 =((rawBMT[94]) & 0xff) - 50;
	msgBMT.BatteryTemperature094 =((rawBMT[93]) & 0xff) - 50;
	msgBMT.BatteryTemperature093 =((rawBMT[92]) & 0xff) - 50;
	msgBMT.BatteryTemperature092 =((rawBMT[91]) & 0xff) - 50;
	msgBMT.BatteryTemperature091 =((rawBMT[90]) & 0xff) - 50;
	msgBMT.BatteryTemperature090 =((rawBMT[89]) & 0xff) - 50;
	msgBMT.BatteryTemperature089 =((rawBMT[88]) & 0xff) - 50;
	msgBMT.BatteryTemperature088 =((rawBMT[87]) & 0xff) - 50;
	msgBMT.BatteryTemperature087 =((rawBMT[86]) & 0xff) - 50;
	msgBMT.BatteryTemperature086 =((rawBMT[85]) & 0xff) - 50;
	msgBMT.BatteryTemperature085 =((rawBMT[84]) & 0xff) - 50;
	msgBMT.BatteryTemperature084 =((rawBMT[83]) & 0xff) - 50;
	msgBMT.BatteryTemperature083 =((rawBMT[82]) & 0xff) - 50;
	msgBMT.BatteryTemperature082 =((rawBMT[81]) & 0xff) - 50;
	msgBMT.BatteryTemperature081 =((rawBMT[80]) & 0xff) - 50;
	msgBMT.BatteryTemperature080 =((rawBMT[79]) & 0xff) - 50;
	msgBMT.BatteryTemperature079 =((rawBMT[78]) & 0xff) - 50;
	msgBMT.BatteryTemperature078 =((rawBMT[77]) & 0xff) - 50;
	msgBMT.BatteryTemperature077 =((rawBMT[76]) & 0xff) - 50;
	msgBMT.BatteryTemperature076 =((rawBMT[75]) & 0xff) - 50;
	msgBMT.BatteryTemperature075 =((rawBMT[74]) & 0xff) - 50;
	msgBMT.BatteryTemperature074 =((rawBMT[73]) & 0xff) - 50;
	msgBMT.BatteryTemperature073 =((rawBMT[72]) & 0xff) - 50;
	msgBMT.BatteryTemperature072 =((rawBMT[71]) & 0xff) - 50;
	msgBMT.BatteryTemperature071 =((rawBMT[70]) & 0xff) - 50;
	msgBMT.BatteryTemperature070 =((rawBMT[69]) & 0xff) - 50;
	msgBMT.BatteryTemperature069 =((rawBMT[68]) & 0xff) - 50;
	msgBMT.BatteryTemperature068 =((rawBMT[67]) & 0xff) - 50;
	msgBMT.BatteryTemperature067 =((rawBMT[66]) & 0xff) - 50;
	msgBMT.BatteryTemperature066 =((rawBMT[65]) & 0xff) - 50;
	msgBMT.BatteryTemperature065 =((rawBMT[64]) & 0xff) - 50;
	msgBMT.BatteryTemperature064 =((rawBMT[63]) & 0xff) - 50;
	msgBMT.BatteryTemperature063 =((rawBMT[62]) & 0xff) - 50;
	msgBMT.BatteryTemperature062 =((rawBMT[61]) & 0xff) - 50;
	msgBMT.BatteryTemperature061 =((rawBMT[60]) & 0xff) - 50;
	msgBMT.BatteryTemperature060 =((rawBMT[59]) & 0xff) - 50;
	msgBMT.BatteryTemperature059 =((rawBMT[58]) & 0xff) - 50;
	msgBMT.BatteryTemperature058 =((rawBMT[57]) & 0xff) - 50;
	msgBMT.BatteryTemperature057 =((rawBMT[56]) & 0xff) - 50;
	msgBMT.BatteryTemperature056 =((rawBMT[55]) & 0xff) - 50;
	msgBMT.BatteryTemperature055 =((rawBMT[54]) & 0xff) - 50;
	msgBMT.BatteryTemperature054 =((rawBMT[53]) & 0xff) - 50;
	msgBMT.BatteryTemperature053 =((rawBMT[52]) & 0xff) - 50;
	msgBMT.BatteryTemperature052 =((rawBMT[51]) & 0xff) - 50;
	msgBMT.BatteryTemperature051 =((rawBMT[50]) & 0xff) - 50;
	msgBMT.BatteryTemperature050 =((rawBMT[49]) & 0xff) - 50;
	msgBMT.BatteryTemperature049 =((rawBMT[48]) & 0xff) - 50;
	msgBMT.BatteryTemperature048 =((rawBMT[47]) & 0xff) - 50;
	msgBMT.BatteryTemperature047 =((rawBMT[46]) & 0xff) - 50;
	msgBMT.BatteryTemperature046 =((rawBMT[45]) & 0xff) - 50;
	msgBMT.BatteryTemperature045 =((rawBMT[44]) & 0xff) - 50;
	msgBMT.BatteryTemperature044 =((rawBMT[43]) & 0xff) - 50;
	msgBMT.BatteryTemperature043 =((rawBMT[42]) & 0xff) - 50;
	msgBMT.BatteryTemperature042 =((rawBMT[41]) & 0xff) - 50;
	msgBMT.BatteryTemperature041 =((rawBMT[40]) & 0xff) - 50;
	msgBMT.BatteryTemperature040 =((rawBMT[39]) & 0xff) - 50;
	msgBMT.BatteryTemperature039 =((rawBMT[38]) & 0xff) - 50;
	msgBMT.BatteryTemperature038 =((rawBMT[37]) & 0xff) - 50;
	msgBMT.BatteryTemperature037 =((rawBMT[36]) & 0xff) - 50;
	msgBMT.BatteryTemperature036 =((rawBMT[35]) & 0xff) - 50;
	msgBMT.BatteryTemperature035 =((rawBMT[34]) & 0xff) - 50;
	msgBMT.BatteryTemperature034 =((rawBMT[33]) & 0xff) - 50;
	msgBMT.BatteryTemperature033 =((rawBMT[32]) & 0xff) - 50;
	msgBMT.BatteryTemperature032 =((rawBMT[31]) & 0xff) - 50;
	msgBMT.BatteryTemperature031 =((rawBMT[30]) & 0xff) - 50;
	msgBMT.BatteryTemperature030 =((rawBMT[29]) & 0xff) - 50;
	msgBMT.BatteryTemperature029 =((rawBMT[28]) & 0xff) - 50;
	msgBMT.BatteryTemperature028 =((rawBMT[27]) & 0xff) - 50;
	msgBMT.BatteryTemperature027 =((rawBMT[26]) & 0xff) - 50;
	msgBMT.BatteryTemperature026 =((rawBMT[25]) & 0xff) - 50;
	msgBMT.BatteryTemperature025 =((rawBMT[24]) & 0xff) - 50;
	msgBMT.BatteryTemperature024 =((rawBMT[23]) & 0xff) - 50;
	msgBMT.BatteryTemperature023 =((rawBMT[22]) & 0xff) - 50;
	msgBMT.BatteryTemperature022 =((rawBMT[21]) & 0xff) - 50;
	msgBMT.BatteryTemperature021 =((rawBMT[20]) & 0xff) - 50;
	msgBMT.BatteryTemperature020 =((rawBMT[19]) & 0xff) - 50;
	msgBMT.BatteryTemperature019 =((rawBMT[18]) & 0xff) - 50;
	msgBMT.BatteryTemperature018 =((rawBMT[17]) & 0xff) - 50;
	msgBMT.BatteryTemperature017 =((rawBMT[16]) & 0xff) - 50;
	msgBMT.BatteryTemperature016 =((rawBMT[15]) & 0xff) - 50;
	msgBMT.BatteryTemperature015 =((rawBMT[14]) & 0xff) - 50;
	msgBMT.BatteryTemperature014 =((rawBMT[13]) & 0xff) - 50;
	msgBMT.BatteryTemperature013 =((rawBMT[12]) & 0xff) - 50;
	msgBMT.BatteryTemperature012 =((rawBMT[11]) & 0xff) - 50;
	msgBMT.BatteryTemperature011 =((rawBMT[10]) & 0xff) - 50;
	msgBMT.BatteryTemperature010 =((rawBMT[9]) & 0xff) - 50;
	msgBMT.BatteryTemperature009 =((rawBMT[8]) & 0xff) - 50;
	msgBMT.BatteryTemperature008 =((rawBMT[7]) & 0xff) - 50;
	msgBMT.BatteryTemperature007 =((rawBMT[6]) & 0xff) - 50;
	msgBMT.BatteryTemperature006 =((rawBMT[5]) & 0xff) - 50;
	msgBMT.BatteryTemperature005 =((rawBMT[4]) & 0xff) - 50;
	msgBMT.BatteryTemperature004 =((rawBMT[3]) & 0xff) - 50;
	msgBMT.BatteryTemperature003 =((rawBMT[2]) & 0xff) - 50;
	msgBMT.BatteryTemperature002 =((rawBMT[1]) & 0xff) - 50;
	msgBMT.BatteryTemperature001 =((rawBMT[0]) & 0xff) - 50;
}

void Raw2BMT()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature128+50.000000);
		rawBMT[127] &= (~(0xff)); 
		rawBMT[127] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature127+50.000000);
		rawBMT[126] &= (~(0xff)); 
		rawBMT[126] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature126+50.000000);
		rawBMT[125] &= (~(0xff)); 
		rawBMT[125] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature125+50.000000);
		rawBMT[124] &= (~(0xff)); 
		rawBMT[124] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature124+50.000000);
		rawBMT[123] &= (~(0xff)); 
		rawBMT[123] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature123+50.000000);
		rawBMT[122] &= (~(0xff)); 
		rawBMT[122] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature122+50.000000);
		rawBMT[121] &= (~(0xff)); 
		rawBMT[121] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature121+50.000000);
		rawBMT[120] &= (~(0xff)); 
		rawBMT[120] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature120+50.000000);
		rawBMT[119] &= (~(0xff)); 
		rawBMT[119] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature119+50.000000);
		rawBMT[118] &= (~(0xff)); 
		rawBMT[118] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature118+50.000000);
		rawBMT[117] &= (~(0xff)); 
		rawBMT[117] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature117+50.000000);
		rawBMT[116] &= (~(0xff)); 
		rawBMT[116] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature116+50.000000);
		rawBMT[115] &= (~(0xff)); 
		rawBMT[115] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature115+50.000000);
		rawBMT[114] &= (~(0xff)); 
		rawBMT[114] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature114+50.000000);
		rawBMT[113] &= (~(0xff)); 
		rawBMT[113] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature113+50.000000);
		rawBMT[112] &= (~(0xff)); 
		rawBMT[112] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature112+50.000000);
		rawBMT[111] &= (~(0xff)); 
		rawBMT[111] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature111+50.000000);
		rawBMT[110] &= (~(0xff)); 
		rawBMT[110] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature110+50.000000);
		rawBMT[109] &= (~(0xff)); 
		rawBMT[109] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature109+50.000000);
		rawBMT[108] &= (~(0xff)); 
		rawBMT[108] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature108+50.000000);
		rawBMT[107] &= (~(0xff)); 
		rawBMT[107] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature107+50.000000);
		rawBMT[106] &= (~(0xff)); 
		rawBMT[106] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature106+50.000000);
		rawBMT[105] &= (~(0xff)); 
		rawBMT[105] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature105+50.000000);
		rawBMT[104] &= (~(0xff)); 
		rawBMT[104] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature104+50.000000);
		rawBMT[103] &= (~(0xff)); 
		rawBMT[103] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature103+50.000000);
		rawBMT[102] &= (~(0xff)); 
		rawBMT[102] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature102+50.000000);
		rawBMT[101] &= (~(0xff)); 
		rawBMT[101] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature101+50.000000);
		rawBMT[100] &= (~(0xff)); 
		rawBMT[100] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature100+50.000000);
		rawBMT[99] &= (~(0xff)); 
		rawBMT[99] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature099+50.000000);
		rawBMT[98] &= (~(0xff)); 
		rawBMT[98] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature098+50.000000);
		rawBMT[97] &= (~(0xff)); 
		rawBMT[97] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature097+50.000000);
		rawBMT[96] &= (~(0xff)); 
		rawBMT[96] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature096+50.000000);
		rawBMT[95] &= (~(0xff)); 
		rawBMT[95] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature095+50.000000);
		rawBMT[94] &= (~(0xff)); 
		rawBMT[94] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature094+50.000000);
		rawBMT[93] &= (~(0xff)); 
		rawBMT[93] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature093+50.000000);
		rawBMT[92] &= (~(0xff)); 
		rawBMT[92] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature092+50.000000);
		rawBMT[91] &= (~(0xff)); 
		rawBMT[91] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature091+50.000000);
		rawBMT[90] &= (~(0xff)); 
		rawBMT[90] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature090+50.000000);
		rawBMT[89] &= (~(0xff)); 
		rawBMT[89] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature089+50.000000);
		rawBMT[88] &= (~(0xff)); 
		rawBMT[88] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature088+50.000000);
		rawBMT[87] &= (~(0xff)); 
		rawBMT[87] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature087+50.000000);
		rawBMT[86] &= (~(0xff)); 
		rawBMT[86] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature086+50.000000);
		rawBMT[85] &= (~(0xff)); 
		rawBMT[85] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature085+50.000000);
		rawBMT[84] &= (~(0xff)); 
		rawBMT[84] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature084+50.000000);
		rawBMT[83] &= (~(0xff)); 
		rawBMT[83] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature083+50.000000);
		rawBMT[82] &= (~(0xff)); 
		rawBMT[82] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature082+50.000000);
		rawBMT[81] &= (~(0xff)); 
		rawBMT[81] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature081+50.000000);
		rawBMT[80] &= (~(0xff)); 
		rawBMT[80] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature080+50.000000);
		rawBMT[79] &= (~(0xff)); 
		rawBMT[79] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature079+50.000000);
		rawBMT[78] &= (~(0xff)); 
		rawBMT[78] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature078+50.000000);
		rawBMT[77] &= (~(0xff)); 
		rawBMT[77] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature077+50.000000);
		rawBMT[76] &= (~(0xff)); 
		rawBMT[76] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature076+50.000000);
		rawBMT[75] &= (~(0xff)); 
		rawBMT[75] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature075+50.000000);
		rawBMT[74] &= (~(0xff)); 
		rawBMT[74] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature074+50.000000);
		rawBMT[73] &= (~(0xff)); 
		rawBMT[73] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature073+50.000000);
		rawBMT[72] &= (~(0xff)); 
		rawBMT[72] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature072+50.000000);
		rawBMT[71] &= (~(0xff)); 
		rawBMT[71] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature071+50.000000);
		rawBMT[70] &= (~(0xff)); 
		rawBMT[70] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature070+50.000000);
		rawBMT[69] &= (~(0xff)); 
		rawBMT[69] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature069+50.000000);
		rawBMT[68] &= (~(0xff)); 
		rawBMT[68] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature068+50.000000);
		rawBMT[67] &= (~(0xff)); 
		rawBMT[67] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature067+50.000000);
		rawBMT[66] &= (~(0xff)); 
		rawBMT[66] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature066+50.000000);
		rawBMT[65] &= (~(0xff)); 
		rawBMT[65] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature065+50.000000);
		rawBMT[64] &= (~(0xff)); 
		rawBMT[64] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature064+50.000000);
		rawBMT[63] &= (~(0xff)); 
		rawBMT[63] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature063+50.000000);
		rawBMT[62] &= (~(0xff)); 
		rawBMT[62] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature062+50.000000);
		rawBMT[61] &= (~(0xff)); 
		rawBMT[61] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature061+50.000000);
		rawBMT[60] &= (~(0xff)); 
		rawBMT[60] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature060+50.000000);
		rawBMT[59] &= (~(0xff)); 
		rawBMT[59] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature059+50.000000);
		rawBMT[58] &= (~(0xff)); 
		rawBMT[58] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature058+50.000000);
		rawBMT[57] &= (~(0xff)); 
		rawBMT[57] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature057+50.000000);
		rawBMT[56] &= (~(0xff)); 
		rawBMT[56] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature056+50.000000);
		rawBMT[55] &= (~(0xff)); 
		rawBMT[55] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature055+50.000000);
		rawBMT[54] &= (~(0xff)); 
		rawBMT[54] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature054+50.000000);
		rawBMT[53] &= (~(0xff)); 
		rawBMT[53] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature053+50.000000);
		rawBMT[52] &= (~(0xff)); 
		rawBMT[52] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature052+50.000000);
		rawBMT[51] &= (~(0xff)); 
		rawBMT[51] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature051+50.000000);
		rawBMT[50] &= (~(0xff)); 
		rawBMT[50] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature050+50.000000);
		rawBMT[49] &= (~(0xff)); 
		rawBMT[49] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature049+50.000000);
		rawBMT[48] &= (~(0xff)); 
		rawBMT[48] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature048+50.000000);
		rawBMT[47] &= (~(0xff)); 
		rawBMT[47] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature047+50.000000);
		rawBMT[46] &= (~(0xff)); 
		rawBMT[46] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature046+50.000000);
		rawBMT[45] &= (~(0xff)); 
		rawBMT[45] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature045+50.000000);
		rawBMT[44] &= (~(0xff)); 
		rawBMT[44] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature044+50.000000);
		rawBMT[43] &= (~(0xff)); 
		rawBMT[43] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature043+50.000000);
		rawBMT[42] &= (~(0xff)); 
		rawBMT[42] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature042+50.000000);
		rawBMT[41] &= (~(0xff)); 
		rawBMT[41] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature041+50.000000);
		rawBMT[40] &= (~(0xff)); 
		rawBMT[40] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature040+50.000000);
		rawBMT[39] &= (~(0xff)); 
		rawBMT[39] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature039+50.000000);
		rawBMT[38] &= (~(0xff)); 
		rawBMT[38] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature038+50.000000);
		rawBMT[37] &= (~(0xff)); 
		rawBMT[37] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature037+50.000000);
		rawBMT[36] &= (~(0xff)); 
		rawBMT[36] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature036+50.000000);
		rawBMT[35] &= (~(0xff)); 
		rawBMT[35] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature035+50.000000);
		rawBMT[34] &= (~(0xff)); 
		rawBMT[34] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature034+50.000000);
		rawBMT[33] &= (~(0xff)); 
		rawBMT[33] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature033+50.000000);
		rawBMT[32] &= (~(0xff)); 
		rawBMT[32] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature032+50.000000);
		rawBMT[31] &= (~(0xff)); 
		rawBMT[31] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature031+50.000000);
		rawBMT[30] &= (~(0xff)); 
		rawBMT[30] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature030+50.000000);
		rawBMT[29] &= (~(0xff)); 
		rawBMT[29] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature029+50.000000);
		rawBMT[28] &= (~(0xff)); 
		rawBMT[28] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature028+50.000000);
		rawBMT[27] &= (~(0xff)); 
		rawBMT[27] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature027+50.000000);
		rawBMT[26] &= (~(0xff)); 
		rawBMT[26] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature026+50.000000);
		rawBMT[25] &= (~(0xff)); 
		rawBMT[25] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature025+50.000000);
		rawBMT[24] &= (~(0xff)); 
		rawBMT[24] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature024+50.000000);
		rawBMT[23] &= (~(0xff)); 
		rawBMT[23] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature023+50.000000);
		rawBMT[22] &= (~(0xff)); 
		rawBMT[22] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature022+50.000000);
		rawBMT[21] &= (~(0xff)); 
		rawBMT[21] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature021+50.000000);
		rawBMT[20] &= (~(0xff)); 
		rawBMT[20] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature020+50.000000);
		rawBMT[19] &= (~(0xff)); 
		rawBMT[19] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature019+50.000000);
		rawBMT[18] &= (~(0xff)); 
		rawBMT[18] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature018+50.000000);
		rawBMT[17] &= (~(0xff)); 
		rawBMT[17] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature017+50.000000);
		rawBMT[16] &= (~(0xff)); 
		rawBMT[16] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature016+50.000000);
		rawBMT[15] &= (~(0xff)); 
		rawBMT[15] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature015+50.000000);
		rawBMT[14] &= (~(0xff)); 
		rawBMT[14] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature014+50.000000);
		rawBMT[13] &= (~(0xff)); 
		rawBMT[13] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature013+50.000000);
		rawBMT[12] &= (~(0xff)); 
		rawBMT[12] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature012+50.000000);
		rawBMT[11] &= (~(0xff)); 
		rawBMT[11] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature011+50.000000);
		rawBMT[10] &= (~(0xff)); 
		rawBMT[10] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature010+50.000000);
		rawBMT[9] &= (~(0xff)); 
		rawBMT[9] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature009+50.000000);
		rawBMT[8] &= (~(0xff)); 
		rawBMT[8] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature008+50.000000);
		rawBMT[7] &= (~(0xff)); 
		rawBMT[7] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature007+50.000000);
		rawBMT[6] &= (~(0xff)); 
		rawBMT[6] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature006+50.000000);
		rawBMT[5] &= (~(0xff)); 
		rawBMT[5] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature005+50.000000);
		rawBMT[4] &= (~(0xff)); 
		rawBMT[4] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature004+50.000000);
		rawBMT[3] &= (~(0xff)); 
		rawBMT[3] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature003+50.000000);
		rawBMT[2] &= (~(0xff)); 
		rawBMT[2] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature002+50.000000);
		rawBMT[1] &= (~(0xff)); 
		rawBMT[1] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMT.BatteryTemperature001+50.000000);
		rawBMT[0] &= (~(0xff)); 
		rawBMT[0] ^= (raw<<0);
	}

}

void BMV2Raw()
{
	msgBMV.SingleBatteryGroup256 =((rawBMV[511]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage256 = ((((rawBMV[511]<<8) + rawBMV[510])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup255 =((rawBMV[509]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage255 = ((((rawBMV[509]<<8) + rawBMV[508])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup254 =((rawBMV[507]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage254 = ((((rawBMV[507]<<8) + rawBMV[506])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup253 =((rawBMV[505]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage253 = ((((rawBMV[505]<<8) + rawBMV[504])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup252 =((rawBMV[503]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage252 = ((((rawBMV[503]<<8) + rawBMV[502])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup251 =((rawBMV[501]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage251 = ((((rawBMV[501]<<8) + rawBMV[500])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup250 =((rawBMV[499]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage250 = ((((rawBMV[499]<<8) + rawBMV[498])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup249 =((rawBMV[497]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage249 = ((((rawBMV[497]<<8) + rawBMV[496])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup248 =((rawBMV[495]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage248 = ((((rawBMV[495]<<8) + rawBMV[494])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup247 =((rawBMV[493]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage247 = ((((rawBMV[493]<<8) + rawBMV[492])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup246 =((rawBMV[491]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage246 = ((((rawBMV[491]<<8) + rawBMV[490])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup245 =((rawBMV[489]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage245 = ((((rawBMV[489]<<8) + rawBMV[488])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup244 =((rawBMV[487]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage244 = ((((rawBMV[487]<<8) + rawBMV[486])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup243 =((rawBMV[485]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage243 = ((((rawBMV[485]<<8) + rawBMV[484])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup242 =((rawBMV[483]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage242 = ((((rawBMV[483]<<8) + rawBMV[482])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup241 =((rawBMV[481]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage241 = ((((rawBMV[481]<<8) + rawBMV[480])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup240 =((rawBMV[479]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage240 = ((((rawBMV[479]<<8) + rawBMV[478])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup239 =((rawBMV[477]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage239 = ((((rawBMV[477]<<8) + rawBMV[476])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup238 =((rawBMV[475]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage238 = ((((rawBMV[475]<<8) + rawBMV[474])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup237 =((rawBMV[473]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage237 = ((((rawBMV[473]<<8) + rawBMV[472])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup236 =((rawBMV[471]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage236 = ((((rawBMV[471]<<8) + rawBMV[470])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup235 =((rawBMV[469]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage235 = ((((rawBMV[469]<<8) + rawBMV[468])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup234 =((rawBMV[467]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage234 = ((((rawBMV[467]<<8) + rawBMV[466])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup233 =((rawBMV[465]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage233 = ((((rawBMV[465]<<8) + rawBMV[464])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup232 =((rawBMV[463]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage232 = ((((rawBMV[463]<<8) + rawBMV[462])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup231 =((rawBMV[461]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage231 = ((((rawBMV[461]<<8) + rawBMV[460])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup230 =((rawBMV[459]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage230 = ((((rawBMV[459]<<8) + rawBMV[458])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup229 =((rawBMV[457]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage229 = ((((rawBMV[457]<<8) + rawBMV[456])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup228 =((rawBMV[455]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage228 = ((((rawBMV[455]<<8) + rawBMV[454])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup227 =((rawBMV[453]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage227 = ((((rawBMV[453]<<8) + rawBMV[452])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup226 =((rawBMV[451]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage226 = ((((rawBMV[451]<<8) + rawBMV[450])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup225 =((rawBMV[449]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage225 = ((((rawBMV[449]<<8) + rawBMV[448])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup224 =((rawBMV[447]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage224 = ((((rawBMV[447]<<8) + rawBMV[446])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup223 =((rawBMV[445]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage223 = ((((rawBMV[445]<<8) + rawBMV[444])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup222 =((rawBMV[443]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage222 = ((((rawBMV[443]<<8) + rawBMV[442])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup221 =((rawBMV[441]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage221 = ((((rawBMV[441]<<8) + rawBMV[440])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup220 =((rawBMV[439]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage220 = ((((rawBMV[439]<<8) + rawBMV[438])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup219 =((rawBMV[437]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage219 = ((((rawBMV[437]<<8) + rawBMV[436])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup218 =((rawBMV[435]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage218 = ((((rawBMV[435]<<8) + rawBMV[434])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup217 =((rawBMV[433]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage217 = ((((rawBMV[433]<<8) + rawBMV[432])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup216 =((rawBMV[431]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage216 = ((((rawBMV[431]<<8) + rawBMV[430])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup215 =((rawBMV[429]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage215 = ((((rawBMV[429]<<8) + rawBMV[428])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup214 =((rawBMV[427]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage214 = ((((rawBMV[427]<<8) + rawBMV[426])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup213 =((rawBMV[425]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage213 = ((((rawBMV[425]<<8) + rawBMV[424])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup212 =((rawBMV[423]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage212 = ((((rawBMV[423]<<8) + rawBMV[422])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup211 =((rawBMV[421]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage211 = ((((rawBMV[421]<<8) + rawBMV[420])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup210 =((rawBMV[419]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage210 = ((((rawBMV[419]<<8) + rawBMV[418])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup209 =((rawBMV[417]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage209 = ((((rawBMV[417]<<8) + rawBMV[416])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup208 =((rawBMV[415]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage208 = ((((rawBMV[415]<<8) + rawBMV[414])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup207 =((rawBMV[413]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage207 = ((((rawBMV[413]<<8) + rawBMV[412])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup206 =((rawBMV[411]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage206 = ((((rawBMV[411]<<8) + rawBMV[410])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup205 =((rawBMV[409]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage205 = ((((rawBMV[409]<<8) + rawBMV[408])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup204 =((rawBMV[407]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage204 = ((((rawBMV[407]<<8) + rawBMV[406])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup203 =((rawBMV[405]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage203 = ((((rawBMV[405]<<8) + rawBMV[404])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup202 =((rawBMV[403]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage202 = ((((rawBMV[403]<<8) + rawBMV[402])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup201 =((rawBMV[401]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage201 = ((((rawBMV[401]<<8) + rawBMV[400])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup200 =((rawBMV[399]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage200 = ((((rawBMV[399]<<8) + rawBMV[398])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup199 =((rawBMV[397]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage199 = ((((rawBMV[397]<<8) + rawBMV[396])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup198 =((rawBMV[395]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage198 = ((((rawBMV[395]<<8) + rawBMV[394])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup197 =((rawBMV[393]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage197 = ((((rawBMV[393]<<8) + rawBMV[392])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup196 =((rawBMV[391]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage196 = ((((rawBMV[391]<<8) + rawBMV[390])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup195 =((rawBMV[389]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage195 = ((((rawBMV[389]<<8) + rawBMV[388])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup194 =((rawBMV[387]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage194 = ((((rawBMV[387]<<8) + rawBMV[386])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup193 =((rawBMV[385]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage193 = ((((rawBMV[385]<<8) + rawBMV[384])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup192 =((rawBMV[383]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage192 = ((((rawBMV[383]<<8) + rawBMV[382])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup191 =((rawBMV[381]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage191 = ((((rawBMV[381]<<8) + rawBMV[380])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup190 =((rawBMV[379]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage190 = ((((rawBMV[379]<<8) + rawBMV[378])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup189 =((rawBMV[377]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage189 = ((((rawBMV[377]<<8) + rawBMV[376])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup188 =((rawBMV[375]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage188 = ((((rawBMV[375]<<8) + rawBMV[374])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup187 =((rawBMV[373]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage187 = ((((rawBMV[373]<<8) + rawBMV[372])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup186 =((rawBMV[371]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage186 = ((((rawBMV[371]<<8) + rawBMV[370])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup185 =((rawBMV[369]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage185 = ((((rawBMV[369]<<8) + rawBMV[368])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup184 =((rawBMV[367]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage184 = ((((rawBMV[367]<<8) + rawBMV[366])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup183 =((rawBMV[365]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage183 = ((((rawBMV[365]<<8) + rawBMV[364])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup182 =((rawBMV[363]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage182 = ((((rawBMV[363]<<8) + rawBMV[362])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup181 =((rawBMV[361]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage181 = ((((rawBMV[361]<<8) + rawBMV[360])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup180 =((rawBMV[359]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage180 = ((((rawBMV[359]<<8) + rawBMV[358])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup179 =((rawBMV[357]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage179 = ((((rawBMV[357]<<8) + rawBMV[356])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup178 =((rawBMV[355]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage178 = ((((rawBMV[355]<<8) + rawBMV[354])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup177 =((rawBMV[353]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage177 = ((((rawBMV[353]<<8) + rawBMV[352])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup176 =((rawBMV[351]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage176 = ((((rawBMV[351]<<8) + rawBMV[350])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup175 =((rawBMV[349]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage175 = ((((rawBMV[349]<<8) + rawBMV[348])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup174 =((rawBMV[347]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage174 = ((((rawBMV[347]<<8) + rawBMV[346])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup173 =((rawBMV[345]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage173 = ((((rawBMV[345]<<8) + rawBMV[344])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup172 =((rawBMV[343]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage172 = ((((rawBMV[343]<<8) + rawBMV[342])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup171 =((rawBMV[341]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage171 = ((((rawBMV[341]<<8) + rawBMV[340])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup170 =((rawBMV[339]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage170 = ((((rawBMV[339]<<8) + rawBMV[338])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup169 =((rawBMV[337]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage169 = ((((rawBMV[337]<<8) + rawBMV[336])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup168 =((rawBMV[335]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage168 = ((((rawBMV[335]<<8) + rawBMV[334])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup167 =((rawBMV[333]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage167 = ((((rawBMV[333]<<8) + rawBMV[332])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup166 =((rawBMV[331]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage166 = ((((rawBMV[331]<<8) + rawBMV[330])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup165 =((rawBMV[329]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage165 = ((((rawBMV[329]<<8) + rawBMV[328])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup164 =((rawBMV[327]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage164 = ((((rawBMV[327]<<8) + rawBMV[326])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup163 =((rawBMV[325]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage163 = ((((rawBMV[325]<<8) + rawBMV[324])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup162 =((rawBMV[323]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage162 = ((((rawBMV[323]<<8) + rawBMV[322])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup161 =((rawBMV[321]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage161 = ((((rawBMV[321]<<8) + rawBMV[320])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup160 =((rawBMV[319]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage160 = ((((rawBMV[319]<<8) + rawBMV[318])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup159 =((rawBMV[317]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage159 = ((((rawBMV[317]<<8) + rawBMV[316])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup158 =((rawBMV[315]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage158 = ((((rawBMV[315]<<8) + rawBMV[314])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup157 =((rawBMV[313]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage157 = ((((rawBMV[313]<<8) + rawBMV[312])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup156 =((rawBMV[311]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage156 = ((((rawBMV[311]<<8) + rawBMV[310])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup155 =((rawBMV[309]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage155 = ((((rawBMV[309]<<8) + rawBMV[308])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup154 =((rawBMV[307]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage154 = ((((rawBMV[307]<<8) + rawBMV[306])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup153 =((rawBMV[305]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage153 = ((((rawBMV[305]<<8) + rawBMV[304])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup152 =((rawBMV[303]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage152 = ((((rawBMV[303]<<8) + rawBMV[302])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup151 =((rawBMV[301]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage151 = ((((rawBMV[301]<<8) + rawBMV[300])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup150 =((rawBMV[299]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage150 = ((((rawBMV[299]<<8) + rawBMV[298])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup149 =((rawBMV[297]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage149 = ((((rawBMV[297]<<8) + rawBMV[296])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup148 =((rawBMV[295]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage148 = ((((rawBMV[295]<<8) + rawBMV[294])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup147 =((rawBMV[293]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage147 = ((((rawBMV[293]<<8) + rawBMV[292])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup146 =((rawBMV[291]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage146 = ((((rawBMV[291]<<8) + rawBMV[290])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup145 =((rawBMV[289]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage145 = ((((rawBMV[289]<<8) + rawBMV[288])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup144 =((rawBMV[287]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage144 = ((((rawBMV[287]<<8) + rawBMV[286])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup143 =((rawBMV[285]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage143 = ((((rawBMV[285]<<8) + rawBMV[284])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup142 =((rawBMV[283]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage142 = ((((rawBMV[283]<<8) + rawBMV[282])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup141 =((rawBMV[281]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage141 = ((((rawBMV[281]<<8) + rawBMV[280])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup140 =((rawBMV[279]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage140 = ((((rawBMV[279]<<8) + rawBMV[278])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup139 =((rawBMV[277]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage139 = ((((rawBMV[277]<<8) + rawBMV[276])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup138 =((rawBMV[275]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage138 = ((((rawBMV[275]<<8) + rawBMV[274])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup137 =((rawBMV[273]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage137 = ((((rawBMV[273]<<8) + rawBMV[272])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup136 =((rawBMV[271]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage136 = ((((rawBMV[271]<<8) + rawBMV[270])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup135 =((rawBMV[269]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage135 = ((((rawBMV[269]<<8) + rawBMV[268])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup134 =((rawBMV[267]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage134 = ((((rawBMV[267]<<8) + rawBMV[266])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup133 =((rawBMV[265]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage133 = ((((rawBMV[265]<<8) + rawBMV[264])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup132 =((rawBMV[263]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage132 = ((((rawBMV[263]<<8) + rawBMV[262])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup131 =((rawBMV[261]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage131 = ((((rawBMV[261]<<8) + rawBMV[260])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup130 =((rawBMV[259]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage130 = ((((rawBMV[259]<<8) + rawBMV[258])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup129 =((rawBMV[257]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage129 = ((((rawBMV[257]<<8) + rawBMV[256])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup128 =((rawBMV[255]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage128 = ((((rawBMV[255]<<8) + rawBMV[254])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup127 =((rawBMV[253]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage127 = ((((rawBMV[253]<<8) + rawBMV[252])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup126 =((rawBMV[251]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage126 = ((((rawBMV[251]<<8) + rawBMV[250])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup125 =((rawBMV[249]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage125 = ((((rawBMV[249]<<8) + rawBMV[248])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup124 =((rawBMV[247]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage124 = ((((rawBMV[247]<<8) + rawBMV[246])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup123 =((rawBMV[245]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage123 = ((((rawBMV[245]<<8) + rawBMV[244])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup122 =((rawBMV[243]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage122 = ((((rawBMV[243]<<8) + rawBMV[242])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup121 =((rawBMV[241]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage121 = ((((rawBMV[241]<<8) + rawBMV[240])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup120 =((rawBMV[239]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage120 = ((((rawBMV[239]<<8) + rawBMV[238])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup119 =((rawBMV[237]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage119 = ((((rawBMV[237]<<8) + rawBMV[236])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup118 =((rawBMV[235]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage118 = ((((rawBMV[235]<<8) + rawBMV[234])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup117 =((rawBMV[233]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage117 = ((((rawBMV[233]<<8) + rawBMV[232])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup116 =((rawBMV[231]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage116 = ((((rawBMV[231]<<8) + rawBMV[230])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup115 =((rawBMV[229]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage115 = ((((rawBMV[229]<<8) + rawBMV[228])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup114 =((rawBMV[227]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage114 = ((((rawBMV[227]<<8) + rawBMV[226])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup113 =((rawBMV[225]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage113 = ((((rawBMV[225]<<8) + rawBMV[224])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup112 =((rawBMV[223]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage112 = ((((rawBMV[223]<<8) + rawBMV[222])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup111 =((rawBMV[221]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage111 = ((((rawBMV[221]<<8) + rawBMV[220])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup110 =((rawBMV[219]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage110 = ((((rawBMV[219]<<8) + rawBMV[218])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup109 =((rawBMV[217]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage109 = ((((rawBMV[217]<<8) + rawBMV[216])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup108 =((rawBMV[215]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage108 = ((((rawBMV[215]<<8) + rawBMV[214])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup107 =((rawBMV[213]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage107 = ((((rawBMV[213]<<8) + rawBMV[212])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup106 =((rawBMV[211]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage106 = ((((rawBMV[211]<<8) + rawBMV[210])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup105 =((rawBMV[209]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage105 = ((((rawBMV[209]<<8) + rawBMV[208])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup104 =((rawBMV[207]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage104 = ((((rawBMV[207]<<8) + rawBMV[206])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup103 =((rawBMV[205]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage103 = ((((rawBMV[205]<<8) + rawBMV[204])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup102 =((rawBMV[203]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage102 = ((((rawBMV[203]<<8) + rawBMV[202])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup101 =((rawBMV[201]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage101 = ((((rawBMV[201]<<8) + rawBMV[200])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup100 =((rawBMV[199]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage100 = ((((rawBMV[199]<<8) + rawBMV[198])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup099 =((rawBMV[197]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage099 = ((((rawBMV[197]<<8) + rawBMV[196])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup098 =((rawBMV[195]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage098 = ((((rawBMV[195]<<8) + rawBMV[194])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup097 =((rawBMV[193]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage097 = ((((rawBMV[193]<<8) + rawBMV[192])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup096 =((rawBMV[191]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage096 = ((((rawBMV[191]<<8) + rawBMV[190])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup095 =((rawBMV[189]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage095 = ((((rawBMV[189]<<8) + rawBMV[188])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup094 =((rawBMV[187]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage094 = ((((rawBMV[187]<<8) + rawBMV[186])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup093 =((rawBMV[185]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage093 = ((((rawBMV[185]<<8) + rawBMV[184])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup092 =((rawBMV[183]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage092 = ((((rawBMV[183]<<8) + rawBMV[182])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup091 =((rawBMV[181]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage091 = ((((rawBMV[181]<<8) + rawBMV[180])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup090 =((rawBMV[179]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage090 = ((((rawBMV[179]<<8) + rawBMV[178])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup089 =((rawBMV[177]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage089 = ((((rawBMV[177]<<8) + rawBMV[176])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup088 =((rawBMV[175]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage088 = ((((rawBMV[175]<<8) + rawBMV[174])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup087 =((rawBMV[173]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage087 = ((((rawBMV[173]<<8) + rawBMV[172])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup086 =((rawBMV[171]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage086 = ((((rawBMV[171]<<8) + rawBMV[170])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup085 =((rawBMV[169]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage085 = ((((rawBMV[169]<<8) + rawBMV[168])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup084 =((rawBMV[167]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage084 = ((((rawBMV[167]<<8) + rawBMV[166])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup083 =((rawBMV[165]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage083 = ((((rawBMV[165]<<8) + rawBMV[164])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup082 =((rawBMV[163]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage082 = ((((rawBMV[163]<<8) + rawBMV[162])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup081 =((rawBMV[161]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage081 = ((((rawBMV[161]<<8) + rawBMV[160])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup080 =((rawBMV[159]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage080 = ((((rawBMV[159]<<8) + rawBMV[158])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup079 =((rawBMV[157]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage079 = ((((rawBMV[157]<<8) + rawBMV[156])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup078 =((rawBMV[155]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage078 = ((((rawBMV[155]<<8) + rawBMV[154])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup077 =((rawBMV[153]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage077 = ((((rawBMV[153]<<8) + rawBMV[152])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup076 =((rawBMV[151]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage076 = ((((rawBMV[151]<<8) + rawBMV[150])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup075 =((rawBMV[149]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage075 = ((((rawBMV[149]<<8) + rawBMV[148])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup074 =((rawBMV[147]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage074 = ((((rawBMV[147]<<8) + rawBMV[146])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup073 =((rawBMV[145]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage073 = ((((rawBMV[145]<<8) + rawBMV[144])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup072 =((rawBMV[143]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage072 = ((((rawBMV[143]<<8) + rawBMV[142])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup071 =((rawBMV[141]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage071 = ((((rawBMV[141]<<8) + rawBMV[140])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup070 =((rawBMV[139]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage070 = ((((rawBMV[139]<<8) + rawBMV[138])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup069 =((rawBMV[137]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage069 = ((((rawBMV[137]<<8) + rawBMV[136])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup068 =((rawBMV[135]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage068 = ((((rawBMV[135]<<8) + rawBMV[134])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup067 =((rawBMV[133]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage067 = ((((rawBMV[133]<<8) + rawBMV[132])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup066 =((rawBMV[131]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage066 = ((((rawBMV[131]<<8) + rawBMV[130])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup065 =((rawBMV[129]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage065 = ((((rawBMV[129]<<8) + rawBMV[128])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup064 =((rawBMV[127]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage064 = ((((rawBMV[127]<<8) + rawBMV[126])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup063 =((rawBMV[125]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage063 = ((((rawBMV[125]<<8) + rawBMV[124])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup062 =((rawBMV[123]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage062 = ((((rawBMV[123]<<8) + rawBMV[122])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup061 =((rawBMV[121]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage061 = ((((rawBMV[121]<<8) + rawBMV[120])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup060 =((rawBMV[119]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage060 = ((((rawBMV[119]<<8) + rawBMV[118])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup059 =((rawBMV[117]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage059 = ((((rawBMV[117]<<8) + rawBMV[116])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup058 =((rawBMV[115]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage058 = ((((rawBMV[115]<<8) + rawBMV[114])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup057 =((rawBMV[113]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage057 = ((((rawBMV[113]<<8) + rawBMV[112])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup056 =((rawBMV[111]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage056 = ((((rawBMV[111]<<8) + rawBMV[110])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup055 =((rawBMV[109]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage055 = ((((rawBMV[109]<<8) + rawBMV[108])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup054 =((rawBMV[107]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage054 = ((((rawBMV[107]<<8) + rawBMV[106])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup053 =((rawBMV[105]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage053 = ((((rawBMV[105]<<8) + rawBMV[104])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup052 =((rawBMV[103]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage052 = ((((rawBMV[103]<<8) + rawBMV[102])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup051 =((rawBMV[101]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage051 = ((((rawBMV[101]<<8) + rawBMV[100])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup050 =((rawBMV[99]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage050 = ((((rawBMV[99]<<8) + rawBMV[98])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup049 =((rawBMV[97]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage049 = ((((rawBMV[97]<<8) + rawBMV[96])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup048 =((rawBMV[95]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage048 = ((((rawBMV[95]<<8) + rawBMV[94])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup047 =((rawBMV[93]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage047 = ((((rawBMV[93]<<8) + rawBMV[92])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup046 =((rawBMV[91]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage046 = ((((rawBMV[91]<<8) + rawBMV[90])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup045 =((rawBMV[89]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage045 = ((((rawBMV[89]<<8) + rawBMV[88])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup044 =((rawBMV[87]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage044 = ((((rawBMV[87]<<8) + rawBMV[86])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup043 =((rawBMV[85]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage043 = ((((rawBMV[85]<<8) + rawBMV[84])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup042 =((rawBMV[83]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage042 = ((((rawBMV[83]<<8) + rawBMV[82])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup041 =((rawBMV[81]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage041 = ((((rawBMV[81]<<8) + rawBMV[80])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup040 =((rawBMV[79]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage040 = ((((rawBMV[79]<<8) + rawBMV[78])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup039 =((rawBMV[77]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage039 = ((((rawBMV[77]<<8) + rawBMV[76])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup038 =((rawBMV[75]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage038 = ((((rawBMV[75]<<8) + rawBMV[74])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup037 =((rawBMV[73]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage037 = ((((rawBMV[73]<<8) + rawBMV[72])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup036 =((rawBMV[71]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage036 = ((((rawBMV[71]<<8) + rawBMV[70])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup035 =((rawBMV[69]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage035 = ((((rawBMV[69]<<8) + rawBMV[68])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup034 =((rawBMV[67]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage034 = ((((rawBMV[67]<<8) + rawBMV[66])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup033 =((rawBMV[65]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage033 = ((((rawBMV[65]<<8) + rawBMV[64])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup032 =((rawBMV[63]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage032 = ((((rawBMV[63]<<8) + rawBMV[62])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup031 =((rawBMV[61]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage031 = ((((rawBMV[61]<<8) + rawBMV[60])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup030 =((rawBMV[59]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage030 = ((((rawBMV[59]<<8) + rawBMV[58])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup029 =((rawBMV[57]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage029 = ((((rawBMV[57]<<8) + rawBMV[56])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup028 =((rawBMV[55]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage028 = ((((rawBMV[55]<<8) + rawBMV[54])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup027 =((rawBMV[53]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage027 = ((((rawBMV[53]<<8) + rawBMV[52])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup026 =((rawBMV[51]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage026 = ((((rawBMV[51]<<8) + rawBMV[50])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup025 =((rawBMV[49]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage025 = ((((rawBMV[49]<<8) + rawBMV[48])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup024 =((rawBMV[47]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage024 = ((((rawBMV[47]<<8) + rawBMV[46])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup023 =((rawBMV[45]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage023 = ((((rawBMV[45]<<8) + rawBMV[44])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup022 =((rawBMV[43]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage022 = ((((rawBMV[43]<<8) + rawBMV[42])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup021 =((rawBMV[41]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage021 = ((((rawBMV[41]<<8) + rawBMV[40])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup020 =((rawBMV[39]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage020 = ((((rawBMV[39]<<8) + rawBMV[38])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup019 =((rawBMV[37]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage019 = ((((rawBMV[37]<<8) + rawBMV[36])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup018 =((rawBMV[35]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage018 = ((((rawBMV[35]<<8) + rawBMV[34])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup017 =((rawBMV[33]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage017 = ((((rawBMV[33]<<8) + rawBMV[32])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup016 =((rawBMV[31]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage016 = ((((rawBMV[31]<<8) + rawBMV[30])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup015 =((rawBMV[29]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage015 = ((((rawBMV[29]<<8) + rawBMV[28])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup014 =((rawBMV[27]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage014 = ((((rawBMV[27]<<8) + rawBMV[26])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup013 =((rawBMV[25]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage013 = ((((rawBMV[25]<<8) + rawBMV[24])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup012 =((rawBMV[23]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage012 = ((((rawBMV[23]<<8) + rawBMV[22])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup011 =((rawBMV[21]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage011 = ((((rawBMV[21]<<8) + rawBMV[20])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup010 =((rawBMV[19]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage010 = ((((rawBMV[19]<<8) + rawBMV[18])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup009 =((rawBMV[17]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage009 = ((((rawBMV[17]<<8) + rawBMV[16])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup008 =((rawBMV[15]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage008 = ((((rawBMV[15]<<8) + rawBMV[14])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup007 =((rawBMV[13]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage007 = ((((rawBMV[13]<<8) + rawBMV[12])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup006 =((rawBMV[11]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage006 = ((((rawBMV[11]<<8) + rawBMV[10])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup005 =((rawBMV[9]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage005 = ((((rawBMV[9]<<8) + rawBMV[8])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup004 =((rawBMV[7]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage004 = ((((rawBMV[7]<<8) + rawBMV[6])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup003 =((rawBMV[5]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage003 = ((((rawBMV[5]<<8) + rawBMV[4])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup002 =((rawBMV[3]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage002 = ((((rawBMV[3]<<8) + rawBMV[2])>>0) & 0xfff) * 0.01;
	msgBMV.SingleBatteryGroup001 =((rawBMV[1]>>4) & 0x0f);
	msgBMV.SingleBatteryVoltage001 = ((((rawBMV[1]<<8) + rawBMV[0])>>0) & 0xfff) * 0.01;
}

void Raw2BMV()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup256);
		rawBMV[511] &= (~(0x0f<<4)); 
		rawBMV[511] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage256)/0.010000;
		rawBMV[510] &= 0xff&(raw<<0);
		rawBMV[511] &= (~0x0f);
		rawBMV[511] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup255);
		rawBMV[509] &= (~(0x0f<<4)); 
		rawBMV[509] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage255)/0.010000;
		rawBMV[508] &= 0xff&(raw<<0);
		rawBMV[509] &= (~0x0f);
		rawBMV[509] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup254);
		rawBMV[507] &= (~(0x0f<<4)); 
		rawBMV[507] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage254)/0.010000;
		rawBMV[506] &= 0xff&(raw<<0);
		rawBMV[507] &= (~0x0f);
		rawBMV[507] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup253);
		rawBMV[505] &= (~(0x0f<<4)); 
		rawBMV[505] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage253)/0.010000;
		rawBMV[504] &= 0xff&(raw<<0);
		rawBMV[505] &= (~0x0f);
		rawBMV[505] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup252);
		rawBMV[503] &= (~(0x0f<<4)); 
		rawBMV[503] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage252)/0.010000;
		rawBMV[502] &= 0xff&(raw<<0);
		rawBMV[503] &= (~0x0f);
		rawBMV[503] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup251);
		rawBMV[501] &= (~(0x0f<<4)); 
		rawBMV[501] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage251)/0.010000;
		rawBMV[500] &= 0xff&(raw<<0);
		rawBMV[501] &= (~0x0f);
		rawBMV[501] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup250);
		rawBMV[499] &= (~(0x0f<<4)); 
		rawBMV[499] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage250)/0.010000;
		rawBMV[498] &= 0xff&(raw<<0);
		rawBMV[499] &= (~0x0f);
		rawBMV[499] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup249);
		rawBMV[497] &= (~(0x0f<<4)); 
		rawBMV[497] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage249)/0.010000;
		rawBMV[496] &= 0xff&(raw<<0);
		rawBMV[497] &= (~0x0f);
		rawBMV[497] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup248);
		rawBMV[495] &= (~(0x0f<<4)); 
		rawBMV[495] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage248)/0.010000;
		rawBMV[494] &= 0xff&(raw<<0);
		rawBMV[495] &= (~0x0f);
		rawBMV[495] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup247);
		rawBMV[493] &= (~(0x0f<<4)); 
		rawBMV[493] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage247)/0.010000;
		rawBMV[492] &= 0xff&(raw<<0);
		rawBMV[493] &= (~0x0f);
		rawBMV[493] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup246);
		rawBMV[491] &= (~(0x0f<<4)); 
		rawBMV[491] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage246)/0.010000;
		rawBMV[490] &= 0xff&(raw<<0);
		rawBMV[491] &= (~0x0f);
		rawBMV[491] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup245);
		rawBMV[489] &= (~(0x0f<<4)); 
		rawBMV[489] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage245)/0.010000;
		rawBMV[488] &= 0xff&(raw<<0);
		rawBMV[489] &= (~0x0f);
		rawBMV[489] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup244);
		rawBMV[487] &= (~(0x0f<<4)); 
		rawBMV[487] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage244)/0.010000;
		rawBMV[486] &= 0xff&(raw<<0);
		rawBMV[487] &= (~0x0f);
		rawBMV[487] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup243);
		rawBMV[485] &= (~(0x0f<<4)); 
		rawBMV[485] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage243)/0.010000;
		rawBMV[484] &= 0xff&(raw<<0);
		rawBMV[485] &= (~0x0f);
		rawBMV[485] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup242);
		rawBMV[483] &= (~(0x0f<<4)); 
		rawBMV[483] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage242)/0.010000;
		rawBMV[482] &= 0xff&(raw<<0);
		rawBMV[483] &= (~0x0f);
		rawBMV[483] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup241);
		rawBMV[481] &= (~(0x0f<<4)); 
		rawBMV[481] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage241)/0.010000;
		rawBMV[480] &= 0xff&(raw<<0);
		rawBMV[481] &= (~0x0f);
		rawBMV[481] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup240);
		rawBMV[479] &= (~(0x0f<<4)); 
		rawBMV[479] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage240)/0.010000;
		rawBMV[478] &= 0xff&(raw<<0);
		rawBMV[479] &= (~0x0f);
		rawBMV[479] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup239);
		rawBMV[477] &= (~(0x0f<<4)); 
		rawBMV[477] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage239)/0.010000;
		rawBMV[476] &= 0xff&(raw<<0);
		rawBMV[477] &= (~0x0f);
		rawBMV[477] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup238);
		rawBMV[475] &= (~(0x0f<<4)); 
		rawBMV[475] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage238)/0.010000;
		rawBMV[474] &= 0xff&(raw<<0);
		rawBMV[475] &= (~0x0f);
		rawBMV[475] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup237);
		rawBMV[473] &= (~(0x0f<<4)); 
		rawBMV[473] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage237)/0.010000;
		rawBMV[472] &= 0xff&(raw<<0);
		rawBMV[473] &= (~0x0f);
		rawBMV[473] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup236);
		rawBMV[471] &= (~(0x0f<<4)); 
		rawBMV[471] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage236)/0.010000;
		rawBMV[470] &= 0xff&(raw<<0);
		rawBMV[471] &= (~0x0f);
		rawBMV[471] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup235);
		rawBMV[469] &= (~(0x0f<<4)); 
		rawBMV[469] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage235)/0.010000;
		rawBMV[468] &= 0xff&(raw<<0);
		rawBMV[469] &= (~0x0f);
		rawBMV[469] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup234);
		rawBMV[467] &= (~(0x0f<<4)); 
		rawBMV[467] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage234)/0.010000;
		rawBMV[466] &= 0xff&(raw<<0);
		rawBMV[467] &= (~0x0f);
		rawBMV[467] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup233);
		rawBMV[465] &= (~(0x0f<<4)); 
		rawBMV[465] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage233)/0.010000;
		rawBMV[464] &= 0xff&(raw<<0);
		rawBMV[465] &= (~0x0f);
		rawBMV[465] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup232);
		rawBMV[463] &= (~(0x0f<<4)); 
		rawBMV[463] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage232)/0.010000;
		rawBMV[462] &= 0xff&(raw<<0);
		rawBMV[463] &= (~0x0f);
		rawBMV[463] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup231);
		rawBMV[461] &= (~(0x0f<<4)); 
		rawBMV[461] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage231)/0.010000;
		rawBMV[460] &= 0xff&(raw<<0);
		rawBMV[461] &= (~0x0f);
		rawBMV[461] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup230);
		rawBMV[459] &= (~(0x0f<<4)); 
		rawBMV[459] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage230)/0.010000;
		rawBMV[458] &= 0xff&(raw<<0);
		rawBMV[459] &= (~0x0f);
		rawBMV[459] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup229);
		rawBMV[457] &= (~(0x0f<<4)); 
		rawBMV[457] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage229)/0.010000;
		rawBMV[456] &= 0xff&(raw<<0);
		rawBMV[457] &= (~0x0f);
		rawBMV[457] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup228);
		rawBMV[455] &= (~(0x0f<<4)); 
		rawBMV[455] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage228)/0.010000;
		rawBMV[454] &= 0xff&(raw<<0);
		rawBMV[455] &= (~0x0f);
		rawBMV[455] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup227);
		rawBMV[453] &= (~(0x0f<<4)); 
		rawBMV[453] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage227)/0.010000;
		rawBMV[452] &= 0xff&(raw<<0);
		rawBMV[453] &= (~0x0f);
		rawBMV[453] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup226);
		rawBMV[451] &= (~(0x0f<<4)); 
		rawBMV[451] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage226)/0.010000;
		rawBMV[450] &= 0xff&(raw<<0);
		rawBMV[451] &= (~0x0f);
		rawBMV[451] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup225);
		rawBMV[449] &= (~(0x0f<<4)); 
		rawBMV[449] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage225)/0.010000;
		rawBMV[448] &= 0xff&(raw<<0);
		rawBMV[449] &= (~0x0f);
		rawBMV[449] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup224);
		rawBMV[447] &= (~(0x0f<<4)); 
		rawBMV[447] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage224)/0.010000;
		rawBMV[446] &= 0xff&(raw<<0);
		rawBMV[447] &= (~0x0f);
		rawBMV[447] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup223);
		rawBMV[445] &= (~(0x0f<<4)); 
		rawBMV[445] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage223)/0.010000;
		rawBMV[444] &= 0xff&(raw<<0);
		rawBMV[445] &= (~0x0f);
		rawBMV[445] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup222);
		rawBMV[443] &= (~(0x0f<<4)); 
		rawBMV[443] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage222)/0.010000;
		rawBMV[442] &= 0xff&(raw<<0);
		rawBMV[443] &= (~0x0f);
		rawBMV[443] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup221);
		rawBMV[441] &= (~(0x0f<<4)); 
		rawBMV[441] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage221)/0.010000;
		rawBMV[440] &= 0xff&(raw<<0);
		rawBMV[441] &= (~0x0f);
		rawBMV[441] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup220);
		rawBMV[439] &= (~(0x0f<<4)); 
		rawBMV[439] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage220)/0.010000;
		rawBMV[438] &= 0xff&(raw<<0);
		rawBMV[439] &= (~0x0f);
		rawBMV[439] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup219);
		rawBMV[437] &= (~(0x0f<<4)); 
		rawBMV[437] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage219)/0.010000;
		rawBMV[436] &= 0xff&(raw<<0);
		rawBMV[437] &= (~0x0f);
		rawBMV[437] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup218);
		rawBMV[435] &= (~(0x0f<<4)); 
		rawBMV[435] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage218)/0.010000;
		rawBMV[434] &= 0xff&(raw<<0);
		rawBMV[435] &= (~0x0f);
		rawBMV[435] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup217);
		rawBMV[433] &= (~(0x0f<<4)); 
		rawBMV[433] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage217)/0.010000;
		rawBMV[432] &= 0xff&(raw<<0);
		rawBMV[433] &= (~0x0f);
		rawBMV[433] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup216);
		rawBMV[431] &= (~(0x0f<<4)); 
		rawBMV[431] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage216)/0.010000;
		rawBMV[430] &= 0xff&(raw<<0);
		rawBMV[431] &= (~0x0f);
		rawBMV[431] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup215);
		rawBMV[429] &= (~(0x0f<<4)); 
		rawBMV[429] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage215)/0.010000;
		rawBMV[428] &= 0xff&(raw<<0);
		rawBMV[429] &= (~0x0f);
		rawBMV[429] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup214);
		rawBMV[427] &= (~(0x0f<<4)); 
		rawBMV[427] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage214)/0.010000;
		rawBMV[426] &= 0xff&(raw<<0);
		rawBMV[427] &= (~0x0f);
		rawBMV[427] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup213);
		rawBMV[425] &= (~(0x0f<<4)); 
		rawBMV[425] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage213)/0.010000;
		rawBMV[424] &= 0xff&(raw<<0);
		rawBMV[425] &= (~0x0f);
		rawBMV[425] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup212);
		rawBMV[423] &= (~(0x0f<<4)); 
		rawBMV[423] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage212)/0.010000;
		rawBMV[422] &= 0xff&(raw<<0);
		rawBMV[423] &= (~0x0f);
		rawBMV[423] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup211);
		rawBMV[421] &= (~(0x0f<<4)); 
		rawBMV[421] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage211)/0.010000;
		rawBMV[420] &= 0xff&(raw<<0);
		rawBMV[421] &= (~0x0f);
		rawBMV[421] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup210);
		rawBMV[419] &= (~(0x0f<<4)); 
		rawBMV[419] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage210)/0.010000;
		rawBMV[418] &= 0xff&(raw<<0);
		rawBMV[419] &= (~0x0f);
		rawBMV[419] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup209);
		rawBMV[417] &= (~(0x0f<<4)); 
		rawBMV[417] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage209)/0.010000;
		rawBMV[416] &= 0xff&(raw<<0);
		rawBMV[417] &= (~0x0f);
		rawBMV[417] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup208);
		rawBMV[415] &= (~(0x0f<<4)); 
		rawBMV[415] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage208)/0.010000;
		rawBMV[414] &= 0xff&(raw<<0);
		rawBMV[415] &= (~0x0f);
		rawBMV[415] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup207);
		rawBMV[413] &= (~(0x0f<<4)); 
		rawBMV[413] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage207)/0.010000;
		rawBMV[412] &= 0xff&(raw<<0);
		rawBMV[413] &= (~0x0f);
		rawBMV[413] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup206);
		rawBMV[411] &= (~(0x0f<<4)); 
		rawBMV[411] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage206)/0.010000;
		rawBMV[410] &= 0xff&(raw<<0);
		rawBMV[411] &= (~0x0f);
		rawBMV[411] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup205);
		rawBMV[409] &= (~(0x0f<<4)); 
		rawBMV[409] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage205)/0.010000;
		rawBMV[408] &= 0xff&(raw<<0);
		rawBMV[409] &= (~0x0f);
		rawBMV[409] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup204);
		rawBMV[407] &= (~(0x0f<<4)); 
		rawBMV[407] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage204)/0.010000;
		rawBMV[406] &= 0xff&(raw<<0);
		rawBMV[407] &= (~0x0f);
		rawBMV[407] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup203);
		rawBMV[405] &= (~(0x0f<<4)); 
		rawBMV[405] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage203)/0.010000;
		rawBMV[404] &= 0xff&(raw<<0);
		rawBMV[405] &= (~0x0f);
		rawBMV[405] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup202);
		rawBMV[403] &= (~(0x0f<<4)); 
		rawBMV[403] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage202)/0.010000;
		rawBMV[402] &= 0xff&(raw<<0);
		rawBMV[403] &= (~0x0f);
		rawBMV[403] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup201);
		rawBMV[401] &= (~(0x0f<<4)); 
		rawBMV[401] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage201)/0.010000;
		rawBMV[400] &= 0xff&(raw<<0);
		rawBMV[401] &= (~0x0f);
		rawBMV[401] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup200);
		rawBMV[399] &= (~(0x0f<<4)); 
		rawBMV[399] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage200)/0.010000;
		rawBMV[398] &= 0xff&(raw<<0);
		rawBMV[399] &= (~0x0f);
		rawBMV[399] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup199);
		rawBMV[397] &= (~(0x0f<<4)); 
		rawBMV[397] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage199)/0.010000;
		rawBMV[396] &= 0xff&(raw<<0);
		rawBMV[397] &= (~0x0f);
		rawBMV[397] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup198);
		rawBMV[395] &= (~(0x0f<<4)); 
		rawBMV[395] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage198)/0.010000;
		rawBMV[394] &= 0xff&(raw<<0);
		rawBMV[395] &= (~0x0f);
		rawBMV[395] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup197);
		rawBMV[393] &= (~(0x0f<<4)); 
		rawBMV[393] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage197)/0.010000;
		rawBMV[392] &= 0xff&(raw<<0);
		rawBMV[393] &= (~0x0f);
		rawBMV[393] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup196);
		rawBMV[391] &= (~(0x0f<<4)); 
		rawBMV[391] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage196)/0.010000;
		rawBMV[390] &= 0xff&(raw<<0);
		rawBMV[391] &= (~0x0f);
		rawBMV[391] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup195);
		rawBMV[389] &= (~(0x0f<<4)); 
		rawBMV[389] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage195)/0.010000;
		rawBMV[388] &= 0xff&(raw<<0);
		rawBMV[389] &= (~0x0f);
		rawBMV[389] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup194);
		rawBMV[387] &= (~(0x0f<<4)); 
		rawBMV[387] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage194)/0.010000;
		rawBMV[386] &= 0xff&(raw<<0);
		rawBMV[387] &= (~0x0f);
		rawBMV[387] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup193);
		rawBMV[385] &= (~(0x0f<<4)); 
		rawBMV[385] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage193)/0.010000;
		rawBMV[384] &= 0xff&(raw<<0);
		rawBMV[385] &= (~0x0f);
		rawBMV[385] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup192);
		rawBMV[383] &= (~(0x0f<<4)); 
		rawBMV[383] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage192)/0.010000;
		rawBMV[382] &= 0xff&(raw<<0);
		rawBMV[383] &= (~0x0f);
		rawBMV[383] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup191);
		rawBMV[381] &= (~(0x0f<<4)); 
		rawBMV[381] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage191)/0.010000;
		rawBMV[380] &= 0xff&(raw<<0);
		rawBMV[381] &= (~0x0f);
		rawBMV[381] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup190);
		rawBMV[379] &= (~(0x0f<<4)); 
		rawBMV[379] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage190)/0.010000;
		rawBMV[378] &= 0xff&(raw<<0);
		rawBMV[379] &= (~0x0f);
		rawBMV[379] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup189);
		rawBMV[377] &= (~(0x0f<<4)); 
		rawBMV[377] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage189)/0.010000;
		rawBMV[376] &= 0xff&(raw<<0);
		rawBMV[377] &= (~0x0f);
		rawBMV[377] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup188);
		rawBMV[375] &= (~(0x0f<<4)); 
		rawBMV[375] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage188)/0.010000;
		rawBMV[374] &= 0xff&(raw<<0);
		rawBMV[375] &= (~0x0f);
		rawBMV[375] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup187);
		rawBMV[373] &= (~(0x0f<<4)); 
		rawBMV[373] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage187)/0.010000;
		rawBMV[372] &= 0xff&(raw<<0);
		rawBMV[373] &= (~0x0f);
		rawBMV[373] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup186);
		rawBMV[371] &= (~(0x0f<<4)); 
		rawBMV[371] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage186)/0.010000;
		rawBMV[370] &= 0xff&(raw<<0);
		rawBMV[371] &= (~0x0f);
		rawBMV[371] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup185);
		rawBMV[369] &= (~(0x0f<<4)); 
		rawBMV[369] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage185)/0.010000;
		rawBMV[368] &= 0xff&(raw<<0);
		rawBMV[369] &= (~0x0f);
		rawBMV[369] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup184);
		rawBMV[367] &= (~(0x0f<<4)); 
		rawBMV[367] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage184)/0.010000;
		rawBMV[366] &= 0xff&(raw<<0);
		rawBMV[367] &= (~0x0f);
		rawBMV[367] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup183);
		rawBMV[365] &= (~(0x0f<<4)); 
		rawBMV[365] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage183)/0.010000;
		rawBMV[364] &= 0xff&(raw<<0);
		rawBMV[365] &= (~0x0f);
		rawBMV[365] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup182);
		rawBMV[363] &= (~(0x0f<<4)); 
		rawBMV[363] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage182)/0.010000;
		rawBMV[362] &= 0xff&(raw<<0);
		rawBMV[363] &= (~0x0f);
		rawBMV[363] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup181);
		rawBMV[361] &= (~(0x0f<<4)); 
		rawBMV[361] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage181)/0.010000;
		rawBMV[360] &= 0xff&(raw<<0);
		rawBMV[361] &= (~0x0f);
		rawBMV[361] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup180);
		rawBMV[359] &= (~(0x0f<<4)); 
		rawBMV[359] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage180)/0.010000;
		rawBMV[358] &= 0xff&(raw<<0);
		rawBMV[359] &= (~0x0f);
		rawBMV[359] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup179);
		rawBMV[357] &= (~(0x0f<<4)); 
		rawBMV[357] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage179)/0.010000;
		rawBMV[356] &= 0xff&(raw<<0);
		rawBMV[357] &= (~0x0f);
		rawBMV[357] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup178);
		rawBMV[355] &= (~(0x0f<<4)); 
		rawBMV[355] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage178)/0.010000;
		rawBMV[354] &= 0xff&(raw<<0);
		rawBMV[355] &= (~0x0f);
		rawBMV[355] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup177);
		rawBMV[353] &= (~(0x0f<<4)); 
		rawBMV[353] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage177)/0.010000;
		rawBMV[352] &= 0xff&(raw<<0);
		rawBMV[353] &= (~0x0f);
		rawBMV[353] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup176);
		rawBMV[351] &= (~(0x0f<<4)); 
		rawBMV[351] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage176)/0.010000;
		rawBMV[350] &= 0xff&(raw<<0);
		rawBMV[351] &= (~0x0f);
		rawBMV[351] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup175);
		rawBMV[349] &= (~(0x0f<<4)); 
		rawBMV[349] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage175)/0.010000;
		rawBMV[348] &= 0xff&(raw<<0);
		rawBMV[349] &= (~0x0f);
		rawBMV[349] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup174);
		rawBMV[347] &= (~(0x0f<<4)); 
		rawBMV[347] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage174)/0.010000;
		rawBMV[346] &= 0xff&(raw<<0);
		rawBMV[347] &= (~0x0f);
		rawBMV[347] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup173);
		rawBMV[345] &= (~(0x0f<<4)); 
		rawBMV[345] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage173)/0.010000;
		rawBMV[344] &= 0xff&(raw<<0);
		rawBMV[345] &= (~0x0f);
		rawBMV[345] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup172);
		rawBMV[343] &= (~(0x0f<<4)); 
		rawBMV[343] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage172)/0.010000;
		rawBMV[342] &= 0xff&(raw<<0);
		rawBMV[343] &= (~0x0f);
		rawBMV[343] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup171);
		rawBMV[341] &= (~(0x0f<<4)); 
		rawBMV[341] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage171)/0.010000;
		rawBMV[340] &= 0xff&(raw<<0);
		rawBMV[341] &= (~0x0f);
		rawBMV[341] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup170);
		rawBMV[339] &= (~(0x0f<<4)); 
		rawBMV[339] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage170)/0.010000;
		rawBMV[338] &= 0xff&(raw<<0);
		rawBMV[339] &= (~0x0f);
		rawBMV[339] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup169);
		rawBMV[337] &= (~(0x0f<<4)); 
		rawBMV[337] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage169)/0.010000;
		rawBMV[336] &= 0xff&(raw<<0);
		rawBMV[337] &= (~0x0f);
		rawBMV[337] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup168);
		rawBMV[335] &= (~(0x0f<<4)); 
		rawBMV[335] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage168)/0.010000;
		rawBMV[334] &= 0xff&(raw<<0);
		rawBMV[335] &= (~0x0f);
		rawBMV[335] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup167);
		rawBMV[333] &= (~(0x0f<<4)); 
		rawBMV[333] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage167)/0.010000;
		rawBMV[332] &= 0xff&(raw<<0);
		rawBMV[333] &= (~0x0f);
		rawBMV[333] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup166);
		rawBMV[331] &= (~(0x0f<<4)); 
		rawBMV[331] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage166)/0.010000;
		rawBMV[330] &= 0xff&(raw<<0);
		rawBMV[331] &= (~0x0f);
		rawBMV[331] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup165);
		rawBMV[329] &= (~(0x0f<<4)); 
		rawBMV[329] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage165)/0.010000;
		rawBMV[328] &= 0xff&(raw<<0);
		rawBMV[329] &= (~0x0f);
		rawBMV[329] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup164);
		rawBMV[327] &= (~(0x0f<<4)); 
		rawBMV[327] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage164)/0.010000;
		rawBMV[326] &= 0xff&(raw<<0);
		rawBMV[327] &= (~0x0f);
		rawBMV[327] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup163);
		rawBMV[325] &= (~(0x0f<<4)); 
		rawBMV[325] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage163)/0.010000;
		rawBMV[324] &= 0xff&(raw<<0);
		rawBMV[325] &= (~0x0f);
		rawBMV[325] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup162);
		rawBMV[323] &= (~(0x0f<<4)); 
		rawBMV[323] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage162)/0.010000;
		rawBMV[322] &= 0xff&(raw<<0);
		rawBMV[323] &= (~0x0f);
		rawBMV[323] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup161);
		rawBMV[321] &= (~(0x0f<<4)); 
		rawBMV[321] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage161)/0.010000;
		rawBMV[320] &= 0xff&(raw<<0);
		rawBMV[321] &= (~0x0f);
		rawBMV[321] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup160);
		rawBMV[319] &= (~(0x0f<<4)); 
		rawBMV[319] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage160)/0.010000;
		rawBMV[318] &= 0xff&(raw<<0);
		rawBMV[319] &= (~0x0f);
		rawBMV[319] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup159);
		rawBMV[317] &= (~(0x0f<<4)); 
		rawBMV[317] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage159)/0.010000;
		rawBMV[316] &= 0xff&(raw<<0);
		rawBMV[317] &= (~0x0f);
		rawBMV[317] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup158);
		rawBMV[315] &= (~(0x0f<<4)); 
		rawBMV[315] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage158)/0.010000;
		rawBMV[314] &= 0xff&(raw<<0);
		rawBMV[315] &= (~0x0f);
		rawBMV[315] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup157);
		rawBMV[313] &= (~(0x0f<<4)); 
		rawBMV[313] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage157)/0.010000;
		rawBMV[312] &= 0xff&(raw<<0);
		rawBMV[313] &= (~0x0f);
		rawBMV[313] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup156);
		rawBMV[311] &= (~(0x0f<<4)); 
		rawBMV[311] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage156)/0.010000;
		rawBMV[310] &= 0xff&(raw<<0);
		rawBMV[311] &= (~0x0f);
		rawBMV[311] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup155);
		rawBMV[309] &= (~(0x0f<<4)); 
		rawBMV[309] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage155)/0.010000;
		rawBMV[308] &= 0xff&(raw<<0);
		rawBMV[309] &= (~0x0f);
		rawBMV[309] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup154);
		rawBMV[307] &= (~(0x0f<<4)); 
		rawBMV[307] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage154)/0.010000;
		rawBMV[306] &= 0xff&(raw<<0);
		rawBMV[307] &= (~0x0f);
		rawBMV[307] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup153);
		rawBMV[305] &= (~(0x0f<<4)); 
		rawBMV[305] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage153)/0.010000;
		rawBMV[304] &= 0xff&(raw<<0);
		rawBMV[305] &= (~0x0f);
		rawBMV[305] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup152);
		rawBMV[303] &= (~(0x0f<<4)); 
		rawBMV[303] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage152)/0.010000;
		rawBMV[302] &= 0xff&(raw<<0);
		rawBMV[303] &= (~0x0f);
		rawBMV[303] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup151);
		rawBMV[301] &= (~(0x0f<<4)); 
		rawBMV[301] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage151)/0.010000;
		rawBMV[300] &= 0xff&(raw<<0);
		rawBMV[301] &= (~0x0f);
		rawBMV[301] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup150);
		rawBMV[299] &= (~(0x0f<<4)); 
		rawBMV[299] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage150)/0.010000;
		rawBMV[298] &= 0xff&(raw<<0);
		rawBMV[299] &= (~0x0f);
		rawBMV[299] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup149);
		rawBMV[297] &= (~(0x0f<<4)); 
		rawBMV[297] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage149)/0.010000;
		rawBMV[296] &= 0xff&(raw<<0);
		rawBMV[297] &= (~0x0f);
		rawBMV[297] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup148);
		rawBMV[295] &= (~(0x0f<<4)); 
		rawBMV[295] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage148)/0.010000;
		rawBMV[294] &= 0xff&(raw<<0);
		rawBMV[295] &= (~0x0f);
		rawBMV[295] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup147);
		rawBMV[293] &= (~(0x0f<<4)); 
		rawBMV[293] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage147)/0.010000;
		rawBMV[292] &= 0xff&(raw<<0);
		rawBMV[293] &= (~0x0f);
		rawBMV[293] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup146);
		rawBMV[291] &= (~(0x0f<<4)); 
		rawBMV[291] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage146)/0.010000;
		rawBMV[290] &= 0xff&(raw<<0);
		rawBMV[291] &= (~0x0f);
		rawBMV[291] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup145);
		rawBMV[289] &= (~(0x0f<<4)); 
		rawBMV[289] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage145)/0.010000;
		rawBMV[288] &= 0xff&(raw<<0);
		rawBMV[289] &= (~0x0f);
		rawBMV[289] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup144);
		rawBMV[287] &= (~(0x0f<<4)); 
		rawBMV[287] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage144)/0.010000;
		rawBMV[286] &= 0xff&(raw<<0);
		rawBMV[287] &= (~0x0f);
		rawBMV[287] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup143);
		rawBMV[285] &= (~(0x0f<<4)); 
		rawBMV[285] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage143)/0.010000;
		rawBMV[284] &= 0xff&(raw<<0);
		rawBMV[285] &= (~0x0f);
		rawBMV[285] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup142);
		rawBMV[283] &= (~(0x0f<<4)); 
		rawBMV[283] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage142)/0.010000;
		rawBMV[282] &= 0xff&(raw<<0);
		rawBMV[283] &= (~0x0f);
		rawBMV[283] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup141);
		rawBMV[281] &= (~(0x0f<<4)); 
		rawBMV[281] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage141)/0.010000;
		rawBMV[280] &= 0xff&(raw<<0);
		rawBMV[281] &= (~0x0f);
		rawBMV[281] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup140);
		rawBMV[279] &= (~(0x0f<<4)); 
		rawBMV[279] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage140)/0.010000;
		rawBMV[278] &= 0xff&(raw<<0);
		rawBMV[279] &= (~0x0f);
		rawBMV[279] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup139);
		rawBMV[277] &= (~(0x0f<<4)); 
		rawBMV[277] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage139)/0.010000;
		rawBMV[276] &= 0xff&(raw<<0);
		rawBMV[277] &= (~0x0f);
		rawBMV[277] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup138);
		rawBMV[275] &= (~(0x0f<<4)); 
		rawBMV[275] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage138)/0.010000;
		rawBMV[274] &= 0xff&(raw<<0);
		rawBMV[275] &= (~0x0f);
		rawBMV[275] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup137);
		rawBMV[273] &= (~(0x0f<<4)); 
		rawBMV[273] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage137)/0.010000;
		rawBMV[272] &= 0xff&(raw<<0);
		rawBMV[273] &= (~0x0f);
		rawBMV[273] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup136);
		rawBMV[271] &= (~(0x0f<<4)); 
		rawBMV[271] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage136)/0.010000;
		rawBMV[270] &= 0xff&(raw<<0);
		rawBMV[271] &= (~0x0f);
		rawBMV[271] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup135);
		rawBMV[269] &= (~(0x0f<<4)); 
		rawBMV[269] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage135)/0.010000;
		rawBMV[268] &= 0xff&(raw<<0);
		rawBMV[269] &= (~0x0f);
		rawBMV[269] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup134);
		rawBMV[267] &= (~(0x0f<<4)); 
		rawBMV[267] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage134)/0.010000;
		rawBMV[266] &= 0xff&(raw<<0);
		rawBMV[267] &= (~0x0f);
		rawBMV[267] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup133);
		rawBMV[265] &= (~(0x0f<<4)); 
		rawBMV[265] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage133)/0.010000;
		rawBMV[264] &= 0xff&(raw<<0);
		rawBMV[265] &= (~0x0f);
		rawBMV[265] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup132);
		rawBMV[263] &= (~(0x0f<<4)); 
		rawBMV[263] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage132)/0.010000;
		rawBMV[262] &= 0xff&(raw<<0);
		rawBMV[263] &= (~0x0f);
		rawBMV[263] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup131);
		rawBMV[261] &= (~(0x0f<<4)); 
		rawBMV[261] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage131)/0.010000;
		rawBMV[260] &= 0xff&(raw<<0);
		rawBMV[261] &= (~0x0f);
		rawBMV[261] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup130);
		rawBMV[259] &= (~(0x0f<<4)); 
		rawBMV[259] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage130)/0.010000;
		rawBMV[258] &= 0xff&(raw<<0);
		rawBMV[259] &= (~0x0f);
		rawBMV[259] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup129);
		rawBMV[257] &= (~(0x0f<<4)); 
		rawBMV[257] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage129)/0.010000;
		rawBMV[256] &= 0xff&(raw<<0);
		rawBMV[257] &= (~0x0f);
		rawBMV[257] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup128);
		rawBMV[255] &= (~(0x0f<<4)); 
		rawBMV[255] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage128)/0.010000;
		rawBMV[254] &= 0xff&(raw<<0);
		rawBMV[255] &= (~0x0f);
		rawBMV[255] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup127);
		rawBMV[253] &= (~(0x0f<<4)); 
		rawBMV[253] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage127)/0.010000;
		rawBMV[252] &= 0xff&(raw<<0);
		rawBMV[253] &= (~0x0f);
		rawBMV[253] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup126);
		rawBMV[251] &= (~(0x0f<<4)); 
		rawBMV[251] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage126)/0.010000;
		rawBMV[250] &= 0xff&(raw<<0);
		rawBMV[251] &= (~0x0f);
		rawBMV[251] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup125);
		rawBMV[249] &= (~(0x0f<<4)); 
		rawBMV[249] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage125)/0.010000;
		rawBMV[248] &= 0xff&(raw<<0);
		rawBMV[249] &= (~0x0f);
		rawBMV[249] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup124);
		rawBMV[247] &= (~(0x0f<<4)); 
		rawBMV[247] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage124)/0.010000;
		rawBMV[246] &= 0xff&(raw<<0);
		rawBMV[247] &= (~0x0f);
		rawBMV[247] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup123);
		rawBMV[245] &= (~(0x0f<<4)); 
		rawBMV[245] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage123)/0.010000;
		rawBMV[244] &= 0xff&(raw<<0);
		rawBMV[245] &= (~0x0f);
		rawBMV[245] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup122);
		rawBMV[243] &= (~(0x0f<<4)); 
		rawBMV[243] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage122)/0.010000;
		rawBMV[242] &= 0xff&(raw<<0);
		rawBMV[243] &= (~0x0f);
		rawBMV[243] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup121);
		rawBMV[241] &= (~(0x0f<<4)); 
		rawBMV[241] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage121)/0.010000;
		rawBMV[240] &= 0xff&(raw<<0);
		rawBMV[241] &= (~0x0f);
		rawBMV[241] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup120);
		rawBMV[239] &= (~(0x0f<<4)); 
		rawBMV[239] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage120)/0.010000;
		rawBMV[238] &= 0xff&(raw<<0);
		rawBMV[239] &= (~0x0f);
		rawBMV[239] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup119);
		rawBMV[237] &= (~(0x0f<<4)); 
		rawBMV[237] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage119)/0.010000;
		rawBMV[236] &= 0xff&(raw<<0);
		rawBMV[237] &= (~0x0f);
		rawBMV[237] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup118);
		rawBMV[235] &= (~(0x0f<<4)); 
		rawBMV[235] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage118)/0.010000;
		rawBMV[234] &= 0xff&(raw<<0);
		rawBMV[235] &= (~0x0f);
		rawBMV[235] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup117);
		rawBMV[233] &= (~(0x0f<<4)); 
		rawBMV[233] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage117)/0.010000;
		rawBMV[232] &= 0xff&(raw<<0);
		rawBMV[233] &= (~0x0f);
		rawBMV[233] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup116);
		rawBMV[231] &= (~(0x0f<<4)); 
		rawBMV[231] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage116)/0.010000;
		rawBMV[230] &= 0xff&(raw<<0);
		rawBMV[231] &= (~0x0f);
		rawBMV[231] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup115);
		rawBMV[229] &= (~(0x0f<<4)); 
		rawBMV[229] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage115)/0.010000;
		rawBMV[228] &= 0xff&(raw<<0);
		rawBMV[229] &= (~0x0f);
		rawBMV[229] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup114);
		rawBMV[227] &= (~(0x0f<<4)); 
		rawBMV[227] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage114)/0.010000;
		rawBMV[226] &= 0xff&(raw<<0);
		rawBMV[227] &= (~0x0f);
		rawBMV[227] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup113);
		rawBMV[225] &= (~(0x0f<<4)); 
		rawBMV[225] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage113)/0.010000;
		rawBMV[224] &= 0xff&(raw<<0);
		rawBMV[225] &= (~0x0f);
		rawBMV[225] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup112);
		rawBMV[223] &= (~(0x0f<<4)); 
		rawBMV[223] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage112)/0.010000;
		rawBMV[222] &= 0xff&(raw<<0);
		rawBMV[223] &= (~0x0f);
		rawBMV[223] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup111);
		rawBMV[221] &= (~(0x0f<<4)); 
		rawBMV[221] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage111)/0.010000;
		rawBMV[220] &= 0xff&(raw<<0);
		rawBMV[221] &= (~0x0f);
		rawBMV[221] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup110);
		rawBMV[219] &= (~(0x0f<<4)); 
		rawBMV[219] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage110)/0.010000;
		rawBMV[218] &= 0xff&(raw<<0);
		rawBMV[219] &= (~0x0f);
		rawBMV[219] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup109);
		rawBMV[217] &= (~(0x0f<<4)); 
		rawBMV[217] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage109)/0.010000;
		rawBMV[216] &= 0xff&(raw<<0);
		rawBMV[217] &= (~0x0f);
		rawBMV[217] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup108);
		rawBMV[215] &= (~(0x0f<<4)); 
		rawBMV[215] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage108)/0.010000;
		rawBMV[214] &= 0xff&(raw<<0);
		rawBMV[215] &= (~0x0f);
		rawBMV[215] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup107);
		rawBMV[213] &= (~(0x0f<<4)); 
		rawBMV[213] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage107)/0.010000;
		rawBMV[212] &= 0xff&(raw<<0);
		rawBMV[213] &= (~0x0f);
		rawBMV[213] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup106);
		rawBMV[211] &= (~(0x0f<<4)); 
		rawBMV[211] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage106)/0.010000;
		rawBMV[210] &= 0xff&(raw<<0);
		rawBMV[211] &= (~0x0f);
		rawBMV[211] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup105);
		rawBMV[209] &= (~(0x0f<<4)); 
		rawBMV[209] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage105)/0.010000;
		rawBMV[208] &= 0xff&(raw<<0);
		rawBMV[209] &= (~0x0f);
		rawBMV[209] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup104);
		rawBMV[207] &= (~(0x0f<<4)); 
		rawBMV[207] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage104)/0.010000;
		rawBMV[206] &= 0xff&(raw<<0);
		rawBMV[207] &= (~0x0f);
		rawBMV[207] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup103);
		rawBMV[205] &= (~(0x0f<<4)); 
		rawBMV[205] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage103)/0.010000;
		rawBMV[204] &= 0xff&(raw<<0);
		rawBMV[205] &= (~0x0f);
		rawBMV[205] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup102);
		rawBMV[203] &= (~(0x0f<<4)); 
		rawBMV[203] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage102)/0.010000;
		rawBMV[202] &= 0xff&(raw<<0);
		rawBMV[203] &= (~0x0f);
		rawBMV[203] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup101);
		rawBMV[201] &= (~(0x0f<<4)); 
		rawBMV[201] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage101)/0.010000;
		rawBMV[200] &= 0xff&(raw<<0);
		rawBMV[201] &= (~0x0f);
		rawBMV[201] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup100);
		rawBMV[199] &= (~(0x0f<<4)); 
		rawBMV[199] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage100)/0.010000;
		rawBMV[198] &= 0xff&(raw<<0);
		rawBMV[199] &= (~0x0f);
		rawBMV[199] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup099);
		rawBMV[197] &= (~(0x0f<<4)); 
		rawBMV[197] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage099)/0.010000;
		rawBMV[196] &= 0xff&(raw<<0);
		rawBMV[197] &= (~0x0f);
		rawBMV[197] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup098);
		rawBMV[195] &= (~(0x0f<<4)); 
		rawBMV[195] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage098)/0.010000;
		rawBMV[194] &= 0xff&(raw<<0);
		rawBMV[195] &= (~0x0f);
		rawBMV[195] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup097);
		rawBMV[193] &= (~(0x0f<<4)); 
		rawBMV[193] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage097)/0.010000;
		rawBMV[192] &= 0xff&(raw<<0);
		rawBMV[193] &= (~0x0f);
		rawBMV[193] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup096);
		rawBMV[191] &= (~(0x0f<<4)); 
		rawBMV[191] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage096)/0.010000;
		rawBMV[190] &= 0xff&(raw<<0);
		rawBMV[191] &= (~0x0f);
		rawBMV[191] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup095);
		rawBMV[189] &= (~(0x0f<<4)); 
		rawBMV[189] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage095)/0.010000;
		rawBMV[188] &= 0xff&(raw<<0);
		rawBMV[189] &= (~0x0f);
		rawBMV[189] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup094);
		rawBMV[187] &= (~(0x0f<<4)); 
		rawBMV[187] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage094)/0.010000;
		rawBMV[186] &= 0xff&(raw<<0);
		rawBMV[187] &= (~0x0f);
		rawBMV[187] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup093);
		rawBMV[185] &= (~(0x0f<<4)); 
		rawBMV[185] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage093)/0.010000;
		rawBMV[184] &= 0xff&(raw<<0);
		rawBMV[185] &= (~0x0f);
		rawBMV[185] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup092);
		rawBMV[183] &= (~(0x0f<<4)); 
		rawBMV[183] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage092)/0.010000;
		rawBMV[182] &= 0xff&(raw<<0);
		rawBMV[183] &= (~0x0f);
		rawBMV[183] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup091);
		rawBMV[181] &= (~(0x0f<<4)); 
		rawBMV[181] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage091)/0.010000;
		rawBMV[180] &= 0xff&(raw<<0);
		rawBMV[181] &= (~0x0f);
		rawBMV[181] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup090);
		rawBMV[179] &= (~(0x0f<<4)); 
		rawBMV[179] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage090)/0.010000;
		rawBMV[178] &= 0xff&(raw<<0);
		rawBMV[179] &= (~0x0f);
		rawBMV[179] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup089);
		rawBMV[177] &= (~(0x0f<<4)); 
		rawBMV[177] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage089)/0.010000;
		rawBMV[176] &= 0xff&(raw<<0);
		rawBMV[177] &= (~0x0f);
		rawBMV[177] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup088);
		rawBMV[175] &= (~(0x0f<<4)); 
		rawBMV[175] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage088)/0.010000;
		rawBMV[174] &= 0xff&(raw<<0);
		rawBMV[175] &= (~0x0f);
		rawBMV[175] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup087);
		rawBMV[173] &= (~(0x0f<<4)); 
		rawBMV[173] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage087)/0.010000;
		rawBMV[172] &= 0xff&(raw<<0);
		rawBMV[173] &= (~0x0f);
		rawBMV[173] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup086);
		rawBMV[171] &= (~(0x0f<<4)); 
		rawBMV[171] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage086)/0.010000;
		rawBMV[170] &= 0xff&(raw<<0);
		rawBMV[171] &= (~0x0f);
		rawBMV[171] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup085);
		rawBMV[169] &= (~(0x0f<<4)); 
		rawBMV[169] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage085)/0.010000;
		rawBMV[168] &= 0xff&(raw<<0);
		rawBMV[169] &= (~0x0f);
		rawBMV[169] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup084);
		rawBMV[167] &= (~(0x0f<<4)); 
		rawBMV[167] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage084)/0.010000;
		rawBMV[166] &= 0xff&(raw<<0);
		rawBMV[167] &= (~0x0f);
		rawBMV[167] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup083);
		rawBMV[165] &= (~(0x0f<<4)); 
		rawBMV[165] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage083)/0.010000;
		rawBMV[164] &= 0xff&(raw<<0);
		rawBMV[165] &= (~0x0f);
		rawBMV[165] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup082);
		rawBMV[163] &= (~(0x0f<<4)); 
		rawBMV[163] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage082)/0.010000;
		rawBMV[162] &= 0xff&(raw<<0);
		rawBMV[163] &= (~0x0f);
		rawBMV[163] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup081);
		rawBMV[161] &= (~(0x0f<<4)); 
		rawBMV[161] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage081)/0.010000;
		rawBMV[160] &= 0xff&(raw<<0);
		rawBMV[161] &= (~0x0f);
		rawBMV[161] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup080);
		rawBMV[159] &= (~(0x0f<<4)); 
		rawBMV[159] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage080)/0.010000;
		rawBMV[158] &= 0xff&(raw<<0);
		rawBMV[159] &= (~0x0f);
		rawBMV[159] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup079);
		rawBMV[157] &= (~(0x0f<<4)); 
		rawBMV[157] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage079)/0.010000;
		rawBMV[156] &= 0xff&(raw<<0);
		rawBMV[157] &= (~0x0f);
		rawBMV[157] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup078);
		rawBMV[155] &= (~(0x0f<<4)); 
		rawBMV[155] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage078)/0.010000;
		rawBMV[154] &= 0xff&(raw<<0);
		rawBMV[155] &= (~0x0f);
		rawBMV[155] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup077);
		rawBMV[153] &= (~(0x0f<<4)); 
		rawBMV[153] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage077)/0.010000;
		rawBMV[152] &= 0xff&(raw<<0);
		rawBMV[153] &= (~0x0f);
		rawBMV[153] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup076);
		rawBMV[151] &= (~(0x0f<<4)); 
		rawBMV[151] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage076)/0.010000;
		rawBMV[150] &= 0xff&(raw<<0);
		rawBMV[151] &= (~0x0f);
		rawBMV[151] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup075);
		rawBMV[149] &= (~(0x0f<<4)); 
		rawBMV[149] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage075)/0.010000;
		rawBMV[148] &= 0xff&(raw<<0);
		rawBMV[149] &= (~0x0f);
		rawBMV[149] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup074);
		rawBMV[147] &= (~(0x0f<<4)); 
		rawBMV[147] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage074)/0.010000;
		rawBMV[146] &= 0xff&(raw<<0);
		rawBMV[147] &= (~0x0f);
		rawBMV[147] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup073);
		rawBMV[145] &= (~(0x0f<<4)); 
		rawBMV[145] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage073)/0.010000;
		rawBMV[144] &= 0xff&(raw<<0);
		rawBMV[145] &= (~0x0f);
		rawBMV[145] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup072);
		rawBMV[143] &= (~(0x0f<<4)); 
		rawBMV[143] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage072)/0.010000;
		rawBMV[142] &= 0xff&(raw<<0);
		rawBMV[143] &= (~0x0f);
		rawBMV[143] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup071);
		rawBMV[141] &= (~(0x0f<<4)); 
		rawBMV[141] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage071)/0.010000;
		rawBMV[140] &= 0xff&(raw<<0);
		rawBMV[141] &= (~0x0f);
		rawBMV[141] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup070);
		rawBMV[139] &= (~(0x0f<<4)); 
		rawBMV[139] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage070)/0.010000;
		rawBMV[138] &= 0xff&(raw<<0);
		rawBMV[139] &= (~0x0f);
		rawBMV[139] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup069);
		rawBMV[137] &= (~(0x0f<<4)); 
		rawBMV[137] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage069)/0.010000;
		rawBMV[136] &= 0xff&(raw<<0);
		rawBMV[137] &= (~0x0f);
		rawBMV[137] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup068);
		rawBMV[135] &= (~(0x0f<<4)); 
		rawBMV[135] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage068)/0.010000;
		rawBMV[134] &= 0xff&(raw<<0);
		rawBMV[135] &= (~0x0f);
		rawBMV[135] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup067);
		rawBMV[133] &= (~(0x0f<<4)); 
		rawBMV[133] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage067)/0.010000;
		rawBMV[132] &= 0xff&(raw<<0);
		rawBMV[133] &= (~0x0f);
		rawBMV[133] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup066);
		rawBMV[131] &= (~(0x0f<<4)); 
		rawBMV[131] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage066)/0.010000;
		rawBMV[130] &= 0xff&(raw<<0);
		rawBMV[131] &= (~0x0f);
		rawBMV[131] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup065);
		rawBMV[129] &= (~(0x0f<<4)); 
		rawBMV[129] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage065)/0.010000;
		rawBMV[128] &= 0xff&(raw<<0);
		rawBMV[129] &= (~0x0f);
		rawBMV[129] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup064);
		rawBMV[127] &= (~(0x0f<<4)); 
		rawBMV[127] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage064)/0.010000;
		rawBMV[126] &= 0xff&(raw<<0);
		rawBMV[127] &= (~0x0f);
		rawBMV[127] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup063);
		rawBMV[125] &= (~(0x0f<<4)); 
		rawBMV[125] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage063)/0.010000;
		rawBMV[124] &= 0xff&(raw<<0);
		rawBMV[125] &= (~0x0f);
		rawBMV[125] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup062);
		rawBMV[123] &= (~(0x0f<<4)); 
		rawBMV[123] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage062)/0.010000;
		rawBMV[122] &= 0xff&(raw<<0);
		rawBMV[123] &= (~0x0f);
		rawBMV[123] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup061);
		rawBMV[121] &= (~(0x0f<<4)); 
		rawBMV[121] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage061)/0.010000;
		rawBMV[120] &= 0xff&(raw<<0);
		rawBMV[121] &= (~0x0f);
		rawBMV[121] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup060);
		rawBMV[119] &= (~(0x0f<<4)); 
		rawBMV[119] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage060)/0.010000;
		rawBMV[118] &= 0xff&(raw<<0);
		rawBMV[119] &= (~0x0f);
		rawBMV[119] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup059);
		rawBMV[117] &= (~(0x0f<<4)); 
		rawBMV[117] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage059)/0.010000;
		rawBMV[116] &= 0xff&(raw<<0);
		rawBMV[117] &= (~0x0f);
		rawBMV[117] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup058);
		rawBMV[115] &= (~(0x0f<<4)); 
		rawBMV[115] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage058)/0.010000;
		rawBMV[114] &= 0xff&(raw<<0);
		rawBMV[115] &= (~0x0f);
		rawBMV[115] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup057);
		rawBMV[113] &= (~(0x0f<<4)); 
		rawBMV[113] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage057)/0.010000;
		rawBMV[112] &= 0xff&(raw<<0);
		rawBMV[113] &= (~0x0f);
		rawBMV[113] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup056);
		rawBMV[111] &= (~(0x0f<<4)); 
		rawBMV[111] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage056)/0.010000;
		rawBMV[110] &= 0xff&(raw<<0);
		rawBMV[111] &= (~0x0f);
		rawBMV[111] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup055);
		rawBMV[109] &= (~(0x0f<<4)); 
		rawBMV[109] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage055)/0.010000;
		rawBMV[108] &= 0xff&(raw<<0);
		rawBMV[109] &= (~0x0f);
		rawBMV[109] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup054);
		rawBMV[107] &= (~(0x0f<<4)); 
		rawBMV[107] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage054)/0.010000;
		rawBMV[106] &= 0xff&(raw<<0);
		rawBMV[107] &= (~0x0f);
		rawBMV[107] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup053);
		rawBMV[105] &= (~(0x0f<<4)); 
		rawBMV[105] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage053)/0.010000;
		rawBMV[104] &= 0xff&(raw<<0);
		rawBMV[105] &= (~0x0f);
		rawBMV[105] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup052);
		rawBMV[103] &= (~(0x0f<<4)); 
		rawBMV[103] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage052)/0.010000;
		rawBMV[102] &= 0xff&(raw<<0);
		rawBMV[103] &= (~0x0f);
		rawBMV[103] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup051);
		rawBMV[101] &= (~(0x0f<<4)); 
		rawBMV[101] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage051)/0.010000;
		rawBMV[100] &= 0xff&(raw<<0);
		rawBMV[101] &= (~0x0f);
		rawBMV[101] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup050);
		rawBMV[99] &= (~(0x0f<<4)); 
		rawBMV[99] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage050)/0.010000;
		rawBMV[98] &= 0xff&(raw<<0);
		rawBMV[99] &= (~0x0f);
		rawBMV[99] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup049);
		rawBMV[97] &= (~(0x0f<<4)); 
		rawBMV[97] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage049)/0.010000;
		rawBMV[96] &= 0xff&(raw<<0);
		rawBMV[97] &= (~0x0f);
		rawBMV[97] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup048);
		rawBMV[95] &= (~(0x0f<<4)); 
		rawBMV[95] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage048)/0.010000;
		rawBMV[94] &= 0xff&(raw<<0);
		rawBMV[95] &= (~0x0f);
		rawBMV[95] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup047);
		rawBMV[93] &= (~(0x0f<<4)); 
		rawBMV[93] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage047)/0.010000;
		rawBMV[92] &= 0xff&(raw<<0);
		rawBMV[93] &= (~0x0f);
		rawBMV[93] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup046);
		rawBMV[91] &= (~(0x0f<<4)); 
		rawBMV[91] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage046)/0.010000;
		rawBMV[90] &= 0xff&(raw<<0);
		rawBMV[91] &= (~0x0f);
		rawBMV[91] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup045);
		rawBMV[89] &= (~(0x0f<<4)); 
		rawBMV[89] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage045)/0.010000;
		rawBMV[88] &= 0xff&(raw<<0);
		rawBMV[89] &= (~0x0f);
		rawBMV[89] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup044);
		rawBMV[87] &= (~(0x0f<<4)); 
		rawBMV[87] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage044)/0.010000;
		rawBMV[86] &= 0xff&(raw<<0);
		rawBMV[87] &= (~0x0f);
		rawBMV[87] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup043);
		rawBMV[85] &= (~(0x0f<<4)); 
		rawBMV[85] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage043)/0.010000;
		rawBMV[84] &= 0xff&(raw<<0);
		rawBMV[85] &= (~0x0f);
		rawBMV[85] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup042);
		rawBMV[83] &= (~(0x0f<<4)); 
		rawBMV[83] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage042)/0.010000;
		rawBMV[82] &= 0xff&(raw<<0);
		rawBMV[83] &= (~0x0f);
		rawBMV[83] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup041);
		rawBMV[81] &= (~(0x0f<<4)); 
		rawBMV[81] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage041)/0.010000;
		rawBMV[80] &= 0xff&(raw<<0);
		rawBMV[81] &= (~0x0f);
		rawBMV[81] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup040);
		rawBMV[79] &= (~(0x0f<<4)); 
		rawBMV[79] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage040)/0.010000;
		rawBMV[78] &= 0xff&(raw<<0);
		rawBMV[79] &= (~0x0f);
		rawBMV[79] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup039);
		rawBMV[77] &= (~(0x0f<<4)); 
		rawBMV[77] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage039)/0.010000;
		rawBMV[76] &= 0xff&(raw<<0);
		rawBMV[77] &= (~0x0f);
		rawBMV[77] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup038);
		rawBMV[75] &= (~(0x0f<<4)); 
		rawBMV[75] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage038)/0.010000;
		rawBMV[74] &= 0xff&(raw<<0);
		rawBMV[75] &= (~0x0f);
		rawBMV[75] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup037);
		rawBMV[73] &= (~(0x0f<<4)); 
		rawBMV[73] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage037)/0.010000;
		rawBMV[72] &= 0xff&(raw<<0);
		rawBMV[73] &= (~0x0f);
		rawBMV[73] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup036);
		rawBMV[71] &= (~(0x0f<<4)); 
		rawBMV[71] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage036)/0.010000;
		rawBMV[70] &= 0xff&(raw<<0);
		rawBMV[71] &= (~0x0f);
		rawBMV[71] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup035);
		rawBMV[69] &= (~(0x0f<<4)); 
		rawBMV[69] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage035)/0.010000;
		rawBMV[68] &= 0xff&(raw<<0);
		rawBMV[69] &= (~0x0f);
		rawBMV[69] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup034);
		rawBMV[67] &= (~(0x0f<<4)); 
		rawBMV[67] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage034)/0.010000;
		rawBMV[66] &= 0xff&(raw<<0);
		rawBMV[67] &= (~0x0f);
		rawBMV[67] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup033);
		rawBMV[65] &= (~(0x0f<<4)); 
		rawBMV[65] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage033)/0.010000;
		rawBMV[64] &= 0xff&(raw<<0);
		rawBMV[65] &= (~0x0f);
		rawBMV[65] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup032);
		rawBMV[63] &= (~(0x0f<<4)); 
		rawBMV[63] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage032)/0.010000;
		rawBMV[62] &= 0xff&(raw<<0);
		rawBMV[63] &= (~0x0f);
		rawBMV[63] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup031);
		rawBMV[61] &= (~(0x0f<<4)); 
		rawBMV[61] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage031)/0.010000;
		rawBMV[60] &= 0xff&(raw<<0);
		rawBMV[61] &= (~0x0f);
		rawBMV[61] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup030);
		rawBMV[59] &= (~(0x0f<<4)); 
		rawBMV[59] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage030)/0.010000;
		rawBMV[58] &= 0xff&(raw<<0);
		rawBMV[59] &= (~0x0f);
		rawBMV[59] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup029);
		rawBMV[57] &= (~(0x0f<<4)); 
		rawBMV[57] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage029)/0.010000;
		rawBMV[56] &= 0xff&(raw<<0);
		rawBMV[57] &= (~0x0f);
		rawBMV[57] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup028);
		rawBMV[55] &= (~(0x0f<<4)); 
		rawBMV[55] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage028)/0.010000;
		rawBMV[54] &= 0xff&(raw<<0);
		rawBMV[55] &= (~0x0f);
		rawBMV[55] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup027);
		rawBMV[53] &= (~(0x0f<<4)); 
		rawBMV[53] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage027)/0.010000;
		rawBMV[52] &= 0xff&(raw<<0);
		rawBMV[53] &= (~0x0f);
		rawBMV[53] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup026);
		rawBMV[51] &= (~(0x0f<<4)); 
		rawBMV[51] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage026)/0.010000;
		rawBMV[50] &= 0xff&(raw<<0);
		rawBMV[51] &= (~0x0f);
		rawBMV[51] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup025);
		rawBMV[49] &= (~(0x0f<<4)); 
		rawBMV[49] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage025)/0.010000;
		rawBMV[48] &= 0xff&(raw<<0);
		rawBMV[49] &= (~0x0f);
		rawBMV[49] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup024);
		rawBMV[47] &= (~(0x0f<<4)); 
		rawBMV[47] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage024)/0.010000;
		rawBMV[46] &= 0xff&(raw<<0);
		rawBMV[47] &= (~0x0f);
		rawBMV[47] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup023);
		rawBMV[45] &= (~(0x0f<<4)); 
		rawBMV[45] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage023)/0.010000;
		rawBMV[44] &= 0xff&(raw<<0);
		rawBMV[45] &= (~0x0f);
		rawBMV[45] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup022);
		rawBMV[43] &= (~(0x0f<<4)); 
		rawBMV[43] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage022)/0.010000;
		rawBMV[42] &= 0xff&(raw<<0);
		rawBMV[43] &= (~0x0f);
		rawBMV[43] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup021);
		rawBMV[41] &= (~(0x0f<<4)); 
		rawBMV[41] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage021)/0.010000;
		rawBMV[40] &= 0xff&(raw<<0);
		rawBMV[41] &= (~0x0f);
		rawBMV[41] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup020);
		rawBMV[39] &= (~(0x0f<<4)); 
		rawBMV[39] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage020)/0.010000;
		rawBMV[38] &= 0xff&(raw<<0);
		rawBMV[39] &= (~0x0f);
		rawBMV[39] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup019);
		rawBMV[37] &= (~(0x0f<<4)); 
		rawBMV[37] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage019)/0.010000;
		rawBMV[36] &= 0xff&(raw<<0);
		rawBMV[37] &= (~0x0f);
		rawBMV[37] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup018);
		rawBMV[35] &= (~(0x0f<<4)); 
		rawBMV[35] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage018)/0.010000;
		rawBMV[34] &= 0xff&(raw<<0);
		rawBMV[35] &= (~0x0f);
		rawBMV[35] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup017);
		rawBMV[33] &= (~(0x0f<<4)); 
		rawBMV[33] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage017)/0.010000;
		rawBMV[32] &= 0xff&(raw<<0);
		rawBMV[33] &= (~0x0f);
		rawBMV[33] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup016);
		rawBMV[31] &= (~(0x0f<<4)); 
		rawBMV[31] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage016)/0.010000;
		rawBMV[30] &= 0xff&(raw<<0);
		rawBMV[31] &= (~0x0f);
		rawBMV[31] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup015);
		rawBMV[29] &= (~(0x0f<<4)); 
		rawBMV[29] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage015)/0.010000;
		rawBMV[28] &= 0xff&(raw<<0);
		rawBMV[29] &= (~0x0f);
		rawBMV[29] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup014);
		rawBMV[27] &= (~(0x0f<<4)); 
		rawBMV[27] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage014)/0.010000;
		rawBMV[26] &= 0xff&(raw<<0);
		rawBMV[27] &= (~0x0f);
		rawBMV[27] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup013);
		rawBMV[25] &= (~(0x0f<<4)); 
		rawBMV[25] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage013)/0.010000;
		rawBMV[24] &= 0xff&(raw<<0);
		rawBMV[25] &= (~0x0f);
		rawBMV[25] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup012);
		rawBMV[23] &= (~(0x0f<<4)); 
		rawBMV[23] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage012)/0.010000;
		rawBMV[22] &= 0xff&(raw<<0);
		rawBMV[23] &= (~0x0f);
		rawBMV[23] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup011);
		rawBMV[21] &= (~(0x0f<<4)); 
		rawBMV[21] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage011)/0.010000;
		rawBMV[20] &= 0xff&(raw<<0);
		rawBMV[21] &= (~0x0f);
		rawBMV[21] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup010);
		rawBMV[19] &= (~(0x0f<<4)); 
		rawBMV[19] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage010)/0.010000;
		rawBMV[18] &= 0xff&(raw<<0);
		rawBMV[19] &= (~0x0f);
		rawBMV[19] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup009);
		rawBMV[17] &= (~(0x0f<<4)); 
		rawBMV[17] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage009)/0.010000;
		rawBMV[16] &= 0xff&(raw<<0);
		rawBMV[17] &= (~0x0f);
		rawBMV[17] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup008);
		rawBMV[15] &= (~(0x0f<<4)); 
		rawBMV[15] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage008)/0.010000;
		rawBMV[14] &= 0xff&(raw<<0);
		rawBMV[15] &= (~0x0f);
		rawBMV[15] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup007);
		rawBMV[13] &= (~(0x0f<<4)); 
		rawBMV[13] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage007)/0.010000;
		rawBMV[12] &= 0xff&(raw<<0);
		rawBMV[13] &= (~0x0f);
		rawBMV[13] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup006);
		rawBMV[11] &= (~(0x0f<<4)); 
		rawBMV[11] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage006)/0.010000;
		rawBMV[10] &= 0xff&(raw<<0);
		rawBMV[11] &= (~0x0f);
		rawBMV[11] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup005);
		rawBMV[9] &= (~(0x0f<<4)); 
		rawBMV[9] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage005)/0.010000;
		rawBMV[8] &= 0xff&(raw<<0);
		rawBMV[9] &= (~0x0f);
		rawBMV[9] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup004);
		rawBMV[7] &= (~(0x0f<<4)); 
		rawBMV[7] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage004)/0.010000;
		rawBMV[6] &= 0xff&(raw<<0);
		rawBMV[7] &= (~0x0f);
		rawBMV[7] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup003);
		rawBMV[5] &= (~(0x0f<<4)); 
		rawBMV[5] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage003)/0.010000;
		rawBMV[4] &= 0xff&(raw<<0);
		rawBMV[5] &= (~0x0f);
		rawBMV[5] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup002);
		rawBMV[3] &= (~(0x0f<<4)); 
		rawBMV[3] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage002)/0.010000;
		rawBMV[2] &= 0xff&(raw<<0);
		rawBMV[3] &= (~0x0f);
		rawBMV[3] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBMV.SingleBatteryGroup001);
		rawBMV[1] &= (~(0x0f<<4)); 
		rawBMV[1] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBMV.SingleBatteryVoltage001)/0.010000;
		rawBMV[0] &= 0xff&(raw<<0);
		rawBMV[1] &= (~0x0f);
		rawBMV[1] ^= ((0xff00&(raw<<0))>>8);
	}

}

void BSM2Raw()
{
	msgBSM.ChargingPermissible =((rawBSM[6]>>4) & 0x03);
	msgBSM.BatteryOutputConnectorState =((rawBSM[6]>>2) & 0x03);
	msgBSM.BatteryInsulationState =((rawBSM[6]) & 0x03);
	msgBSM.BatteryExcessTemperature =((rawBSM[5]>>6) & 0x03);
	msgBSM.BatteryChargingOvercurrent =((rawBSM[5]>>4) & 0x03);
	msgBSM.StateOfCharge =((rawBSM[5]>>2) & 0x03);
	msgBSM.BatteryCellVoltageState =((rawBSM[5]) & 0x03);
	msgBSM.BatteryLowestTempSerNo =((rawBSM[4]) & 0xff) + 1;
	msgBSM.BatteryLowestTemp =((rawBSM[3]) & 0xff) - 50;
	msgBSM.BatteryHighestTempSerNo =((rawBSM[2]) & 0xff) + 1;
	msgBSM.BatteryHighestTemp =((rawBSM[1]) & 0xff) - 50;
	msgBSM.SingleBatteryHighestVoltageSerNo =((rawBSM[0]) & 0xff) + 1;
}

void Raw2BSM()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSM.ChargingPermissible);
		rawBSM[6] &= (~(0x03<<4)); 
		rawBSM[6] ^= (raw<<4);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSM.BatteryOutputConnectorState);
		rawBSM[6] &= (~(0x03<<2)); 
		rawBSM[6] ^= (raw<<2);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSM.BatteryInsulationState);
		rawBSM[6] &= (~(0x03)); 
		rawBSM[6] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSM.BatteryExcessTemperature);
		rawBSM[5] &= (~(0x03<<6)); 
		rawBSM[5] ^= (raw<<6);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSM.BatteryChargingOvercurrent);
		rawBSM[5] &= (~(0x03<<4)); 
		rawBSM[5] ^= (raw<<4);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSM.StateOfCharge);
		rawBSM[5] &= (~(0x03<<2)); 
		rawBSM[5] ^= (raw<<2);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSM.BatteryCellVoltageState);
		rawBSM[5] &= (~(0x03)); 
		rawBSM[5] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSM.BatteryLowestTempSerNo-1.000000);
		rawBSM[4] &= (~(0xff)); 
		rawBSM[4] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSM.BatteryLowestTemp+50.000000);
		rawBSM[3] &= (~(0xff)); 
		rawBSM[3] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSM.BatteryHighestTempSerNo-1.000000);
		rawBSM[2] &= (~(0xff)); 
		rawBSM[2] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSM.BatteryHighestTemp+50.000000);
		rawBSM[1] &= (~(0xff)); 
		rawBSM[1] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBSM.SingleBatteryHighestVoltageSerNo-1.000000);
		rawBSM[0] &= (~(0xff)); 
		rawBSM[0] ^= (raw<<0);
	}

}

void CCS2Raw()
{
	msgCCS.ChargingPermissible =((rawCCS[6]) & 0x03);
	msgCCS.CumulativeChargingTime =(((rawCCS[5]<<8) + rawCCS[4])>>0) & 0xffff;
	msgCCS.CurrentOutputValue = ((((rawCCS[3]<<8) + rawCCS[2])>>0) & 0xffff) * 0.1 - 400;
	msgCCS.VoltageOutputValue = ((((rawCCS[1]<<8) + rawCCS[0])>>0) & 0xffff) * 0.1;
}

void Raw2CCS()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCCS.ChargingPermissible);
		rawCCS[6] &= (~(0x03)); 
		rawCCS[6] ^= (raw<<0);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgCCS.CumulativeChargingTime);
		rawCCS[4] &= 0xff&(raw<<0);
		rawCCS[5] &= (~0xff);
		rawCCS[5] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgCCS.CurrentOutputValue+400.000000)/0.100000;
		rawCCS[2] &= 0xff&(raw<<0);
		rawCCS[3] &= (~0xff);
		rawCCS[3] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgCCS.VoltageOutputValue)/0.100000;
		rawCCS[0] &= 0xff&(raw<<0);
		rawCCS[1] &= (~0xff);
		rawCCS[1] ^= ((0xff00&(raw<<0))>>8);
	}

}

void BCS2Raw()
{
	msgBCS.EstimatedRemainingChargingTime =(((rawBCS[8]<<8) + rawBCS[7])>>0) & 0xffff;
	msgBCS.CurrentChargeState =((rawBCS[6]) & 0xff);
	msgBCS.GroupNrOfCellWithHighestVoltage =((rawBCS[5]>>4) & 0x0f);
	msgBCS.HighestVoltageOfBatteryCell = ((((rawBCS[5]<<8) + rawBCS[4])>>0) & 0xfff) * 0.01;
	msgBCS.MeasuredChargingCurrent = ((((rawBCS[3]<<8) + rawBCS[2])>>0) & 0xffff) * 0.1 - 400;
	msgBCS.MeasuredChargingVoltage = ((((rawBCS[1]<<8) + rawBCS[0])>>0) & 0xffff) * 0.1;
}

void Raw2BCS()
{
	{
		uint16_t raw;
		raw = (uint16_t)(msgBCS.EstimatedRemainingChargingTime);
		rawBCS[7] &= 0xff&(raw<<0);
		rawBCS[8] &= (~0xff);
		rawBCS[8] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBCS.CurrentChargeState);
		rawBCS[6] &= (~(0xff)); 
		rawBCS[6] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBCS.GroupNrOfCellWithHighestVoltage);
		rawBCS[5] &= (~(0x0f<<4)); 
		rawBCS[5] ^= (raw<<4);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBCS.HighestVoltageOfBatteryCell)/0.010000;
		rawBCS[4] &= 0xff&(raw<<0);
		rawBCS[5] &= (~0x0f);
		rawBCS[5] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBCS.MeasuredChargingCurrent+400.000000)/0.100000;
		rawBCS[2] &= 0xff&(raw<<0);
		rawBCS[3] &= (~0xff);
		rawBCS[3] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBCS.MeasuredChargingVoltage)/0.100000;
		rawBCS[0] &= 0xff&(raw<<0);
		rawBCS[1] &= (~0xff);
		rawBCS[1] ^= ((0xff00&(raw<<0))>>8);
	}

}

void BCL2Raw()
{
	msgBCL.ChargingMode =((rawBCL[4]) & 0xff);
	msgBCL.CurrentDemand = ((((rawBCL[3]<<8) + rawBCL[2])>>0) & 0xffff) * 0.1 - 400;
	msgBCL.VoltageDemand = ((((rawBCL[1]<<8) + rawBCL[0])>>0) & 0xffff) * 0.1;
}

void Raw2BCL()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBCL.ChargingMode);
		rawBCL[4] &= (~(0xff)); 
		rawBCL[4] ^= (raw<<0);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBCL.CurrentDemand+400.000000)/0.100000;
		rawBCL[2] &= 0xff&(raw<<0);
		rawBCL[3] &= (~0xff);
		rawBCL[3] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBCL.VoltageDemand)/0.100000;
		rawBCL[0] &= 0xff&(raw<<0);
		rawBCL[1] &= (~0xff);
		rawBCL[1] ^= ((0xff00&(raw<<0))>>8);
	}

}

void CRO2Raw()
{
	msgCRO.ChargerReadyForCharging =((rawCRO[0]) & 0xff);
}

void Raw2CRO()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCRO.ChargerReadyForCharging);
		rawCRO[0] &= (~(0xff)); 
		rawCRO[0] ^= (raw<<0);
	}

}

void BRO2Raw()
{
	msgBRO.BmsReadyForCharging =((rawBRO[0]) & 0xff);
}

void Raw2BRO()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBRO.BmsReadyForCharging);
		rawBRO[0] &= (~(0xff)); 
		rawBRO[0] ^= (raw<<0);
	}

}

void CML2Raw()
{
	msgCML.MinOutputCurrent = ((((rawCML[7]<<8) + rawCML[6])>>0) & 0xffff) * 0.1 - 400;
	msgCML.MaxOutputCurrent = ((((rawCML[5]<<8) + rawCML[4])>>0) & 0xffff) * 0.1 - 400;
	msgCML.MinOutputVoltage = ((((rawCML[3]<<8) + rawCML[2])>>0) & 0xffff) * 0.1;
	msgCML.MaxOutputVoltage = ((((rawCML[1]<<8) + rawCML[0])>>0) & 0xffff) * 0.1;
}

void Raw2CML()
{
	{
		uint16_t raw;
		raw = (uint16_t)(msgCML.MinOutputCurrent+400.000000)/0.100000;
		rawCML[6] &= 0xff&(raw<<0);
		rawCML[7] &= (~0xff);
		rawCML[7] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgCML.MaxOutputCurrent+400.000000)/0.100000;
		rawCML[4] &= 0xff&(raw<<0);
		rawCML[5] &= (~0xff);
		rawCML[5] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgCML.MinOutputVoltage)/0.100000;
		rawCML[2] &= 0xff&(raw<<0);
		rawCML[3] &= (~0xff);
		rawCML[3] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgCML.MaxOutputVoltage)/0.100000;
		rawCML[0] &= 0xff&(raw<<0);
		rawCML[1] &= (~0xff);
		rawCML[1] ^= ((0xff00&(raw<<0))>>8);
	}

}

void CTS2Raw()
{
	msgCTS.Centuries =((rawCTS[6]) & 0xff);
	msgCTS.Years =((rawCTS[5]) & 0xff);
	msgCTS.Months =((rawCTS[4]) & 0xff);
	msgCTS.Days =((rawCTS[3]) & 0xff);
	msgCTS.Hours =((rawCTS[2]) & 0xff);
	msgCTS.Minutes =((rawCTS[1]) & 0xff);
	msgCTS.Seconds =((rawCTS[0]) & 0xff);
}

void Raw2CTS()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCTS.Centuries);
		rawCTS[6] &= (~(0xff)); 
		rawCTS[6] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCTS.Years);
		rawCTS[5] &= (~(0xff)); 
		rawCTS[5] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCTS.Months);
		rawCTS[4] &= (~(0xff)); 
		rawCTS[4] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCTS.Days);
		rawCTS[3] &= (~(0xff)); 
		rawCTS[3] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCTS.Hours);
		rawCTS[2] &= (~(0xff)); 
		rawCTS[2] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCTS.Minutes);
		rawCTS[1] &= (~(0xff)); 
		rawCTS[1] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCTS.Seconds);
		rawCTS[0] &= (~(0xff)); 
		rawCTS[0] ^= (raw<<0);
	}

}

void BCP2Raw()
{
	msgBCP.CurrentBatteryVoltage = ((((rawBCP[12]<<8) + rawBCP[11])>>0) & 0xffff) * 0.1;
	msgBCP.StatusOfCharge = ((((rawBCP[10]<<8) + rawBCP[9])>>0) & 0xffff) * 0.1;
	msgBCP.MaxPermTemperature =((rawBCP[8]) & 0xff) - 50;
	msgBCP.TotalMaxPermChargingVoltage = ((((rawBCP[7]<<8) + rawBCP[6])>>0) & 0xffff) * 0.1;
	msgBCP.TotalNominalEnergy = ((((rawBCP[5]<<8) + rawBCP[4])>>0) & 0xffff) * 0.1;
	msgBCP.MaxPermChargingCurrent = ((((rawBCP[3]<<8) + rawBCP[2])>>0) & 0xffff) * 0.1 - 400;
	msgBCP.MaxBattCellChargingVoltage = ((((rawBCP[1]<<8) + rawBCP[0])>>0) & 0xffff) * 0.01;
}

void Raw2BCP()
{
	{
		uint16_t raw;
		raw = (uint16_t)(msgBCP.CurrentBatteryVoltage)/0.100000;
		rawBCP[11] &= 0xff&(raw<<0);
		rawBCP[12] &= (~0xff);
		rawBCP[12] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBCP.StatusOfCharge)/0.100000;
		rawBCP[9] &= 0xff&(raw<<0);
		rawBCP[10] &= (~0xff);
		rawBCP[10] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBCP.MaxPermTemperature+50.000000);
		rawBCP[8] &= (~(0xff)); 
		rawBCP[8] ^= (raw<<0);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBCP.TotalMaxPermChargingVoltage)/0.100000;
		rawBCP[6] &= 0xff&(raw<<0);
		rawBCP[7] &= (~0xff);
		rawBCP[7] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBCP.TotalNominalEnergy)/0.100000;
		rawBCP[4] &= 0xff&(raw<<0);
		rawBCP[5] &= (~0xff);
		rawBCP[5] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBCP.MaxPermChargingCurrent+400.000000)/0.100000;
		rawBCP[2] &= 0xff&(raw<<0);
		rawBCP[3] &= (~0xff);
		rawBCP[3] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBCP.MaxBattCellChargingVoltage)/0.010000;
		rawBCP[0] &= 0xff&(raw<<0);
		rawBCP[1] &= (~0xff);
		rawBCP[1] ^= ((0xff00&(raw<<0))>>8);
	}

}

void BRM2Raw()
{
	msgBRM.BmsSoftwareVersion_Reserved =(((rawBRM[48]<<16) +(rawBRM[47]<<8) + rawBRM[46])>>0) & 0xffffff;
	msgBRM.BmsSoftwareVersion_CompileYear =(((rawBRM[45]<<8) + rawBRM[44])>>0) & 0xffff;
	msgBRM.BmsSoftwareVersion_CompileMonth =((rawBRM[43]) & 0xff);
	msgBRM.BmsSoftwareVersion_CompileDay =((rawBRM[42]) & 0xff);
	msgBRM.BmsSoftwareVersion_Serial =((rawBRM[41]) & 0xff);
	msgBRM.BRM_Reserved1 =((rawBRM[23]) & 0xff);
	//msgBRM.VIN =NULL;
	msgBRM.Property =((rawBRM[22]) & 0xff);
	msgBRM.ChargingFrequency =(((rawBRM[21]<<16) +(rawBRM[20]<<8) + rawBRM[19])>>0) & 0xffffff;
	msgBRM.ProductionDay =((rawBRM[18]) & 0xff);
	msgBRM.ProductionMonth =((rawBRM[17]) & 0xff);
	msgBRM.ProductionYear =((rawBRM[16]) & 0xff) + 1985;
	msgBRM.BatteryPackNumber =(((rawBRM[15]<<24) + (rawBRM[14]<<16) + (rawBRM[13]<<8) + rawBRM[12])>>0) & 0xffffffff;
	msgBRM.ManufacturerName =(((rawBRM[11]<<24) + (rawBRM[10]<<16) + (rawBRM[9]<<8) + rawBRM[8])>>0) & 0xffffffff;
	msgBRM.BatterySystemVoltage = ((((rawBRM[7]<<8) + rawBRM[6])>>0) & 0xffff) * 0.1;
	msgBRM.BatterySystemCapacity = ((((rawBRM[5]<<8) + rawBRM[4])>>0) & 0xffff) * 0.1;
	msgBRM.BmsVersion =(((rawBRM[2]<<8) + rawBRM[1])>>0) & 0xffff;
	msgBRM.BmsRevision =((rawBRM[0]) & 0xff);
	msgBRM.BatteryType =((rawBRM[3]) & 0xff);
}

void Raw2BRM()
{
	{
		uint32_t raw;
		raw = (uint32_t)(msgBRM.BmsSoftwareVersion_Reserved);
		rawBRM[46] &= 0xff&(raw<<0);
		rawBRM[47] &= 0xff00&(raw<<0);
		rawBRM[48] &= (~0xff);
		rawBRM[48] ^= ((0xff0000&(raw<<0))>>16);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBRM.BmsSoftwareVersion_CompileYear);
		rawBRM[44] &= 0xff&(raw<<0);
		rawBRM[45] &= (~0xff);
		rawBRM[45] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBRM.BmsSoftwareVersion_CompileMonth);
		rawBRM[43] &= (~(0xff)); 
		rawBRM[43] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBRM.BmsSoftwareVersion_CompileDay);
		rawBRM[42] &= (~(0xff)); 
		rawBRM[42] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBRM.BmsSoftwareVersion_Serial);
		rawBRM[41] &= (~(0xff)); 
		rawBRM[41] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBRM.BRM_Reserved1);
		rawBRM[23] &= (~(0xff)); 
		rawBRM[23] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBRM.Property);
		rawBRM[22] &= (~(0xff)); 
		rawBRM[22] ^= (raw<<0);
	}
	{
		uint32_t raw;
		raw = (uint32_t)(msgBRM.ChargingFrequency);
		rawBRM[19] &= 0xff&(raw<<0);
		rawBRM[20] &= 0xff00&(raw<<0);
		rawBRM[21] &= (~0xff);
		rawBRM[21] ^= ((0xff0000&(raw<<0))>>16);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBRM.ProductionDay);
		rawBRM[18] &= (~(0xff)); 
		rawBRM[18] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBRM.ProductionMonth);
		rawBRM[17] &= (~(0xff)); 
		rawBRM[17] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBRM.ProductionYear-1985.000000);
		rawBRM[16] &= (~(0xff)); 
		rawBRM[16] ^= (raw<<0);
	}
	{
		uint32_t raw;
		raw = (uint32_t)(msgBRM.BatteryPackNumber); 
		rawBRM[12] &= 0xff&(raw<<0);
		rawBRM[13] &= 0xff00&(raw<<0);
		rawBRM[14] &= 0xff0000&(raw<<0);
		rawBRM[15] &= (~0xff);
		rawBRM[15] ^= ((0xff000000&(raw<<0))>>24);
	}
	{
		uint32_t raw;
		raw = (uint32_t)(msgBRM.ManufacturerName); 
		rawBRM[8] &= 0xff&(raw<<0);
		rawBRM[9] &= 0xff00&(raw<<0);
		rawBRM[10] &= 0xff0000&(raw<<0);
		rawBRM[11] &= (~0xff);
		rawBRM[11] ^= ((0xff000000&(raw<<0))>>24);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBRM.BatterySystemVoltage)/0.100000;
		rawBRM[6] &= 0xff&(raw<<0);
		rawBRM[7] &= (~0xff);
		rawBRM[7] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBRM.BatterySystemCapacity)/0.100000;
		rawBRM[4] &= 0xff&(raw<<0);
		rawBRM[5] &= (~0xff);
		rawBRM[5] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgBRM.BmsVersion);
		rawBRM[1] &= 0xff&(raw<<0);
		rawBRM[2] &= (~0xff);
		rawBRM[2] ^= ((0xff00&(raw<<0))>>8);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBRM.BmsRevision);
		rawBRM[0] &= (~(0xff)); 
		rawBRM[0] ^= (raw<<0);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgBRM.BatteryType);
		rawBRM[3] &= (~(0xff)); 
		rawBRM[3] ^= (raw<<0);
	}

}

void CRM2Raw()
{
	msgCRM.ChargerLocation =(((rawCRM[7]<<16) +(rawCRM[6]<<8) + rawCRM[5])>>0) & 0xffffff;
	msgCRM.ChargerNumber =(((rawCRM[4]<<24) + (rawCRM[3]<<16) + (rawCRM[2]<<8) + rawCRM[1])>>0) & 0xffffffff;
	msgCRM.RecognitionResult =((rawCRM[0]) & 0xff);
}

void Raw2CRM()
{
	{
		uint32_t raw;
		raw = (uint32_t)(msgCRM.ChargerLocation);
		rawCRM[5] &= 0xff&(raw<<0);
		rawCRM[6] &= 0xff00&(raw<<0);
		rawCRM[7] &= (~0xff);
		rawCRM[7] ^= ((0xff0000&(raw<<0))>>16);
	}
	{
		uint32_t raw;
		raw = (uint32_t)(msgCRM.ChargerNumber); 
		rawCRM[1] &= 0xff&(raw<<0);
		rawCRM[2] &= 0xff00&(raw<<0);
		rawCRM[3] &= 0xff0000&(raw<<0);
		rawCRM[4] &= (~0xff);
		rawCRM[4] ^= ((0xff000000&(raw<<0))>>24);
	}
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCRM.RecognitionResult);
		rawCRM[0] &= (~(0xff)); 
		rawCRM[0] ^= (raw<<0);
	}

}

void BHM2Raw()
{
	msgBHM.MaxChargingVoltage = ((((rawBHM[1]<<8) + rawBHM[0])>>0) & 0xffff) * 0.1;
}

void Raw2BHM()
{
	{
		uint16_t raw;
		raw = (uint16_t)(msgBHM.MaxChargingVoltage)/0.100000;
		rawBHM[0] &= 0xff&(raw<<0);
		rawBHM[1] &= (~0xff);
		rawBHM[1] ^= ((0xff00&(raw<<0))>>8);
	}

}

void CHM2Raw()
{
	msgCHM.MinorChargerVersion =((rawCHM[0]) & 0xff);
	msgCHM.MajorChargerVersion =(((rawCHM[2]<<8) + rawCHM[1])>>0) & 0xffff;
}

void Raw2CHM()
{
	{
		uint8_t raw; 
		raw = (uint8_t)(msgCHM.MinorChargerVersion);
		rawCHM[0] &= (~(0xff)); 
		rawCHM[0] ^= (raw<<0);
	}
	{
		uint16_t raw;
		raw = (uint16_t)(msgCHM.MajorChargerVersion);
		rawCHM[1] &= 0xff&(raw<<0);
		rawCHM[2] &= (~0xff);
		rawCHM[2] ^= ((0xff00&(raw<<0))>>8);
	}

}

